\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage[style=iso]{datetime2}
\usepackage{amsmath, amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{ragged2e}
\usepackage{siunitx}
\pdfsuppresswarningpagegroup=1

\title{Theoretische Physik 3: Quantenmechanik}
\subtitle{Submission for exercise 1}
\author{Stefan Gehr}

\date{2020-04-25}
%\date{\today}

\begin{document}
\maketitle
\section*{Problem 1: Double Slit Experiment}
\subsection*{a,}
When choosing the midway between the two slits \(S_1\) and \(S_2\) as the centre of our coordinate system then point \(P\) lies at \(\vec{r} = (r \cos{\theta}, r \sin{\theta})^{\mathrm{T}}\).
Therefore the distances \(r_1\) and  \(r_2\) correspond to
\begin{align*}
	r_{1,2} &= \left|\left| \vec{r} - \left(0, \pm d / 2 \right)^{\mathrm{T}} \right|\right| \\
			&= \sqrt{r^2 \cos^2{\theta} + (r \sin{\theta} \mp d/2)^2} = \sqrt{r^2 + d^2/4 \mp dr \sin{\theta}} \\
			&= r \sqrt{1 + \frac{d^2}{4r^2} \mp \frac{d}{r} \sin{\theta}}
.\end{align*}
This means the difference equates to \[
\delta = | r_1 - r_2 | = \left| \sqrt{r^2 + d^2/4 - dr \sin{\theta}} - \sqrt{r^2 + d^2/4 + dr \sin{\theta}} \right|
.\] 
Because of \(r = L \cos{\theta} \leq L\) by assuming \( L \gg d \) we can also assume \( r \gg d \).
By doing a Taylor series around  \(\frac{d}{r} \approx 0\) we get
\begin{align}
	r_{1,2} = r \left( 1 \mp \frac{d \sin{\theta}}{2r} + O\left(\frac{d^2}{r^2}\right) \right)
\label{eq:tayloredr}
.\end{align}
When ditching the terms for orders \(\geq 2\) and allowing \(\delta\) to be negative when \(r_2>r_1\) we get the simple equation  \[
	\delta = r_1 - r_2 = d \sin{\theta}
.\]
\subsection*{b,}
With \(y = L \tan{\theta}\) we can write above equation as \[
	\delta = d \sin{(\arctan{(y/L)})}
,\] or if we're only interested in small deflections from the horizontal axis (i.e. \(|y| \ll L\))
\begin{align}
	\delta = \frac{d}{L}y
	\label{eq:deltay}
.\end{align}
Constructive interference happens when \(\delta = n \lambda\) where \(\lambda\) is the wavelength of the wave and \(n \in \mathbb{Z}\). This equates to the relation \[
	y_{\mathrm{max}} = \frac{L \lambda}{d} n
.\]
Destructive interference on the other hand happens when \(\delta = (2n-1) \lambda / 2\), again with  \(n \in \mathbb{Z}\), which leads us to \[
	y_{\mathrm{min}} = \frac{L \lambda}{2d} ( 2n - 1 )
.\] 

\subsection*{c,}
First, let's define \(\beta := \omega t\), \(\gamma_j := 2 \pi r_j/\lambda\) and \(\alpha_j := \beta - \gamma_j\).
Now we can combine the two waves according to the superposition principle
\begin{align*}
	|E_1 + E_2|^2	&= |\overline{E}|^2 (\sin{\alpha_1} + \sin{\alpha_2})^2 \\
					&= |\overline{E}|^2 \left[2 \sin{\left(\frac{\alpha_1+\alpha_2}{2}\right)} \cos{\left(\frac{\alpha_1-\alpha_2}{2}\right)}\right]^2 \\
					&= 4|\overline{E}|^2 \sin^2{\left(\frac{\alpha_1+\alpha_2}{2}\right)} \cos^2{\left(\frac{\pi}{\lambda}\delta\right)}
.\end{align*}
With \(\omega = 2\pi/T\) and \(\gamma_{\mathrm{m}} := (\gamma_1+\gamma_2) / 2\) we get
\begin{align*}
	I	&= \frac{4}{T} |\overline{E}|^2 \cos^2{\left(\frac{\pi}{\lambda}\delta\right)} \int_s^{s+T} \mathrm{d}t \sin^2{\left(\omega t - \frac{\gamma_1+\gamma_2}{2}\right)} \\
		&= \frac{4}{T} |\overline{E}|^2 \cos^2{\left(\frac{\pi}{\lambda}\delta\right)} \frac{T}{4\pi}
			\bigg[ u - \sin{(u)}\cos{(u)} \bigg]_{u=\omega s - \gamma_{\mathrm{m}}}^{\omega (s+T) - \gamma_{\mathrm{m}}} \\
		&= 2|\overline{E}|^2 \cos^2{\left(\frac{\pi}{\lambda}\delta\right)}
\end{align*}
for the resulting intensity.
Combining this with \autoref{eq:deltay} we obtain \[
	I = 2 |\overline{E}|^2 \cos^2{\left(\frac{\pi d}{\lambda L} y\right)}
.\]
\begin{figure}[h]
	\Centering
	\includegraphics[scale=0.8]{cos2.pdf}
	\caption{Interference pattern caused by overlapping wave functions}
\end{figure} 
\subsection*{d,}
The intensity \(I\) of a classical particle beam is given by the average particle rate per area, or in SI-units: \([I] = \si{\per\second\per\metre\squared}\).
If we view \(A\) as the constant linear particle rate density for the \(z\)-direction (\([A] = \si{\per\second\per\metre}\)) we can focus on the radial dependence \[
	I_j(r) = \frac{A}{2\pi} \frac{1}{r}
.\] 
For the classical particle beams the intensities simply add up to
\begin{align}
	I = I_1 + I_2 &= \frac{A}{2\pi} \left( \frac{1}{r_1} + \frac{1}{r_2} \right)
\label{eq:classicalr}
.\end{align}
Using \autoref{eq:tayloredr} (and therefore assuming \(L \gg d\) and \(r \gg d\)) and \(y=r\sin{\theta}\) we get
\begin{align*}
	I	&= \frac{A}{2\pi} \left( \frac{1}{r-\frac{d}{2}\sin{\theta}} + \frac{1}{r+\frac{d}{2}\sin{\theta}} \right) \\
		&= \frac{A}{\pi} \frac{r}{r^2 - \frac{d^2}{4} \sin^2{\theta}} \\
		&= \frac{A}{\pi} \frac{r}{r^2 - \frac{d^2}{4r^2}y^2} \approx \frac{A}{\pi r} \\
		&= \frac{A}{\pi\sqrt{L^2 + y^2}}
.\end{align*}
\begin{figure}[h]
	\Centering
	\includegraphics[scale=0.8]{particlebeam.pdf}
	\caption{Maximum in the centre, trailing off on both sides}
\end{figure}
Alternatively, without assuming that \(L \gg d\), we can write \[
	r_{1,2} = \sqrt{L^2 + \left(y\mp\frac{d}{2}\right)^2}
\] 
and use this in \autoref{eq:classicalr}
\begin{align*}
	I = \frac{A}{2\pi} \left[
		\left( L^2 + \left(y - \frac{d}{2}\right)^2 \right)^{-\frac{1}{2}}
		+
		\left( L^2 + \left(y + \frac{d}{2}\right)^2 \right)^{-\frac{1}{2}}
	\right]
.\end{align*}
When \(L\) is small enough compared to \(d\) this gives us an entirely different pattern.
\begin{figure}[h]
	\Centering
	\includegraphics[scale=0.8]{particlebeamDlessThanL.pdf}
	\caption{Classical particle beam when \(L\) is small enough compared to  \(d\)}
\end{figure}
\end{document}

