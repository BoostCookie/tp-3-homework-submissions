#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

def main():
    plt.rc("text", usetex=True)
    plt.rc("font", family="serif")
    plt.rcParams.update({'font.size': 15})
    fig, ax = plt.subplots()

    y_max = 1
    y = np.linspace(-y_max, y_max, 1000)
    Ap = 0.2
    I = Ap / np.sqrt(Ap**2 + y**2)

    plt.plot(y, I)

    plt.yticks([0, 1], ["0", r"$\frac{A}{\pi L}$"])
    plt.xticks([0], ["0"])

    ax.set_xlim([-y_max, y_max])
    #plt.legend()
    plt.grid()
    #plt.minorticks_on()
    #plt.suptitle("title")
    ax.set_title(r"$I=\frac{A}{\pi\sqrt{L^2 + y^2}}$")
    plt.xlabel("y")
    plt.ylabel("Intensity")
    fig.savefig("particlebeam.pdf")

if __name__ == "__main__":
    main()
