#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

def main():
    plt.rc("text", usetex=True)
    plt.rc("font", family="serif")
    plt.rcParams.update({'font.size': 15})
    fig, ax = plt.subplots()

    y_max = 30
    y = np.linspace(-y_max, y_max, 1000)
    L = 1
    d = 3
    I = 1/np.sqrt(L**2 + (y - 0.5*d)**2) + 1/np.sqrt(L**2 + (y + 0.5*d)**2)

    plt.plot(y, I)

    plt.yticks([0], ["0"])
    plt.xticks([0], ["0"])

    ax.set_xlim([-y_max, y_max])
    #plt.legend()
    plt.grid()
    #plt.minorticks_on()
    #plt.suptitle("title")
    #ax.set_title(r"$I=\frac{A}{\pi}\frac{1}{\sqrt{L^2 + y^2}}$")
    plt.gcf().subplots_adjust(top=0.99, left=0.05, right=0.99)
    plt.xlabel("y")
    plt.ylabel("Intensity")
    fig.savefig("particlebeamDlessThanL.pdf")

if __name__ == "__main__":
    main()
