#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

def main():
    plt.rc("text", usetex=True)
    plt.rc("font", family="serif")
    plt.rcParams.update({'font.size': 15})
    fig, ax = plt.subplots()#figsize=(7,5))

    y_max = 2.2
    y = np.linspace(-y_max, y_max, 1000)
    I = np.cos(np.pi * y)**2

    plt.plot(y, I)

    ticklist = []
    ticknames = []
    frac = r"\frac{\lambda L}{d}"
    for i in range(int(np.ceil(-y_max)), int(np.floor(y_max)) + 1, 1):
        ticklist.append(i)
        if i == 0:
            ticknames.append("0")
        else:
            cur_tickname = frac
            if i == -1:
                cur_tickname = "-" + cur_tickname
            elif i != 1:
                cur_tickname = str(i) + cur_tickname
            cur_tickname = r"$" + cur_tickname + r"$"
            ticknames.append(cur_tickname)

    #print(f"{ticklist=}\n{ticknames=}")
    plt.xticks(ticklist, ticknames)
    plt.yticks([0, 1], ["0", r"$2 |\bar{E}|^2$"])

    ax.set_xlim([-y_max, y_max])
    #plt.legend()
    plt.grid()
    #plt.minorticks_on()
    #plt.suptitle("title")
    ax.set_title(r"$I=2 |\bar{E}|^2 \cos^2{\left(\frac{\pi d}{\lambda L}y\right)}$")
    plt.xlabel("y")
    plt.ylabel("Intensity")
    plt.tight_layout()
    plt.gcf().subplots_adjust(bottom=0.13)
    plt.gcf().subplots_adjust(left=0.14)
    #plt.xscale("log")
    #plt.yscale("log")
    fig.savefig("cos2.pdf")
    #fig.savefig("cos2.png", dpi=508) #200 dots/cm

if __name__ == "__main__":
    main()
