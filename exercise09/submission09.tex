\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage[style=iso]{datetime2}
\usepackage{amsmath, amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{ragged2e}
\usepackage[locale=DE]{siunitx}
\usepackage{braket}
\usepackage{bm}
\usepackage{bbold}
\usepackage{extarrows}
\pdfsuppresswarningpagegroup=1

\title{Theoretische Physik 3: Quantenmechanik}
\subtitle{Submission for exercise 9}
\author{Stefan Gehr}

%\date{\today}
\date{2020-06-29}

\begin{document}
\maketitle
\section*{Problem 1: Second order perturbation theory}
In the lecture we've derived
\begin{align}
	&(\hat{H}_0+\lambda\hat{H}_1)
	(\ket{\psi^{(0)}}+\lambda\ket{\psi^{(1)}}+\lambda^2\ket{\psi^{(2)}}\hdots) \nonumber\\
	&= ( W^{(0)}+\lambda W^{(1)}+\lambda^2 W^{(2)}+\hdots)
	(\ket{\psi^{(0)}}+\lambda\ket{\psi^{(1)}}+\lambda^2\ket{\psi^{(2)}}+\hdots)
	\label{eq:maineq}
\end{align}
which is called (9.1.4) in the script.
The equation can be separated into the different orders of \(\lambda^n\), because they all
describe a different order of magnitude of corrective terms.
This gives us the equations
\begin{align}
	\hat{H}_0\ket{\psi^{(0)}}
	&=  W^{(0)}\ket{\psi^{(0)}}
	\label{eq:zeroOrder}\\
	\hat{H}_0\ket{\psi^{(1)}}+\hat{H}_1\ket{\psi^{(0)}}
	&=  W^{(0)}\ket{\psi^{(1)}}+ W^{(1)}\ket{\psi^{(0)}}
	\label{eq:oneOrder}\\
	\hat{H}_0\ket{\psi^{(2)}}+\hat{H}_1\ket{\psi^{(1)}}
	&=  W^{(0)}\ket{\psi^{(2)}}+ W^{(1)}\ket{\psi^{(1)}}+ W^{(2)}\ket{\psi^{(0)}}
	\label{eq:twoOrder}\\
	&\vdots\nonumber
\end{align}
being called (9.1.5), (9.1.6) and (9.1.7) in the script respectively.
Because we only want to deal with non-degenerate states in this case we can write
\begin{equation}
	\forall j \in \mathbb{N}_0: \quad
	\ket{\psi^{(j)}} = \sum_{k=1}^{\mathrm{dim}(\mathcal{H})}\gamma_k^{(j)}\ket{k}
	\label{eq:energyBasis}
.\end{equation}
With
\begin{equation}
	\hat{H}_0\ket{k}=E_k\ket{k}
	\label{eq:eigenEnergy}
\end{equation}
we're putting \autoref{eq:energyBasis} in \autoref{eq:zeroOrder}
\begin{align*}
	\hat{H}_0\ket{\psi^{(0)}}
	&\overset{(\ref{eq:energyBasis})}{=}
	\hat{H}_0 \sum_{k=1}^{\mathrm{dim}(\mathcal{H})}\gamma_k^{(0)}\ket{k}
	\overset{(\ref{eq:eigenEnergy})}{=}
	\sum_{k=1}^{\mathrm{dim}(\mathcal{H})} E_n\gamma_k^{(0)}\ket{k} \\
	&\overset{(\ref{eq:zeroOrder})}{=}
	 W^{(0)}\ket{\psi^{(0)}}
	\overset{(\ref{eq:energyBasis})}{=}
	 W^{(0)}\sum_{k=1}^{\mathrm{dim}(\mathcal{H})}\gamma_k^{(0)}\ket{k}
.\end{align*}
Because \((\ket{1},\ket{2}, \hdots, \ket{\mathrm{dim}(\mathcal{H})})\) is a basis and
\(E_m = E_n \Rightarrow m = n\) we know that there is just one \(k=n\) in that sum for which \[
	 W^{(0)} = E_n
\] and for all the other \(k\neq n\) we have \(\gamma_k^{(0)} = 0\) which gives us \[
\ket{\psi^{(0)}}=\ket{n}
.\] 
Inserting this into \autoref{eq:oneOrder} and multiplying from the left with \(\bra{n}\) gets us
 \begin{align}
	 E_n \braket{n|\psi^{(1)}} + \bra{n}\hat{H}_1\ket{n}
	 = E_n \braket{n|\psi^{(1)}} +  W^{(1)}  \nonumber\\
	 \Leftrightarrow  W^{(1)} = \bra{n}\hat{H}_1\ket{n}
	 \label{eq:WOne}
.\end{align}
We look again at \autoref{eq:oneOrder} and multiply it from the left with
\(\bra{k}\) where \(k\neq n\)
\begin{align}
	\bra{k}\hat{H}_0\ket{\psi^{(1)}}+\bra{k}\hat{H}_1\ket{\psi^{(0)}}
	&= E_n\braket{k|\psi^{(1)}} \nonumber\\
	\Leftrightarrow
	(E_n - E_k)\braket{k|\psi^{(1)}} &= \bra{k}\hat{H}_1\ket{n}
	\label{eq:braKketPsiOne}
.\end{align}
We can of course write \(\ket{\psi^{(1)}}\) as a linear combination of the basis vectors
\begin{align}
	\ket{\psi^{(1)}}
	&= \sum_{k=1}^{\mathrm{dim}(\mathcal{H})} \braket{k|\psi^{(1)}}\ket{k}
	\overset{(\ref{eq:braKketPsiOne})}{=} \braket{n|\psi^{(1)}}\ket{n}
	+ \sum_{k=1,k\neq n}^{\mathrm{dim}(\mathcal{H})}
	\frac{\bra{k}\hat{H}_1\ket{n}}{E_n-E_k} \ket{k}
	\label{eq:prePsiOne}
.\end{align}
Because we want \(\ket{\psi}\) to be normalised
\begin{align*}
	1
	&\overset{!}{=} \braket{\psi|\psi}
	\overset{(\ref{eq:energyBasis})}{=} 
	\overbrace{\braket{\psi^{(0)}|\psi^{(0)}}}^{=\braket{n|n}}
	+ \lambda(\braket{\psi^{(0)}|\psi^{(1)}}+\braket{\psi^{(1)}|\psi^{(0)}}) + \hdots \\
	\xLongleftrightarrow{\braket{n|n}=1}
	0 &= \lambda\mathrm{Re}(\braket{n|\psi^{(1)}}) + \hdots
.\end{align*}
Because different orders of \(\lambda\) again represent terms with vastly different
orders of magnitude we expect all those terms to be 0
\begin{equation}
\mathrm{Re}(\braket{n|\psi^{(1)}}) = 0
\label{eq:realzero}
.\end{equation}
Because the physics don't change by adding
a constant phase \(\ket{\psi}\mapsto e^{i\alpha}\ket{\psi}\)
we can also make the imaginary part 0 \(\Rightarrow\braket{n|\psi^{(1)}}=0\).
This simplifies \autoref{eq:prePsiOne} to
\begin{equation}
	\ket{\psi^{(1)}} = 
	\sum_{k=1,k\neq n}^{\mathrm{dim}(\mathcal{H})}
	\frac{\bra{k}\hat{H}_1\ket{n}}{E_n-E_k} \ket{k}
	\label{eq:psiOne}
.\end{equation}
We can insert this into \autoref{eq:twoOrder} and multiply it from the left with
\(\bra{n}\) to get
\begin{align}
	\overbrace{\bra{n}\hat{H}_0}^{=E_n\bra{n}}\ket{\psi^{(2)}}
	+\bra{n}\hat{H}_1\ket{\psi^{(1)}}
	=  \overbrace{W^{(0)}}^{=E_n}\braket{n|\psi^{(2)}}
	+ W^{(1)}\overbrace{\braket{n|\psi^{(1)}}}^{=0}
	+ W^{(2)}\overbrace{\braket{n|\psi^{(0)}}}^{=\braket{n|n}=1} \nonumber\\
	\xLongleftrightarrow{(\ref{eq:psiOne})}
	\sum_{k=1,k\neq n}^{\mathrm{dim}(\mathcal{H})}
	\frac{\bra{k}\hat{H}_1\ket{n}}{E_n-E_k} \bra{n}\hat{H}_1\ket{k}
	= W^{(2)} \nonumber\\
	\Leftrightarrow
	W^{(2)} = \sum_{k=1,k\neq n}^{\mathrm{dim}(\mathcal{H})}
	\frac{\left|\bra{k}\hat{H}_1\ket{n}\right|^2}{E_n-E_k} \quad \square
	\label{eq:WTwo}
.\end{align}
\section*{Problem 2: Perturbations in a Potential Well}
\subsection*{a,}
With \autoref{eq:WOne} and \[
\ket{n} =
\begin{cases}
	\frac{1}{\sqrt{a}}\sin\left(\frac{n\pi(x+a)}{2a}\right) &\text{for } |x| \leq a \\
	0 &\text{for } |x| > a
\end{cases}
\] we simply get
\begin{align*}
	W^{(1)}
	&= \bra{n}\alpha \hat{x}\ket{n}
	= \frac{\alpha}{a}
	\int_{-a}^{a}\mathrm{d}x\,
	x
	\sin^2\left(\frac{n\pi(x+a)}{2a}\right) \\
	&\overset{\phi=\frac{n\pi(x+a)}{2a}}{=} \frac{2\alpha}{n\pi}
	\int_{0}^{n\pi}\mathrm{d}\phi\,
	\left(\frac{2a}{n\pi}\phi-a\right)\sin^2(\phi) \\
	&= \frac{2\alpha}{n\pi} \left[
	\int_{0}^{n\pi}\mathrm{d}\phi\,\frac{2a}{n\pi}
	\phi\sin^2(\phi)
	-a\int_{0}^{n\pi}\mathrm{d}\phi\,
	\sin^2(\phi)
	\right] \\
	&= \frac{2\alpha}{n\pi} \left\{\frac{2a}{n\pi}
	\int_{0}^{n\pi}\mathrm{d}\phi\,
	\phi\sin^2(\phi)
	-a\left[
		\frac{1}{2}(\phi-\sin(\phi)\cos(\phi))
	\right]_{\phi=0}^{n\pi}
	\right\} \\
	&= \frac{2\alpha}{n\pi} \left\{\frac{2a}{n\pi}
		\left[
			\frac{1}{8}(2\phi(\phi-\sin(2\phi))-\cos(2\phi))
		\right]_{\phi=0}^{n\pi}
		-\frac{1}{2}an\pi
	\right\} \\
	&= \frac{2\alpha}{n\pi} \left\{\frac{2a}{n\pi}
			\frac{1}{4}n^2\pi^2
		-\frac{1}{2}an\pi
	\right\} \\
	&= \frac{2\alpha}{n\pi} \left\{\frac{an\pi}{2}
		-\frac{1}{2}an\pi
	\right\} \\
	&= 0
.\end{align*}
\subsection*{b,}
We want to determine the second order energy shift to the ground state energy (\(n=1\)).
With \[
E_n = n^2 \frac{\pi^2\hbar^2}{8ma^2}
\] we get
\begin{align*}
	W_1^{(2)}
	&= \sum_{k=2}^{\mathrm{dim}(\mathcal{H})}
	\frac{\left|\bra{k}\hat{H}_1\ket{1}\right|^2}{E_1-E_k} \\
	&= \frac{8ma^2}{\pi^2\hbar^2}
	\sum_{k=2}^{\mathrm{dim}(\mathcal{H})}
	\frac{1}{1-k^2}
	\left|\frac{\alpha}{a}\int_{-a}^{a}\mathrm{d}x\,
	\sin\left(\frac{\pi(x+a)}{2a}\right)x
	\sin\left(\frac{k\pi(x+a)}{2a}\right)\right|^2 \\
	&= \frac{8ma^2}{\pi^2\hbar^2}
	\sum_{k=2}^{\mathrm{dim}(\mathcal{H})}
	\frac{1}{1-k^2}
	\left|\frac{\alpha}{a}\int_{-a}^{a}\mathrm{d}x\,x
	\cos\left(\frac{\pi x}{2a}\right)
	\sin\left(\frac{k\pi(x+a)}{2a}\right)\right|^2
.\end{align*}
For an odd \(k\) the integral is
\begin{align*}
	\left|\int_{-a}^{a}\mathrm{d}x\,x
	\cos\left(\frac{\pi x}{2a}\right)
	\sin\left(\frac{k\pi(x+a)}{2a}\right)\right|
	&= \left|\int_{-a}^{a}\mathrm{d}x\,x
	\cos\left(\frac{\pi x}{2a}\right)
	\sin\left(\frac{k\pi x}{2a}+\frac{k}{2}\pi\right)\right| \\
	&= \bigg|\int_{-a}^{a}\mathrm{d}x\,\underbrace{x}_{\text{odd}}
	\underbrace{\cos\left(\frac{\pi x}{2a}\right)}_{\text{even}}
	\underbrace{\cos\left(\frac{k\pi x}{2a}\right)}_{\text{even}}\bigg| \\
	&= 0
.\end{align*}
For an even \(k\) the integral is
\begin{align*}
	\left|\int_{-a}^{a}\mathrm{d}x\,x
	\cos\left(\frac{\pi x}{2a}\right)
	\sin\left(\frac{k\pi(x+a)}{2a}\right)\right|
	&= \left|\int_{-a}^{a}\mathrm{d}x\,x
	\cos\left(\frac{\pi x}{2a}\right)
	\sin\left(\frac{k\pi x}{2a}+\frac{k}{2}\pi\right)\right| \\
	&= \left|\int_{-a}^{a}\mathrm{d}x\,x
	\cos\left(\frac{\pi x}{2a}\right)
	\sin\left(\frac{k\pi x}{2a}\right)\right| \\
	&\overset{\phi:=\frac{\pi x}{2a}}{=} \left|\frac{4a^2}{\pi^2}
	\int_{-\frac{\pi}{2}}^{\frac{\pi}{2}}\mathrm{d}\phi\,\phi
	\cos\left(\phi\right)
	\sin\left(2\frac{k}{2}\phi\right)\right| \\
	&= \left|\frac{4a^2}{\pi^2}
	\frac{4k}{(k^2-1)^2}\right| \\
	&= \left|\frac{16a^2}{\pi^2} \frac{k}{(k^2-1)^2}\right|
.\end{align*}
We insert this into our equation and get
\begin{align*}
	W_1^{(2)}
	&= \frac{8ma^2}{\pi^2\hbar^2}
	\sum_{k\in2\mathbb{N}}
	\frac{1}{1-k^2}
	\frac{256a^4}{\pi^4} \frac{k^2}{(k^2-1)^4}
	= \frac{2048ma^6}{\pi^6\hbar^2}
	\sum_{k\in2\mathbb{N}}
	\frac{k^2}{(1-k^2)^5} \\
	&= \frac{8192ma^6}{\pi^6\hbar^2}
	\sum_{k=1}^{\infty}
	\frac{k^2}{(1-4k^2)^5}
	= \frac{8192ma^6}{\pi^6\hbar^2}
	\frac{\pi^2(\pi^2-15)}{12288} \\
	&= -\frac{2ma^6(15-\pi^2)}{3\pi^4\hbar^2}
	< 0
.\end{align*}
\section*{Problem 3: Finite perturbation theory}
\subsection*{a,}
\begin{align*}
	[\tilde{H}(n)]_{r',r} = \bra{n,r'}\hat{H}_1\ket{n,r}
.\end{align*}
Looking at \(H_0\) we determine that for \(n=1\) we have \(r',r\in\set{1,2}\) 
and for \(n=2\) we only have \(r',r\in\set{1}\).
\begin{align*}
	[\tilde{H}(1)]_{r',r}
	&=
	\begin{cases}
		\lambda &\text{for } r'\neq r \\
		0 &\text{else}
	\end{cases} \\
	&=
	\begin{pmatrix}
		0 & \lambda \\
		\lambda & 0
	\end{pmatrix}
.\end{align*}
We calculate the eigenvalues of \(\tilde{H}(1)\)
 \begin{align*}
	\begin{vmatrix}
		-\kappa & \lambda \\
		\lambda & -\kappa
	\end{vmatrix}
	\overset{!}{=} 0 \\
	\Leftrightarrow \kappa^2-\lambda^2 = 0 \\
	\Leftrightarrow \kappa = \pm \lambda
\end{align*}
which gives us our first two values
\begin{align*}
	\Delta E(1,1) = W^{(1)}_{1,1} = \lambda \qquad
	\Delta E(1,2) = W^{(1)}_{1,2} = -\lambda
.\end{align*}
For \(n=2\) we get
\begin{align*}
	\tilde{H}(2) = \bra{2,1}\hat{H}_1\ket{2,1} = 0
\end{align*}
which has \(\kappa = 0\) as the only eigenvalue which gives us
\begin{align*}
	\Delta E(2,1) = W_{2,1}^{(1)} = 0
\end{align*}
as our third value.
\subsection*{b,}
We want to diagonalise \(H_0'\). We do this by diagonalising \[
	\begin{pmatrix}
		E & \lambda \\
		\lambda & E
	\end{pmatrix}
.\] 
First lets find the eigenvalues
\begin{align*}
	\begin{vmatrix}
		E-a & \lambda \\
		\lambda & E-a
	\end{vmatrix}
	&\overset{!}{=} 0 \\
	\Leftrightarrow (E-a)^2 &= \lambda^2 \\
	\Leftrightarrow a_{+,-} &= E\pm \lambda
\end{align*}
and then the eigenvectors
\begin{align*}
	\begin{pmatrix}
		E & \lambda \\
		\lambda & E
	\end{pmatrix}
	\begin{pmatrix}
		v_1 \\
		v_2
	\end{pmatrix}
	&\overset{!}{=}
	(E\pm\lambda)
	\begin{pmatrix}
		v_1 \\
		v_2
	\end{pmatrix} \\
	\Leftrightarrow
	\begin{pmatrix}
		E v_1 + \lambda v_2 \\
		\lambda v_1 + E v_2
	\end{pmatrix}
	&=
	\begin{pmatrix}
		(E\pm\lambda)v_1 \\
		(E\pm\lambda)v_2
	\end{pmatrix} \\
	\Leftrightarrow
	v_2 &= \pm v_1 \\
	\Rightarrow
	v_+ = \frac{1}{\sqrt{2}}(1\quad 1)^{\intercal}
		&\qquad v_- = \frac{1}{\sqrt{2}}(1\quad -1)^{\intercal}
.\end{align*}
\(\Rightarrow\) The eigenvectors of \(H_0'\) are
\begin{align*}
	\frac{1}{\sqrt{2}}
	\begin{pmatrix}
		1 \\
		1 \\
		0
	\end{pmatrix},
	\frac{1}{\sqrt{2}}
	\begin{pmatrix}
		1 \\
		-1 \\
		0
	\end{pmatrix}
	\text{ and }
	\begin{pmatrix}
		0 \\
		0 \\
		1
	\end{pmatrix}
.\end{align*}
\begin{align*}
	\tilde{H}(1)
	&= \frac{1}{2}
	\begin{pmatrix}
		1 & 1 & 0
	\end{pmatrix}
	\begin{pmatrix}
		0 & 0 & 0 \\
		0 & 0 & \lambda \\
		0 & \lambda & 0
	\end{pmatrix}
	\begin{pmatrix}
		1 \\ 1 \\ 0
	\end{pmatrix}
	= 0 \\
	\tilde{H}(2)
	&= \frac{1}{2}
	\begin{pmatrix}
		1 & -1 & 0
	\end{pmatrix}
	\begin{pmatrix}
		0 & 0 & 0 \\
		0 & 0 & \lambda \\
		0 & \lambda & 0
	\end{pmatrix}
	\begin{pmatrix}
		1 \\ -1 \\ 0
	\end{pmatrix}
	= 0 \\
	\tilde{H}(3)
	&=
	\begin{pmatrix}
		0 & 0 & 1
	\end{pmatrix}
	\begin{pmatrix}
		0 & 0 & 0 \\
		0 & 0 & \lambda \\
		0 & \lambda & 0
	\end{pmatrix}
	\begin{pmatrix}
		0 \\ 0 \\ 1
	\end{pmatrix}
	= 0
\end{align*}
\[
	\Longrightarrow \Delta E(1) = W_1^{(1)} = \Delta E(2) = W_2^{(1)}
	= \Delta E(3) = W_3^{(1)} = 0
.\] 
\end{document}

