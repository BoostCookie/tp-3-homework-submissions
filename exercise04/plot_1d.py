#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from math import tau

# a = 1
# omega = tau

def x_expect(t):
    return 1 / np.sqrt(2) * np.cos(tau * t)

# sqrt(var(x))
def x_uncert(t):
    return np.sqrt(1 - 0.5 * np.cos(tau * t)**2)

def main():
    plt.rc("text", usetex=True)
    plt.rc("text.latex", preamble=r"\usepackage{amsmath}\usepackage{braket}")
    plt.rc("font", family="serif")
    plt.rcParams.update({'font.size': 15})
    fig, ax = plt.subplots()

    t_minmax = 1.6
    tvals = np.linspace(-t_minmax, t_minmax, 1000)
    plt.plot(tvals, x_expect(tvals), label=r"$ \braket{x}_t $")
    plt.plot(tvals, x_uncert(tvals), label=r"$ (\Delta x)_t $")

    plt.yticks([-1/np.sqrt(2), 0, 1/np.sqrt(2), 1], [r"$-\frac{a}{\sqrt{2}}$", "0", r"$\frac{a}{\sqrt{2}}$", r"$a$"])
    x_tickvals = []
    x_ticklabels = []
    for t2 in range(-int(np.ceil(2*t_minmax)), int(np.ceil(2*t_minmax)) + 1):
        cur_t = 0.5 * t2
        if cur_t >= -t_minmax and cur_t <= t_minmax:
            x_tickvals.append(cur_t)
            cur_tickval = "$"
            if cur_t == 0:
                cur_tickval += "0$"
            else:
                if cur_t == -1:
                    cur_tickval += "-"
                elif cur_t != 1:
                    cur_tickval += str(cur_t)
                cur_tickval += r"\,T$"
            x_ticklabels.append(cur_tickval)
    plt.xticks(x_tickvals, x_ticklabels)

    ax.set_xlim([-t_minmax, t_minmax])

    plt.legend(loc="upper right")
    plt.grid()
    plt.minorticks_on()
    #plt.suptitle("title")
    #ax.set_title("title")
    plt.xlabel("$t$")
    plt.ylabel(r"$x$ and $\Delta x$")
    #plt.xscale("log")
    #plt.yscale("log")
    plt.tight_layout()
    plt.gcf().subplots_adjust(bottom=0.12)
    fig.savefig("plot_1d.pdf")
    #fig.savefig("plot.png", dpi=508) #200 dots/cm

if __name__ == "__main__":
    main()
