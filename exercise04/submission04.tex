\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage[style=iso]{datetime2}
\usepackage{amsmath, amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{ragged2e}
\usepackage[locale=DE]{siunitx}
\usepackage{braket}
\pdfsuppresswarningpagegroup=1

\title{Theoretische Physik 3: Quantenmechanik}
\subtitle{Submission for exercise 4}
\author{Stefan Gehr}

%\date{\today}
%\date{2020-05-17}
\date{2020-05-22}

\begin{document}
\maketitle
\section*{Problem 1: Time evolution of the harmonic oscillator}
When \(\psi_{\alpha}(x)\) is an eigenfunctions of \(\hat{H}\) we can write
\begin{align}
	\hat{H} \psi_{\alpha}(x) = E_{\alpha} \psi_{\alpha}(x)
.\end{align}
Given this Eigenfunction the general solution to the Schrödinger equation is
\begin{align}
	\psi_{\alpha}(x,t) = \psi_{\alpha}(x)\, e^{-\frac{i}{\hbar}E_{\alpha}t}
.\end{align}
The eigenfunctions \(\psi_0(x)\) and \(\psi_1(x)\) are given by
\begin{equation}
	\psi_n(x) = \frac{\pi^{-\frac{1}{4}}}{\sqrt{2^nn!a}}e^{-\frac{x^2}{2a^2}} \cdot \mathrm{H}_n\left(\frac{x}{a}\right)
\end{equation}
with the Hermite polynomials
\begin{equation}
	\mathrm{H}_n(y) = (-1)^n e^{y^2} \frac{\mathrm{d}^n}{\mathrm{d}y^n}e^{-y^2}
\end{equation}
and \(a = \sqrt{\frac{\hbar}{m\omega}}\).
Therefore we get
\begin{align*}
	\psi_0(x) &= \frac{\pi^{-\frac{1}{4}}}{\sqrt{a}}e^{-\frac{x^2}{2a^2}} \\
	\psi_1(x) &= \frac{\pi^{-\frac{1}{4}}}{\sqrt{2a}}e^{-\frac{x^2}{2a^2}} \cdot 2 \frac{x}{a}
.\end{align*}
\subsection*{a,}
\(\psi_0\) and \(\psi_1\) are both eigenfunctions. Therefore we can write
\begin{align}
	\psi_j(x,t) = \psi_j(x)\, e^{-\frac{i}{\hbar}E_j t}
.\end{align}
The possible energy levels are
\begin{align}
	E_n =& \left(n+\frac{1}{2}\right)\hbar\omega \\
	\Rightarrow E_0 =& \frac{1}{2}\hbar\omega \\
	\Rightarrow E_1 =& \frac{3}{2}\hbar\omega
.\end{align}
With our initial state
\begin{align}
	\psi(x,0) = \alpha \psi_0(x) + \beta \psi_1(x)
\end{align}
which is a superposition of eigenfunctions, we get
\begin{align}
	\psi(x,t) = \alpha \psi_0(x) e^{-i \frac{1}{2}\omega t} + \beta \psi_1(x) e^{-i \frac{3}{2}\omega t}
.\end{align}
Note that because of \(\int_{-\infty}^{\infty}\mathrm{d}x\,|\psi(x,t)|^2 \overset{!}{=} 1\) we know that
\begin{equation}
	|\alpha|^2+|\beta|^2=1
	\label{eq:alphabeta}
.\end{equation}
\subsection*{b,}
Using the equation
\begin{equation}
	\frac{\mathrm{d}}{\mathrm{d}t}\braket{a}_t = \frac{i}{\hbar} \braket{[\hat{H},\hat{A}]}
\end{equation}
it's easy to see that
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t} \braket{E}_t =& \frac{i}{\hbar} \braket{[\hat{H},\hat{H}]} = 0 \\
	\Rightarrow \braket{E}_t =&\, \mathrm{const.} \\
	\frac{\mathrm{d}}{\mathrm{d}t} (\Delta E)^2 =& \frac{\mathrm{d}}{\mathrm{d}t} \left( \braket{E^2} - \braket{E}^2 \right)
	= \frac{i}{\hbar} \braket{[\hat{H},\hat{H}^2]} - 2 \braket{E} \frac{\mathrm{d}}{\mathrm{d}t} \braket{E} = 0 \\
	\Rightarrow (\Delta E)^2 =&\, \mathrm{const}
.\end{align*}
So both the expectation value for the energy and its variance stay constant. The expected energy can be calculated with
\begin{align*}
	\braket{E}_t =& \braket{E} \\
	=& \int\mathrm{d}x\, \left[\alpha^*\psi_0^*(x) + \beta^*\psi_1^*(x)\right] \left[\hat{H}\left(\alpha\psi_0(x) + \beta\psi_1(x)\right)\right] \\
	=& \int\mathrm{d}x\, \left[\alpha^*\psi_0^*(x) + \beta^*\psi_1^*(x)\right] \left[\alpha E_0\psi_0(x) + \beta E_1\psi_1(x)\right] \\
	=& \int\mathrm{d}x\, \left[|\alpha|^2E_0|\psi_0(x)|^2 + |\beta|^2E_1|\psi_1(x)|^2 + \alpha^*\beta E_1\psi_0^*(x)\psi_1(x) + \alpha\beta^* E_0\psi_0(x)\psi_1^*(x) \right] \\
	=& |\alpha|^2 E_0 + |\beta|^2 E_1 +
	\underbrace{\int\mathrm{d}x\, \left[ \alpha^*\beta E_1\psi_0^*(x)\psi_1(x) + \alpha\beta^* E_0\psi_0(x)\psi_1^*(x) \right]}_{= 0 \mathrm{,\:because\:}\psi_0\perp\psi_1} \\
	=& |\alpha|^2 E_0 + |\beta|^2 E_1
	= \frac{1}{2}\hbar\omega \left(|\alpha|^2 + 3|\beta|^2\right) \\
	\overset{(\ref{eq:alphabeta})}{=}& \hbar\omega \left(\frac{3}{2} - |\alpha|^2\right)
.\end{align*}
Calculating the variance is analogous:
\begin{align*}
	(\Delta E)_t^2 =& (\Delta E)^2 = \braket{E^2} - \braket{E}^2
\end{align*}
\begin{align*}
	\braket{E^2} =& \int\mathrm{d}x\, \left[\alpha^*\psi_0^*(x) + \beta^*\psi_1^*(x)\right] \left[\hat{H}^2\left(\alpha\psi_0(x) + \beta\psi_1(x)\right)\right] \\
	=& \int\mathrm{d}x\, \left[\alpha^*\psi_0^*(x) + \beta^*\psi_1^*(x)\right] \left[\alpha E_0^2\psi_0(x) + \beta E_1^2\psi_1(x)\right] \\
	=& \int\mathrm{d}x\, \left[|\alpha|^2E_0^2|\psi_0(x)|^2 + |\beta|^2E_1^2|\psi_1(x)|^2 + \alpha^*\beta E_1^2\psi_0^*(x)\psi_1(x) + \alpha\beta^* E_0^2\psi_0(x)\psi_1^*(x) \right] \\
	=& |\alpha|^2 E_0^2 + |\beta|^2 E_1^2 +
	\underbrace{\int\mathrm{d}x\, \left[ \alpha^*\beta E_1^2\psi_0^*(x)\psi_1(x) + \alpha\beta^* E_0^2\psi_0(x)\psi_1^*(x) \right]}_{= 0 \mathrm{,\:because\:}\psi_0\perp\psi_1} \\
	=& |\alpha|^2 E_0^2 + |\beta|^2 E_1^2 \\
\end{align*}
\begin{align*}
	\Rightarrow (\Delta E)^2 =& |\alpha|^2 E_0^2 + |\beta|^2 E_1^2 - \left(|\alpha|^2 E_0 + |\beta|^2 E_1\right)^2 \\
	=& \left(|\alpha|^2-|\alpha|^4\right)E_0^2 + \left(|\beta|^2-|\beta|^4\right)E_1^2 - 2|\alpha|^2|\beta|^2 E_0 E_1 \\
	=& \frac{\hbar^2\omega^2}{4} \left[ |\alpha|^2\left(1-|\alpha|^2\right) + 9|\beta|^2 \left(1-|\beta|^2\right) -6|\alpha|^2|\beta|^2 \right] \\
	\overset{(\ref{eq:alphabeta})}{=}& \hbar^2\omega^2|\alpha|^2|\beta|^2
.\end{align*}
\subsection*{c,}
We can calculate the expected position values of the eigenfunctions with
\begin{align*}
	\braket{x}_{\psi_0} &= \int\mathrm{d}x\, \overbrace{\psi_0^*(x)}^{=\psi_0(x)} \left[x \psi_0(x)\right]
	= \frac{\pi^{-\frac{1}{2}}}{a}
	\int\mathrm{d}x\, \underbrace{\overbrace{x}^{\mathrm{odd}} \overbrace{e^{-\frac{x^2}{a^2}}}^{\mathrm{even}}}_{\mathrm{odd}} = 0 \\
	\braket{x}_{\psi_1} &= \int\mathrm{d}x\, \overbrace{\psi_1^*(x)}^{=\psi_1(x)} \left[x \psi_1(x)\right]
	= \frac{2\pi^{-\frac{1}{2}}}{a^3}
	\int\mathrm{d}x\, \underbrace{\overbrace{x^3}^{\mathrm{odd}} \overbrace{e^{-\frac{x^2}{a^2}}}^{\mathrm{even}}}_{\mathrm{odd}} = 0
\end{align*}
and also
\begin{align*}
	\braket{x^2}_{\psi_0} &= \int\mathrm{d}x\, \overbrace{\psi_0^*(x)}^{=\psi_0(x)} \left[x^2 \psi_0(x)\right]
	= \frac{\pi^{-\frac{1}{2}}}{a}
	\int\mathrm{d}x\, x^2 e^{-\frac{x^2}{a^2}} \\
  &= \frac{\pi^{-\frac{1}{2}}}{a} \left(-\frac{a^2}{2}\right)
	\int\mathrm{d}x\, \underbrace{x}_{u(x)} \underbrace{\left(-\frac{2x}{a^2}\right) e^{-\frac{x^2}{a^2}}}_{v'(x)}
	\overset{\mathrm{P.I.}}{=} \frac{a}{2\sqrt{\pi}}
	\int\mathrm{d}x\, e^{-\frac{x^2}{a^2}}
	= \frac{a^2}{2} \\
	\braket{x^2}_{\psi_1} &= \int\mathrm{d}x\, \overbrace{\psi_1^*(x)}^{=\psi_1(x)} \left[x^2 \psi_1(x)\right]
	= \frac{2\pi^{-\frac{1}{2}}}{a^3}
	\int\mathrm{d}x\, x^4 e^{-\frac{x^2}{a^2}} \\
  &= \frac{2\pi^{-\frac{1}{2}}}{a^3} \left(-\frac{a^2}{2}\right)
  \int\mathrm{d}x\, \underbrace{x^3}_{u(x)} \underbrace{\left(-\frac{2x}{a^2}\right) e^{-\frac{x^2}{a^2}}}_{v'(x)}
  \overset{\mathrm{P.I.}}{=} \frac{3}{a\sqrt{\pi}}
  \int\mathrm{d}x\, x^2 e^{-\frac{x^2}{a^2}}
  = \frac{3}{2} a^2
.\end{align*}
With these infos we can calculate the expected position for the whole wave function
\begin{align}
	\braket{x}_t &= \int\mathrm{d}x\, \psi^*(x,t) \left[x\psi(x,t)\right] \nonumber\\
				 &= \int\mathrm{d}x\,
				 \Big[\alpha^*\overbrace{\psi_0^*(x)}^{=\psi_0(x)}e^{i\frac{1}{2}\omega t}+\beta^*\overbrace{\psi_1^*(x)}^{=\psi_1(x)}e^{i\frac{3}{2}\omega t}\Big]
				 x\left[\alpha\psi_0(x)e^{-i\frac{1}{2}\omega t}+\beta\psi_1(x)e^{-i\frac{3}{2}\omega t}\right] \nonumber\\
				 &= |\alpha|^2\underbrace{\braket{x}_{\psi_0}}_{=0} + |\beta|^2\underbrace{\braket{x}_{\psi_1}}_{=0}
				 + \int\mathrm{d}x\, x \left[ \alpha\beta^*\psi_0(x)\psi_1(x)e^{i\omega t} + \alpha^*\beta\psi_0(x)\psi_1(x)e^{-i\omega t} \right] \nonumber\\
				 &= \left(\alpha\beta^*e^{i\omega t} + \alpha^*\beta e^{-i\omega t}\right)
				 \int\mathrm{d}x\, x\, \psi_0(x)\psi_1(x) \nonumber\\
				 &= \left[\alpha\beta^*e^{i\omega t} + \left(\alpha\beta^*e^{i\omega t}\right)^*\right] \sqrt{\frac{2}{\pi}} \frac{1}{a^2}
				 \int\mathrm{d}x\, x^2 e^{-\frac{x^2}{a^2}} \nonumber\\
				 &= 2\,\mathrm{Re}\left(\alpha\beta^*e^{i\omega t}\right) \sqrt{\frac{2}{\pi}} \frac{1}{a^2}
				 \frac{a^3\sqrt{\pi}}{2} \nonumber\\
				 &= \mathrm{Re}\left(\alpha\beta^*e^{i\omega t}\right) \sqrt{2}a
				 \overset{\alpha,\beta\in\mathbb{R}}{=} \sqrt{2}a\,\alpha\beta \cos(\omega t)
				 \label{eq:xexp}
\end{align}
and also the variance with
\begin{align*}
	\braket{x^2}_t &= \int\mathrm{d}x\, \psi^*(x,t) \left[x^2\psi(x,t)\right] \\
&= \int\mathrm{d}x\,
\left[\alpha^*\psi_0^*(x)e^{i\frac{1}{2}\omega t}+\beta^*\psi_1^*(x)e^{i\frac{3}{2}\omega t}\right]
x^2\left[\alpha\psi_0(x)e^{-i\frac{1}{2}\omega t}+\beta\psi_1(x)e^{-i\frac{3}{2}\omega t}\right] \\
&= |\alpha|^2\braket{x^2}_{\psi_0} + |\beta|^2\braket{x^2}_{\psi_1}
+ \int\mathrm{d}x\, x^2 \left[ \alpha\beta^*\psi_0(x)\psi_1(x)e^{i\omega t} + \alpha^*\beta\psi_0(x)\psi_1(x)e^{-i\omega t} \right] \\
&= \frac{a^2}{2}\left(|\alpha|^2 + 3|\beta|^2\right) + \left(\alpha\beta^*e^{i\omega t} + \alpha^*\beta e^{-i\omega t}\right)
\int\mathrm{d}x\, x^2\, \psi_0(x)\psi_1(x) \\
&= \frac{a^2}{2}\left(|\alpha|^2 + 3|\beta|^2\right) +
2\,\mathrm{Re}\left(\alpha\beta^*e^{i\omega t}\right) \sqrt{\frac{2}{\pi}} \frac{1}{a^2}
\int\mathrm{d}x\, \underbrace{\overbrace{x^3}^{\mathrm{odd}} \overbrace{e^{-\frac{x^2}{a^2}}}^{\mathrm{even}}}_{\mathrm{odd}} \\
&= \frac{a^2}{2}\left(|\alpha|^2 + 3|\beta|^2\right)
\end{align*}
\begin{align}
	\left(\Delta x\right)_t^2 &= \braket{x^2}_t - \braket{x}_t^2 \nonumber\\
&= \frac{a^2}{2}\left(|\alpha|^2 + 3|\beta|^2\right) - \mathrm{Re}\left(\alpha\beta^*e^{i\omega t}\right)^2 2a^2 \nonumber\\
&= a^2 \left[ \frac{1}{2}\left(|\alpha|^2+3|\beta|^2\right) - 2\,\mathrm{Re}\left(\alpha\beta^*e^{i\omega t}\right)^2\right] \nonumber\\
&\overset{(\ref{eq:alphabeta})}{=} a^2 \left[ \frac{3}{2}-|\alpha|^2 - 2\,\mathrm{Re}\left(\alpha\beta^*e^{i\omega t}\right)^2\right] \\
&\overset{\alpha,\beta\in\mathbb{R}}{=} a^2 \left[ \frac{3}{2}-\alpha^2\left(1+2\beta^2 \cos^2(\omega t)\right)\right]
\label{eq:xvar}
.\end{align}
\subsection*{d,}
To ensure \(\int_{-\infty}^{\infty}\mathrm{d}x\, |\psi(x,t)|^2\) = 1, \(\alpha\) and \(\beta\) need to have the relation \[
	|\alpha|^2 + |\beta|^2 = 1
.\] 
Assuming \(\alpha,\beta\in\mathbb{R}_{>0}\) we can write \[
\beta^2 = 1-\alpha^2
.\] 
Inserting this into \autoref{eq:xexp} we obtain \[
	\braket{x}_t = \sqrt{2}a\,\alpha\sqrt{1-\alpha^2} \cos(\omega t)
.\]
Let's find possible maxima and minima with
\begin{align*}
	\left(\frac{\mathrm{d}}{\mathrm{d}\alpha} \alpha\sqrt{1-\alpha^2}\right)
	&= \sqrt{1-\alpha^2} - \frac{\alpha^2}{\sqrt{1-\alpha^2}}
	= \frac{1-2\alpha^2}{\sqrt{1-\alpha^2}} \overset{!}{=} 0 \\
	\Leftrightarrow \alpha &= \pm \frac{1}{\sqrt{2}} \overset{\alpha>0}{=} \frac{1}{\sqrt{2}} \\
	\Rightarrow \beta &= \sqrt{1-\alpha^2} = \frac{1}{\sqrt{2}} = \alpha
.\end{align*}
So there is a maximum amplitude at \(\alpha=\beta=\frac{1}{\sqrt{2}}\).
When we insert these values into \autoref{eq:xvar} we get
\begin{align*}
	(\Delta x)_t^2 &= a^2 \left[ \frac{1}{2} \left(\frac{1}{2}+\frac{3}{2}\right) - 2\cdot\frac{1}{2}\cdot\frac{1}{2} \cos^2(\omega t) \right]	\\
				   &= a^2 \left[ 1 - \frac{1}{2}\cos^2(\omega t) \right]
.\end{align*} 
\begin{figure}[h]
	\Centering
	\includegraphics[scale=0.8]{plot_1d.pdf}
	\caption{\(\braket{x}_t\) and \(\Delta x\) as a function of time. \(T = \frac{2\pi}{\omega}.\)}
	\label{fig:plot}
\end{figure}
In \autoref{fig:plot} we can see that for this case the variance \((\Delta x)^2\) is highest when  \(\braket{x} = 0\) and lowest when \(\braket{x}\) is at its maxima or minima.
\section*{Problem 2: 3-dimensional Potential (Particle in a box)}
\subsection*{a,}
Let's look at the time-independent Schrödinger equation \[
	\left[\frac{\hat{p}^2}{2m}+\tilde{V}(\vec{r})\right]\psi(\vec{r}) = E\psi(\vec{r})
.\] We know that the momentum operator \[
\hat{p}^2
= -\hbar^2\nabla^2
= -\hbar^2 \partial_{a}\partial^{a}
= -\hbar^2\left(\frac{\partial^2}{\partial x^2}+\frac{\partial^2}{\partial y^2}+\frac{\partial^2}{\partial z^2}\right)
= \hat{p}_x^2 + \hat{p}_y^2 + \hat{p}_z^2
\] is a sum of the three degrees of freedom just like the potential \[
\tilde{V}(\vec{r}) = V(r^1)+V(r^2)+V(r^3)
.\] Therefore the time independent Schrödinger equation can be separated into three independent equations
\begin{equation}
	\left[-\frac{\hbar^2}{2m} (\partial_a)^2 + V(r^a)\right]\psi(\vec{r}) = E_a \psi(\vec{r})
\label{eq:tischr}
\end{equation}
These equations work when we write the wave function as \[
\psi(\vec{r}) = \prod_{a=1}^{3}\psi_a(r^a)
,\] because \[
(\partial_a)^2 \prod_{b=1}^{3}\psi_b(r^b) = \left[(\partial_a)^2\psi_a(r^a)\right] \prod_{b\neq a}\psi_b(r^b)
.\] Note that the term \(\prod_{b\neq a}\psi_b(r^b)\) would remain on both sides of \autoref{eq:tischr}, therefore having no effect on it.
\subsection*{b,}
In the lecture we derived the solutions to \autoref{eq:tischr}:
\begin{align*}
	\psi_{a,n_a}(r^a) = \sqrt{\frac{2}{L}} \sin\left(\frac{n_a\pi r_a}{L}\right) \\
	E_{a,n_a} = \frac{\hbar^2\pi^2n_a^2}{2mL^2}
.\end{align*}
This yields us
\begin{align*}
	\psi_n(\vec{r})
	=\prod_{a=1}^{3}\psi_{a,n_a}(r^a)
	=\sqrt{\frac{8}{L^3}}\prod_{a=1}^{3}\sin\left(\frac{n_a\pi r_a}{L}\right)
.\end{align*}
Entering this into \autoref{eq:tischr} we get
\begin{align*}
-\frac{\hbar^2}{2m}(\partial_a)^2\psi_n(\vec{r})
+ V(r^a)\psi_n(r^a)
= E_{a,n_a}\psi_n(\vec{r})
.\end{align*}
Note that \(V(r^a)\psi_n(r^a) = 0\), because either \(V(r^a) = 0\) (when \(r^a\in[0,L]\)), or  \(\psi_n(r^a)=0\) (when \(V(r^a)=\infty \Leftrightarrow r^a \not\in[0,L]\)).
We take a look at the second derivative
\begin{align*}
	(\partial_a)^2\psi_n(\vec{r})
	= \sqrt{\frac{8}{L^3}}\left[(\partial_a)^2\sin\left(\frac{n_a\pi r_a}{L}\right)\right]\prod_{b\neq a}\sin\left(\frac{n_b\pi r_b}{L}\right)
	= -\frac{n_a^2\pi^2}{L^2}\psi_n(\vec{r})
.\end{align*}
So overall for the possible energy values of each coordinate we get
\begin{align*}
	E_{a,n_a} = \frac{\hbar^2\pi^2}{2mL^2}n_a^2
\end{align*}
and for the whole energies of course
\begin{align*}
	E_{n_1,n_2,n_3}
	= \sum_{a=1}^{3} E_{a,n_a}
	= \frac{\hbar^2\pi^2}{2mL^2} \sum_{a=1}^3 n_a^2
	= \frac{\hbar^2\pi^2}{2mL^2}\left(n_1^2+n_2^2+n_3^2\right)
.\end{align*}
\subsection*{c,}
No, for example the states
\begin{align*}
	(1, 0, 0) \neq (0, 1, 0)
\end{align*}
have the same energy.
Another example would be states
\begin{align*}
	(0,3,0) \neq (1,2,2)
\end{align*}
which also have the same energy, because \(0^2+3^2+0^2 = 9 = 1^2+2^2+2^2\).
\section*{Problem 3: \(\delta\)-function potential}
\subsection*{a,}
The time independent Schrödinger equation is \[
	\psi''(x) = -\frac{2m}{\hbar^2}\left[E - \alpha\delta(x)\right]\psi(x)
.\] 
Integrating on both sides yields
\begin{align*}
	\int_{-\epsilon}^{\epsilon}\mathrm{d}x\, \psi''(x) =
	-\frac{2m}{\hbar^2}\left[E\int_{-\epsilon}^{\epsilon}\mathrm{d}x\, \psi(x)
	-\alpha\int_{-\epsilon}^{\epsilon}\mathrm{d}x\, \delta(x)\psi(x)\right] \\
	\Leftrightarrow \psi'(\epsilon)-\psi'(-\epsilon) = \frac{2m}{\hbar^2}\left[\alpha\psi(0) - E\int_{-\epsilon}^{\epsilon}\mathrm{d}x\, \psi(x)\right]
.\end{align*}
If we now take the \(\lim_{\epsilon\to 0}\) on both sides we get
\begin{align*}
	\lim_{\epsilon\to 0} \left[\psi'(\epsilon) - \psi'(-\epsilon)\right] = \frac{2m}{\hbar^2}\cdot \alpha \psi(0)
\end{align*}
which shows that \(\psi'(x)\) makes a jump of distance \(\frac{2m}{\hbar^2}\cdot\alpha\psi(0)\) at \(x=0\).
\subsection*{b,}
For \(x\neq0\) we get
\begin{align}
	-\frac{\hbar^2}{2m}\psi''(x) = E\psi(x) = -|E|\psi(x)
	\label{eq:quickschrod}
.\end{align}
The general solution to this differential equation is of course \[
	\psi(x)=A e^{kx} + Be^{-kx}
.\] Entering this into \autoref{eq:quickschrod} gets us
\begin{equation}
k = \sqrt{\frac{2m|E|}{\hbar^2}} > 0
\label{eq:k1}
\end{equation}
This means for \(x>0\) the left side diverges and for \(x<0\) the right side diverges.
This means for \(x>0\) we have to set \(A=0\) and for \(x<0\) we have to set \(B=0\).
We also want \(\psi(x)\) to be continuous at \(0\), which gives us \(A=B=\psi(0)\) and we can write
\begin{align*}
	\psi(x) &= Ae^{-k|x|} \\
	\psi'(x) &= -k\, \mathrm{sgn}(x) \psi(x)
.\end{align*}
We know that there has to be a jump in the derivate at \(x=0\):
\begin{align}
	-kA-kA = \frac{2m}{\hbar^2}\cdot\alpha A \nonumber\\
	\Leftrightarrow k = \frac{m|\alpha|}{\hbar^2}
.\end{align}
With this and \autoref{eq:k1} we now know
\begin{align*}
	\sqrt{\frac{2m|E|}{\hbar^2}} = \frac{m|\alpha|}{\hbar^2} \\
	\overset{E<0}{\Leftrightarrow} E = - \frac{m\alpha^2}{2\hbar^2}
.\end{align*}
This is the only possible Energy.
\section*{Problem 4: Double well potential}
\subsection*{a,}
Take a look at \autoref{fig:sketch}.
\begin{figure}[h]
	\Centering
	\includegraphics[scale=0.1]{4a-quick-sketch.jpg}
	\caption{Sketch of \(|\psi(x)|^2\)}
	\label{fig:sketch}
\end{figure}
\subsection*{b,}
Two possible superpositions:
\begin{align*}
	\psi = \psi_{\mathrm{even}} + \psi_{\mathrm{odd}}
	\psi = \psi_{\mathrm{even}} - \psi_{\mathrm{odd}}
.\end{align*}
\subsection*{c,}
\begin{align*}
	\psi(x,t) = \psi_{\mathrm{even}}(x) e^{-\frac{i}{\hbar}E_{\mathrm{even}}t} + \psi_{\mathrm{odd}}(x) e^{-\frac{i}{\hbar}E_{\mathrm{odd}}t}
.\end{align*}
At \(t=0\) the particle shall be localised in the left well. At \(T>0\)
\begin{align*}
	\psi(x,T) = (\psi_{\mathrm{even}}-\psi_{\mathrm{odd}})\underbrace{e^{i\alpha}}_{\mathrm{some\:phase}}
.\end{align*}
\end{document}

