#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import gamma

def prob(alpha, n):
    return abs(alpha)**(2*n) * np.exp(-abs(alpha)**2) / np_factorial(n)

def np_factorial(n_array):
    res = []
    for n in n_array:
        res.append(gamma(n+1))
    return np.array(res)

def main():
    plt.rc("text", usetex=True)
    plt.rc("font", family="serif")
    plt.rcParams.update({'font.size': 15})

    alpha_list = (1, 10)
    n_max_list = (8, 14)
    fig, ax_list = plt.subplots(1, len(alpha_list))

    for i in range(len(alpha_list)):
        n_max = n_max_list[i]
        n_vals = np.arange(0, n_max)
        alpha = alpha_list[i]
        ax = ax_list[i]
        ax.plot(n_vals, prob(alpha, n_vals), 'o', label=r"$\alpha=" + str(alpha) + "$")
        ax.set_title(r"$\alpha = " + str(alpha) + "$")
        ax.set_xlabel("n")
        ticknames = []
        for n in n_vals:
            if (n < 10) or (n % 5 == 0) or (n == n_vals[-1]):
                ticknames.append(str(n))
            else:
                ticknames.append("")
        ax.set_xticks(n_vals)
        ax.set_xticklabels(ticknames)
        ax.grid()
    
    ax_list[0].set_ylabel("$P(E_n)$")

    #plt.legend()
    #plt.minorticks_on()
    #plt.suptitle("title")
    #plt.xscale("log")
    #plt.yscale("log")
    fig.savefig("E7P2bPlot.pdf")
    #fig.savefig("E7P2bPlot.png", dpi=508) #200 dots/cm

if __name__ == "__main__":
    main()
