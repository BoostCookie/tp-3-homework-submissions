\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage[style=iso]{datetime2}
\usepackage{amsmath, amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{ragged2e}
\usepackage[locale=DE]{siunitx}
\usepackage{braket}
\usepackage{bbm}
\pdfsuppresswarningpagegroup=1

\title{Theoretische Physik 3: Quantenmechanik}
\subtitle{Submission for exercise 12}
\author{Stefan Gehr}

%\date{\today}
\date{2020-07-16}

\begin{document}
\maketitle
\section*{Problem 1: Spin-\texorpdfstring{\(\frac{1}{2}\)}{1/2}-particles in a magnetic field}
\subsection*{a,}
\begin{align*}
	\hat{H}
	&= -\frac{2\mu}{\hbar}\hat{\vec{S}}\cdot\vec{B}
	= -\frac{2\mu}{\hbar}\left(\hat{S}_xB_x + \hat{S}_yB_x + \hat{S}_zB_z\right)
	= -\mu\left(\hat{\sigma}_xB_x + \hat{\sigma}_yB_y + \hat{\sigma}_zB_z\right)
.\end{align*}
Remember in exercise 10 we had \[
\sigma_x =
\begin{pmatrix}
	0 & 1 \\
	1 & 0
\end{pmatrix}
\qquad
\sigma_y =
\begin{pmatrix}
	0 & -i \\
	i & 0
\end{pmatrix}
\qquad
\sigma_z =
\begin{pmatrix}
	1 & 0 \\
	0 & -1
\end{pmatrix}
,\] which means we can write \[
H = -\mu
\begin{pmatrix}
	B_z & B_x-iB_y \\
	B_x+iB_y & -B_z
\end{pmatrix}
.\] We find the eigenvalues with
\begin{align*}
	\det(H - E \mathbbm{1})
	&=
	\begin{vmatrix}
		-\mu B_z-E & -\mu(B_x-iB_y) \\
		-\mu(B_x+iB_y) & \mu B_z-E
	\end{vmatrix} \\
	&= (-\mu B_z -E)(\mu B_z-E)-\mu^2(B_x+iB_y)(B_x-iB_y) \\
	&= -\mu^2B_z^2 + E^2 -\mu^2(B_x^2+B_y^2) \\
	&= -\mu^2\vec{B}^2 + E^2
	\overset{!}{=} 0 \\
	\Leftrightarrow E_{1,2}
	&= \pm \mu|\vec{B}|
\end{align*}
and the normalised eigenvectors with
\begin{align*}
	H \vec{v}
	&= -\mu
	\begin{pmatrix}
		B_z & B_x-iB_y \\
		B_x+iB_y & -B_z
	\end{pmatrix}
	\begin{pmatrix}
		a \\
		b
	\end{pmatrix} \\
	&= -\mu
	\begin{pmatrix}
		aB_z + b(B_x-iB_y) \\
		a(B_x+iB_y) -bB_z
	\end{pmatrix}
	\overset{!}{=} \pm \mu|\vec{B}|
	\begin{pmatrix}
		a \\
		b
	\end{pmatrix}
\end{align*}
\begin{align*}
	\Rightarrow b
	&= a \frac{\mp|\vec{B}|-B_z}{B_x-iB_y} \\
	1
	&\overset{!}{=} |a|^2 + |b|^2
	= \left[1 + \frac{|\vec{B}|^2+B_z^2\pm 2|\vec{B}|B_z}{B_x^2+B_y^2}\right] |a|^2
	= 2\frac{|\vec{B}|^2\pm |\vec{B}|B_z}{B_x^2+B_y^2} |a|^2 \\
	\Rightarrow |a|^2
	&= \frac{1}{2} \frac{B_x^2+B_y^2}{|\vec{B}|^2\pm|\vec{B}|B_z}
\end{align*}
We choose \[
	a = \frac{B_x-iB_y}{\sqrt{2|\vec{B}|(|\vec{B}|-B_z)}}
\] to get the neat solution \[
b = \frac{\mp|\vec{B}|-B_z}{\sqrt{2|\vec{B}|(|\vec{B}|-B_z)}}
,\] so overall \[
\vec{v}_{1,2} = \frac{1}{\sqrt{2|\vec{B}|(|\vec{B}|-B_z)}}
\begin{pmatrix}
	B_x-iB_y \\
	\mp|\vec{B}|-B_z
\end{pmatrix}
.\] 
The normalised eigenstates are
\begin{equation}
	\ket{\psi_{1,2}} = \frac{1}{\sqrt{2|\vec{B}|(|\vec{B}|-B_z)}}\left[
		(B_x-iB_y)\ket{+_z}+(\mp|\vec{B}|-B_z)\ket{-_z}
	\right]
	\label{eq:eigenstates}
\end{equation}
with energy eigenvalues \(\pm\mu|\vec{B}|\).
\subsection*{b,}
We enter \(\vec{B}=B_x \vec{e}_x,\:B_y=B_z=0\) into \autoref{eq:eigenstates} and get
\begin{equation}
	\ket{\psi_{1,2}} = \frac{1}{\sqrt{2}}\left(\ket{+_z} \mp \ket{-_z}\right)
	\label{eq:eigensimply}
\end{equation}
with energy eigenvalues \(\pm\mu B_x\).
\subsection*{c,}
The \(\hat{S}_x\) eigenstate with eigenvalue \(\frac{\hbar}{2}\) is \[
	\ket{\psi_{S_x}} = \frac{1}{\sqrt{2}}\left(\ket{+_z}+\ket{-_z}\right)
.\] Starting with this state,
the probability to measure a positive energy eigenvalue
(the state \(\ket{\psi_1}\) from \autoref{eq:eigensimply}) is \[
	P(E>0)
	= \left|\braket{\psi_1|\psi_{S_x}}\right|^2
	= \left|-\frac{1}{2}+\frac{1}{2}\right|^2 = 0
.\] 
It would have been more interesting if the particle was prepared in the
\(\hat{S}_z\) eigenstate with eigenvalue \(+\frac{\hbar}{2}\).
This eigenstate is of course \[
	\ket{+}
.\] Starting with this state the probability to measure the positive energy eigenvalue is \[
P(E>0) = \left|\braket{\psi_1|+}\right|^2
= \frac{1}{2}
.\] 
\section*{Problem 2: Angular momentum relations}
\subsection*{a,}
We use
\begin{equation}
	\epsilon^{abc}J_bJ_c = i\hbar J^a
	\label{eq:jcom}
\end{equation}
to get
\begin{align}
	J_+J_-
	&= (J_x+iJ_y)(J_x-iJ_y) = J_x^2+J_y^2 -i[J_x,J_y]
	\overset{(\ref{eq:jcom})}{=} J_x^2+J_y^2 +\hbar J_z \nonumber\\
	\Rightarrow \vec{J}^2
	&= J_z^2+J_+J_--\hbar J_z \qquad \square
	\label{eq:2a}
.\end{align}
\subsection*{b,}
\begin{align*}
	\bra{j,m}J_+J_-\ket{j,m}
	&= \bra{j,m}J_-^{\dagger}J_-\ket{j,m}
	= |c_-|^2 \\
	&\overset{(\ref{eq:2a})}{=}
	\bra{j,m}\left(\vec{J}^2-J_z^2+\hbar J_z\right)\ket{j,m}
	= j(j+1)\hbar^2-\hbar^2m^2+\hbar^2m \\
	&= \hbar^2\left[j(j+1)-m(m-1)\right]
\end{align*}
\[
	\Rightarrow c_- = e^{i\phi}\,\hbar\sqrt{j(j+1)-m(m-1)}
\] with constant phase \(\phi\).
Because physics ain't changing with a phase offset we can choose
\(\phi=0\) to get \[
c_- = \hbar\sqrt{j(j+1)-m(m-1)}
.\] 
\section*{Problem 3: Rotation of a spin-\texorpdfstring{\(\frac{1}{2}\)}{1/2} spinor by \texorpdfstring{\(2\pi\)}{2 pi}}
\subsection*{a,}
The definition of \(\gamma_0\) should be \[
	\gamma_0 = \frac{2\mu_0}{\hbar}
.\] With this we get
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t}\vec{\mu}(t)
	&= -\gamma_0 B_0 \vec{e}_z\times\vec{\mu}(t) \\
	\Rightarrow \frac{\mathrm{d}}{\mathrm{d}t}\mu_z(t)
	&= 0 \qquad \Rightarrow \mu_z(t) = \text{const.} \qquad
	\mu_z(0)
	= 0 \qquad \Rightarrow \mu_z(t) = 0 \\
	\frac{\mathrm{d}}{\mathrm{d}t} \mu_x(t)
	&= \gamma_0B_0\mu_y \qquad 
	\frac{\mathrm{d}}{\mathrm{d}t} \mu_y(t)
	= -\gamma_0B_0\mu_x \\
	\Rightarrow \frac{\mathrm{d}^2}{\mathrm{d}t^2}\mu_x(t)
	&= \gamma_0B_0 \frac{\mathrm{d}}{\mathrm{d}t}\mu_y
	= -\gamma_0^2B_0^2 \mu_x
.\end{align*}
We solve the differential equation with \[
	\mu_x(t) = A\cos(\omega t) + B\sin(\omega t)
\] and get
\begin{align*}
	\omega
	&= \gamma_0B_0 \\
	\mu_x(0)
	&= \overline{\mu}_x \qquad\Rightarrow
	A = \overline{\mu}_x \\
	\frac{\mathrm{d}}{\mathrm{d}t}\mu_x(t)
	&= \gamma_0B_0\mu_y(t) \qquad \Leftrightarrow
	\mu_y(t) = \frac{1}{\gamma_0B_0}\frac{\mathrm{d}}{\mathrm{d}t}\mu_x(t)
	= -\overline{\mu}_x \sin(\omega t) + B\cos(\omega t) \\
	\mu_y(0)
	&= 0 \qquad \Rightarrow
	B = 0
.\end{align*}
So overall we get \[
	\vec{\mu}(t) = \overline{\mu}_x
	\begin{pmatrix}
		\cos(\gamma_0B_0 t) \\
		-\sin(\gamma_0B_0 t) \\
		0
	\end{pmatrix}
.\] After the time \(t^*\) with \[
\gamma_0B_0t^*=2\pi \quad \Longleftrightarrow\quad t^* = \frac{2\pi}{\gamma_0B_0}
\] \(\vec{\mu}(t)\) does return back to its initial value for the first time.
\subsection*{b,}
\begin{align*}
	i\hbar \frac{\partial}{\partial t}\ket{\psi(t)}
	&= \hat{H}\ket{\psi(t)} \\
	\ket{\psi(t)}
	&= \frac{1}{\sqrt{2}}\left(\ket{+(t)}+\ket{-(t)}\right) \\
	i\hbar \frac{\partial}{\partial t}\ket{+(t)}
	&= -\mu_0B_0\hat{\sigma}_z\ket{+(t)} \\
	\Rightarrow \ket{+(t)}
	&= e^{\frac{i}{\hbar}\mu_0B_0t}\ket{+} \\
	\ket{-(t)} &= e^{-\frac{i}{\hbar}\mu_0B_0t}\ket{-} \\
\end{align*}
\[
	\Rightarrow \ket{\psi(t)} = \frac{1}{\sqrt{2}}
	\left(e^{\frac{i}{\hbar}\mu_0B_0t}\ket{+}
	+e^{-\frac{i}{\hbar}\mu_0B_0t}\ket{-}\right)
.\] We look at \(\ket{\psi(t^*)} = \ket{\psi(\frac{\pi\hbar}{\mu_0B_0})}\) 
and get \[
	\ket{\psi(t^*)}
	= \frac{1}{\sqrt{2}}\left(e^{i\pi}\ket{+}+e^{-i\pi}\ket{-}\right)
	= -\ket{\psi(0)}
.\] Seems to be the opposite of the previous result and at \(2t^*\) it should
return to the original state.
\subsection*{c,}
\begin{align*}
	\bra{\psi(t)}\hat{\sigma}_x\ket{\psi(t)}
	&= \frac{1}{2}
	\left(e^{-\frac{i}{\hbar}\mu_0B_0t}\bra{+}
	+e^{\frac{i}{\hbar}\mu_0B_0t}\bra{-}\right)
	\hat{\sigma}_x
	\left(e^{\frac{i}{\hbar}\mu_0B_0t}\ket{+}
	+e^{-\frac{i}{\hbar}\mu_0B_0t}\ket{-}\right) \\
	&= \frac{1}{2}
	\left(e^{-\frac{i}{\hbar}\mu_0B_0t}\bra{+}
	+e^{\frac{i}{\hbar}\mu_0B_0t}\bra{-}\right)
	\left(e^{\frac{i}{\hbar}\mu_0B_0t}\ket{-}
	+e^{-\frac{i}{\hbar}\mu_0B_0t}\ket{+}\right) \\
	&= \frac{1}{2}\left(
	e^{-2 \frac{i}{\hbar}\mu_0B_0t}+e^{2 \frac{i}{\hbar}\mu_0B_0t}\right)
	= \cos\left(2 \frac{\mu_0B_0}{\hbar}t\right)
\end{align*}
\begin{align*}
	\bra{\psi(t)}\hat{\sigma}_y\ket{\psi(t)}
	&= \frac{1}{2}
	\left(e^{-\frac{i}{\hbar}\mu_0B_0t}\bra{+}
	+e^{\frac{i}{\hbar}\mu_0B_0t}\bra{-}\right)
	\hat{\sigma}_y
	\left(e^{\frac{i}{\hbar}\mu_0B_0t}\ket{+}
	+e^{-\frac{i}{\hbar}\mu_0B_0t}\ket{-}\right) \\
	&= \frac{1}{2}
	\left(e^{-\frac{i}{\hbar}\mu_0B_0t}\bra{+}
	+e^{\frac{i}{\hbar}\mu_0B_0t}\bra{-}\right)
	\left(e^{\frac{i}{\hbar}\mu_0B_0t}i\ket{-}
	-e^{-\frac{i}{\hbar}\mu_0B_0t}i\ket{+}\right) \\
	&= \frac{i}{2}\left(
	-e^{-2 \frac{i}{\hbar}\mu_0B_0t}+e^{2 \frac{i}{\hbar}\mu_0B_0t}\right)
	= -\sin\left(2 \frac{\mu_0B_0}{\hbar}t\right)
\end{align*}
\begin{align*}
	\bra{\psi(t)}\hat{\sigma}_z\ket{\psi(t)}
	&= \frac{1}{2}
	\left(e^{-\frac{i}{\hbar}\mu_0B_0t}\bra{+}
	+e^{\frac{i}{\hbar}\mu_0B_0t}\bra{-}\right)
	\hat{\sigma}_z
	\left(e^{\frac{i}{\hbar}\mu_0B_0t}\ket{+}
	+e^{-\frac{i}{\hbar}\mu_0B_0t}\ket{-}\right) \\
	&= \frac{1}{2}
	\left(e^{-\frac{i}{\hbar}\mu_0B_0t}\bra{+}
	+e^{\frac{i}{\hbar}\mu_0B_0t}\bra{-}\right)
	\left(e^{\frac{i}{\hbar}\mu_0B_0t}\ket{+}
	-e^{-\frac{i}{\hbar}\mu_0B_0t}\ket{-}\right) \\
	&= \frac{1}{2}\left(
	1 - 1 \right)
	= 0
.\end{align*}
\[
	\Rightarrow \braket{\hat{\vec{\mu}}} = \mu_0
	\begin{pmatrix}
		\cos(\gamma_0B_0 t) \\
		-\sin(\gamma_0B_0 t) \\
		0
	\end{pmatrix}
.\] The result is identical to the one for classical mechanics.
So even though in \textbf{b} we've shown that it takes \(2t^*\) to get back to the original state,
the state at \(t^*\) yields the same eigenvalue.

\end{document}

