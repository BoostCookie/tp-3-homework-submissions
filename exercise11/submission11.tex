\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage[style=iso]{datetime2}
\usepackage{amsmath, amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{ragged2e}
\usepackage[locale=DE]{siunitx}
\usepackage{braket}
\pdfsuppresswarningpagegroup=1

\title{Theoretische Physik 3: Quantenmechanik}
\subtitle{Submission for exercise 11}
\author{Stefan Gehr}

%\date{\today}
\date{2020-07-09}

\begin{document}
\maketitle
\section*{Problem 1: Spherically symmetrical potential well}
We have the Schrödinger equation for a spherical potential
(\(V(\vec{r})=V(|\vec{r}|)=V(r)\))
\begin{equation}
	i\hbar \frac{\mathrm{d}}{\mathrm{d}t}\psi(\vec{r},t)
	= -\frac{\hbar^2}{2\mu}\nabla^2\psi(\vec{r},t) + V(r)\psi(\vec{r},t)
	\label{eq:schroed}
\end{equation}
In 3D the Laplacian \(\nabla^2=\Delta\) can be written in spherical coordinates as
%\begin{equation}
%	\Delta = \frac{1}{r^2}\frac{\partial}{\partial r}
%	r^2 \frac{\partial}{\partial r}
%	+ \frac{1}{r^2\sin(\theta)}\frac{\partial}{\partial \theta}
%	\sin\theta \frac{\partial}{\partial\theta}
%	+ \frac{1}{r^2\sin^2\theta}\frac{\partial^2}{\partial\phi^2}
%	\label{eq:sphericalLaplace}
%.\end{equation}
\begin{equation}
	\Delta =
	\frac{\partial^2}{\partial r^2}
	+ \frac{2}{r}\frac{\partial}{\partial r}
	+ \frac{1}{r^2}\left(
		\frac{\partial^2}{\partial\theta^2}
		+ \frac{\cos\theta}{\sin\theta}\frac{\partial}{\partial\theta}
		+ \frac{1}{\sin^2\theta}\frac{\partial^2}{\partial\phi^2}
	\right)
.\end{equation}
\subsection*{a,}
First we consider a free particle (\(V=0\)).
The time-independent Schrödinger equation is of course
\begin{align*}
	-\frac{\hbar^2}{2\mu}\nabla^2\psi(\vec{r}) = E\psi(\vec{r})
.\end{align*}
We use the Ansatz \(\psi(\vec{r})=R_l(r)Y_{l,m}(\theta,\phi)\) \[
	-\frac{\hbar^2}{2\mu}\left[
	\frac{\partial^2}{\partial r^2}
	+ \frac{2}{r}\frac{\partial}{\partial r}
	+ \frac{1}{r^2}\left(
		\frac{\partial^2}{\partial\theta^2}
		+ \frac{\cos\theta}{\sin\theta}\frac{\partial}{\partial\theta}
		+ \frac{1}{\sin^2\theta}\frac{\partial^2}{\partial\phi^2}
	\right)
	\right]R_l(r)Y_{l,m}(\theta,\phi)
	= E\,R_l(r)Y_{l,m}(\theta,\phi)
.\] With the property
\begin{equation}
	\left(\frac{\partial^2}{\partial\theta^2}
		+\frac{\cos\theta}{\sin\theta}\frac{\partial}{\partial\theta}
	+\frac{1}{\sin^2\theta}\frac{\partial^2}{\partial\phi^2}\right)
	Y_{l,m}(\theta,\phi) = -l(l+1)Y_{l,m}(\theta,\phi)
	\label{eq:spherefun}
\end{equation}
of \(Y_{l,m}(\theta,\phi)\) our equation goes to
\begin{align*}
	\frac{\partial^2}{\partial r^2} R_l(r)
	+ \frac{2}{r}\frac{\partial}{\partial r} R_l(r)
	- \frac{1}{r^2} R_l(r) \,
	l(l+1)
	&= -\frac{2\mu}{\hbar^2}E\,R_l(r) \\
	\Leftrightarrow
	r^2\frac{\partial^2}{\partial r^2} R_l(r)
	+ 2r\frac{\partial}{\partial r} R_l(r)
	+ \left(\frac{2\mu}{\hbar^2}Er^2-l(l+1)\right)R_l(r)
	&= 0
.\end{align*}
We define
\begin{align*}
	\tilde{r}
	&:= \frac{\sqrt{2\mu E}}{\hbar}r \\
	\tilde{R}_l(\tilde{r})
	&:= R_l\left(\frac{\hbar}{\sqrt{2\mu E}}\tilde{r}\right)
	= R_l(r)
\end{align*}
which has the properties
\begin{align*}
	\tilde{r}\frac{\partial}{\partial\tilde{r}}\tilde{R}_l(\tilde{r})
	&= \frac{\sqrt{2\mu E}}{\hbar}r
	\frac{\partial R_l(r)}{\partial r}\frac{\partial r}{\partial\tilde{r}}
	= r \frac{\partial}{\partial r}R_l(r)
.\end{align*}
Therefore we can write our equation as
\begin{equation}
	\tilde{r}^2\frac{\partial^2}{\partial \tilde{r}^2} \tilde{R}_l(\tilde{r})
	+ 2\tilde{r}\frac{\partial}{\partial \tilde{r}} \tilde{R}_l(\tilde{r})
	+ \left(\tilde{r}^2-l(l+1)\right)\tilde{R}_l(\tilde{r})
	= 0 \qquad\square
	\label{eq:diffeq}
\end{equation}
\subsection*{b,}
The solutions to \autoref{eq:diffeq} are apparently \[
	j_l(\tilde{r})=(-1)^l\tilde{r}^l
	\left(\frac{1}{\tilde{r}}\frac{\mathrm{d}}{\mathrm{d}\tilde{r}}\right)^l
	\left(\frac{\sin\tilde{r}}{\tilde{r}}\right)
	\qquad
	y_l(\tilde{r})=(-1)^{l+1}\tilde{r}^l
	\left(\frac{1}{\tilde{r}}\frac{\mathrm{d}}{\mathrm{d}\tilde{r}}\right)^l
	\left(\frac{\cos\tilde{r}}{\tilde{r}}\right)
.\] 
But of those two apparently only \(j_l\) has the property that \[
	\lim_{\tilde{r}\to 0} j_l(\tilde{r}) \neq \pm\infty
.\] This can be shown by doing a Taylor expansion of both functions,
computing the functions for the first few values of \(l\),
recognising a pattern
and proving it by induction.
Because then you should get \[
	j_l(\tilde{r}) = (-1)^l\tilde{r}^l\cdot 2^l\sum_{n=l}^{\infty}(-1)^n
	\frac{n!}{(n-l)!}\frac{\tilde{r}^{2(n-l)}}{(2n+1)!}
\] which of course has the property \[
	\lim_{\tilde{r}\to 0} j_l(\tilde{r}) = 0
,\] while for \(y_l(\tilde{r})\) you should get something that blows up. \\
Now you might ask yourself: "OK, where is this derivation you're constantly
talking about? Let me see it!" But I have to disappoint you.
I won't show it to you. Because I didn't do it and I don't want to do it. \\
If you have done the derivation and you have written it in \LaTeX{} then
please send it to me, or even better, do a merge request. \\
Anyway the solution of the time-independent Schrödinger equation is then \[
	\psi(\vec{r}) =
	\begin{cases}
		c_n\,j_l\left(\frac{\sqrt{2\mu E}}{\hbar}r\right)\,
		Y_{l,m}(\theta,\phi), &\text{for } r\leq a \\
		0,	&\text{for } r>a
	\end{cases}
\] with \(c_n\in\mathbb{C}\).
\subsection*{c,}
The boundary condition is
\begin{align*}
	\psi(r=a)
	&\overset{!}{=} 0 \\
	\Leftrightarrow j_l\left(\frac{\sqrt{2\mu E}}{\hbar}a\right)
	&\overset{!}{=} 0
.\end{align*}
If \(\tilde{r}_{n,l}\) with  \(n\in\mathbb{N}\) are the zeros of \(j_l(\tilde{r})\) we have
\begin{align*}
	\frac{\sqrt{2\mu E}}{\hbar}a
	&= \tilde{r}_{n,l} \\
	\Leftrightarrow
	E &= E_{n,l} = \frac{\hbar^2 \tilde{r}_{n,l}^2}{2\mu a}
\end{align*} as values for the allowed energies.
\subsection*{d,}
The Energies are independent of \(m\), so for fixed values of \(n\) and \(l\) 
each different \(m\) gives the same energy.
This means the degeneracy of \(\ket{n,l,m}\) is \(2l+1\) since that
is the number of different values \(m\) can take for each \(l\).
Because \(m \in \{-l, -l+1, \hdots, -1, 0, 1, \hdots, l-1, l\}\).
\section*{Problem 2: Spherically symmetrical potential}
\subsection*{a,}
%\begin{align*}
%	\hat{L}^a
%	&= \epsilon^{abc}\hat{r}_b\hat{p}_c
%	= \frac{\hbar}{i}\epsilon^{abc}r_b\partial_c \\
%	\hat{L}^2
%	&= \hat{L}_a\hat{L}^a
%	= -\hbar^2\epsilon^{abc}r_b\partial_c \, \epsilon_{a}^{\phantom{a}de}r_d\partial_e
%	= -\hbar^2\left(\delta^{bd}\delta^{ce}-\delta^{be}\delta^{cd}\right)
%	r_b\partial_cr_d\partial_e \\
%	&= -\hbar^2\left(\delta^{bd}\delta^{ce}-\delta^{be}\delta^{cd}\right)
%	\left(r_b\delta_{cd}\partial_e+r_br_d\partial_c\partial_e\right) \\
%	&= -\hbar^2\left(r^a\partial_a - r^ar^b\partial_b\partial_a\right) \\
%	\hat{L}^2
%	&= -\hbar^2 \left[\vec{r}\cdot\vec{\nabla}
%	- \vec{r}\cdot(\vec{r}\cdot\vec{\nabla})\vec{\nabla}\right] \\
%%	&= \hbar^2\big[
%%		x(x\partial_x+y\partial_y+z\partial_z)\partial_x
%%		+ y(x\partial_x+y\partial_y+z\partial_z)\partial_y
%%		+ z(x\partial_x+y\partial_y+z\partial_z)\partial_z \\
%%		&-x\partial_x-y\partial_y-z\partial_z
%%	\big] \\
%%	&= \hbar^2\big[
%%		x^2(\partial_x)^2+y^2(\partial_y)^2+z^2(\partial_z)^2
%%	+ 2xy\partial_x\partial_y+2xz\partial_x\partial_z+2yz\partial_x\partial_z
%%	- x\partial_x-y\partial_y-z\partial_z\big] \\
%%	\frac{\partial f(r)}{\partial r^a}
%%	&= f'(r) \frac{\partial r}{\partial r^a}
%%	= f'(r) \frac{r^a}{r} \\
%%	\frac{\partial^2f(r)}{(\partial r^a)^2}
%%	&= f''(r) \frac{(r^a)^2}{r^2} + f'(r) \frac{r-\frac{(r^a)^2}{r}}{r^2}
%%	= f''(r) \frac{(r^a)^2}{r^2} + f'(r)\left[\frac{1}{r}-\frac{(r^a)^2}{r^3}\right]
%%	\frac{\partial^2f(r)}{\partial}
%\end{align*}
%\begin{align*}
%	\vec{\nabla}f(r)
%	&= f'(r)\vec{\nabla}r = f'(r) \frac{\vec{r}}{r} \\
%	\vec{\nabla}\psi(\vec{r})
%	&=
%	\begin{pmatrix}
%		f(r) + \frac{x^2}{r}f'(r) \\
%		f(r) + \frac{y^2}{r}f'(r) \\
%		3f(r) + 3 \frac{z^2}{r}f'(r)
%	\end{pmatrix}
%	= f(r)
%	\begin{pmatrix}
%		1 \\ 1 \\ 3
%	\end{pmatrix}
%	+ \frac{f'(r)}{r}
%	\begin{pmatrix}
%		x^2 \\ y^2 \\ 3z^2
%	\end{pmatrix} \\
%	\vec{r}\cdot\vec{\nabla}\psi(\vec{r})
%	&= (x+y+3z)f(r) + \frac{f'(r)}{r}(x^3+y^3+3z^3)
%\end{align*}
%\begin{align*}
%	\vec{r}\cdot\vec{\nabla}\left(f(r)+\frac{f'(r)}{r}x^2\right)
%	&= rf'(r) + x^2f''(r) + f'(r)\vec{r}\cdot
%	\begin{pmatrix}
%		\frac{2xr-\frac{x^3}{r}}{r^2} \\
%		- \frac{x^2y}{r^3} \\
%		- \frac{x^2z}{r^3}
%	\end{pmatrix} \\
%	&= rf'(r) + x^2f''(r) + \frac{f'(r)}{r^3}
%	\left(2x^2r^2-x^4-x^2y^2-x^2z^2\right) \\
%	&= rf'(r) + x^2f''(r) + \frac{f'(r)}{r^3}
%	\left(2x^2r^2-x^2r^2\right) \\
%	&= rf'(r) + x^2f''(r) + f'(r)\frac{x^2}{r}
%\end{align*}
%\begin{align*}
%	\Rightarrow \hat{L}^2\psi(\vec{r})
%	&= \hbar^2\bigg[ rxf'(r) + x^3f''(r) + f'(r)\frac{x^3}{r} \\
%	&+ ryf'(r) + y^3f''(r) + f'(r)\frac{y^3}{r} \\
%	&+ 3rzf'(r) + 3z^3f''(r) + 3f'(r)\frac{z^3}{r} \\
%	&- (x+y+3z)f(r) - \frac{f'(r)}{r}(x^3+y^3+3z^3) \bigg] \\
%	&= \hbar^2
%		[f'(r)\,r-f(r)](x+y+3z)
%			+ \hbar^2(x^3+y^3+3z^3)f''(r)
%.\end{align*}
%% That led me absolutely nowhere
Expressed in spherical coordinates we have \[
	\psi(\vec{r})=r(\cos(\phi)\sin(\theta)+\sin(\phi)\sin(\theta)+3\cos(\theta))f(r)
\] and \[
\hat{L}^2 = -\hbar^2\left[
	\frac{1}{\sin(\theta)}\frac{\partial}{\partial \theta}
	\left(\sin(\theta) \frac{\partial}{\partial\theta}\right)
	+\frac{1}{\sin^2(\theta)} \frac{\partial^2}{\partial\phi^2}
\right]
.\] 
Therefore we get
\begin{align*}
	\hat{L}^2\psi(\vec{r})
	&= \hbar^2r\,f(r)\left[
		\frac{\cos(\phi)}{\sin(\theta)} +\frac{\sin(\phi)}{\sin(\theta)}
		-(\cos(\phi)+\sin(\phi)) \frac{\cos^2(\theta)-\sin^2(\theta)}{\sin(\theta)}
		+6\cos(\theta)
	\right] \\
	&= \hbar^2r\,f(r)\left\{\frac{\cos(\phi)+\sin(\phi)}{\sin(\theta)}\left[
			1 - (\cos^2(\theta)-\sin^2(\theta))
	\right]+6\cos(\theta)\right\} \\
	&= 2\hbar^2r\,f(r)\left\{\sin(\theta)[\cos(\phi)+\sin(\phi)]
	+ 3\cos(\theta)\right\} \\
	&= 2\hbar^2f(r)(x+y+3z) \\
	&= 2\hbar^2\psi(\vec{r})
.\end{align*}
\(\psi(\vec{r})\) is an eigenstate with
\begin{align*}
	\hat{L}^2\psi(\vec{r})
	&= \hbar^2l(l+1)\psi(\vec{r}) \\
	\Rightarrow l(l+1)
	&= 2 \\
	\Rightarrow l &= 1
.\end{align*}
\subsection*{b,}
\begin{align*}
	Y_{1,0}(\theta,\phi)
	&= \sqrt{\frac{3}{4\pi}}\cos(\theta) \\
	Y_{1,1}(\theta,\phi)
	&= \sqrt{\frac{3}{8\pi}}\sin(\theta)e^{-i\phi} \\
	Y_{1,-1}(\theta,\phi)
	&= -\sqrt{\frac{3}{8\pi}}\sin(\theta)e^{i\phi} \\
	Y_{1,1}(\theta,\phi)+Y_{1,-1}(\theta,\phi)
	&= \frac{1}{i}\sqrt{\frac{3}{2\pi}}\sin(\theta)\sin(\phi) \\
	Y_{1,1}(\theta,\phi)-Y_{1,-1}(\theta,\phi)
	&= \sqrt{\frac{3}{2\pi}}\sin(\theta)\cos(\phi)
\end{align*}
\begin{align*}
	\Rightarrow\psi(\vec{r})
	&= rf(r) \sqrt{\frac{2\pi}{3}}\left[
		Y_{1,1}(\theta,\phi)-Y_{1,-1}(\theta,\phi)
		+iY_{1,1}(\theta,\phi)+iY_{1,-1}(\theta,\phi)
		+3\sqrt{2}Y_{1,0}(\theta,\phi)
	\right] \\
	&= rf(r) \sqrt{\frac{2\pi}{3}}\left[
		(1+i)\,Y_{1,1}(\theta,\phi)+(-1+i)\,Y_{1,-1}(\theta,\phi)+3\sqrt{2}\,Y_{1,0}(\theta,\phi)
	\right]
.\end{align*}
We'll now use the fact that the functions \[
	\int_0^{2\pi}\mathrm{d}\phi\int_{0}^{\pi}\mathrm{d}\theta\,\sin(\theta)\,
		Y_{l,m}^*(\theta,\phi)\,Y_{l',m'}(\theta,\phi)
		= \delta_{l,l'}\delta_{m,m'}
\] are perpendicular to one another.
The probability to detect the eigenstate \(l=1,m=0\) is
\begin{align*}
	P(1,0)
	&= |\braket{1,0|\psi}|^2
	= \left|\int\mathrm{d}^3r\,
	Y_{1,0}^*(\theta,\phi)\psi(\vec{r})
	\right|^2
	= 12\pi \left|\int_0^{\infty}\mathrm{d}r\,r^3f(r)\right|^2
.\end{align*}
Similarly we get
\begin{align*}
	P(1,1)
	&= \frac{4\pi}{3}\left|\int_0^{\infty}\mathrm{d}r\,r^3f(r)\right|^2 \\
	&= P(1,-1)
.\end{align*}
Of course \(\psi(\vec{r}\) has to be normalised which gives us
\begin{align*}
	P(1,0) + P(1,1) + P(1,-1) \overset{!}{=} 1 \\
	\Leftrightarrow \left|\int_0^{\infty}\mathrm{d}r\,r^3f(r)\right|^2
	= \frac{1}{12\pi + \frac{8}{3}\pi}
	= \frac{3}{44\pi}
.\end{align*}
So overall we have \[
	P(1,0) = \frac{9}{11} \qquad
	P(1,1) = P(1,-1) = \frac{1}{11}
.\] 
\subsection*{c,}
We use \[
	\nabla^2 = \frac{1}{r^2} \frac{\partial}{\partial r}\left(r^2 \frac{\partial}{\partial r}\right)
	- \frac{\hat{L}^2}{\hbar^2r^2}
.\] 
\begin{align*}
	-\frac{\hbar^2}{2m}\nabla^2\psi(\vec{r})
	&= [E-V(r)]\psi(\vec{r}) \\
	\Leftrightarrow \frac{\hbar^2}{2m}\left[\frac{2}{r^2}\psi(r)
	-\frac{1}{r^2} \frac{\partial}{\partial r}r^2 \frac{\partial}{\partial r}\psi(\vec{r})\right]
	&= [E-V(r)]\psi(\vec{r}) \\
	\Leftrightarrow \frac{2}{r}f(r)-\frac{1}{r^2}\frac{\partial}{\partial r}
	r^2 \frac{\partial}{\partial r}rf(r)
	&= \frac{2m}{\hbar^2}\left[E-V(r)\right]rf(r) \\
		\Leftrightarrow -4f'(r)-rf''(r)
	&= \frac{2m}{\hbar^2}\left[E-V(r)\right]rf(r) \\
	\Leftrightarrow \frac{\hbar^2}{2mrf(r)}\left[4f'(r)+rf''(r)\right] + E
	&= V(r)
.\end{align*}
\section*{Problem 3: Harmonic oscillator in 3D}
\subsection*{a,}
This Hamilton operator has spherical symmetry as the potential term \[
	V(\hat{\vec{r}}) = \frac{\mu\omega^2}{2}\hat{\vec{r}}^2 = \tilde{V}(|\vec{r}|)
\] depends only on \(|\vec{r}|\).
\subsection*{b,}
We use the Laplacian for spherical coordinates and get \[
	\left[-\frac{\hbar^2}{2\mu}\left(\frac{\partial^2}{\partial r^2}
	+ \frac{2}{r}\frac{\partial}{\partial r}-\frac{\hat{L}^2}{\hbar^2r^2}\right)
	+ \frac{\mu\omega^2}{2}r^2\right]\psi(\vec{r})
	= E\psi(\vec{r})
.\] 
\subsection*{c,}
I won't fully rewrite it, but \[
	\psi_0(\vec{r})
	= \frac{1}{a^{\frac{3}{2}}\pi^{\frac{3}{4}}}\exp\left(-\frac{\vec{r}^2}{2a^2}\right)
	\propto R_0(r)\,Y_{0,0}(\theta,\phi)
.\] Since \(\psi(\vec{r})\) does not depend on angles, only on \(r\), we
will always measure \(l=m=0\).
\section*{Problem 4: Radial part of the Laplace operator in 3D and 2D}
\subsection*{a,}
\begin{align*}
	\partial_a f(r)
	&= (\partial_rf(r))\,(\partial_a r)
	= \frac{r_a}{r} f'(r) \\
	(\partial_a)^2 f(r)
	&= \frac{1}{r^2}\left[
		r\left(f'(r)+\frac{(r_a)^2}{r}f''(r)\right)-r_af'(r)\frac{r_a}{r}
	\right]
	= \frac{(r_a)^2}{r^2}f''(r) + \left(\frac{1}{r}-\frac{(r_a)^2}{r^3}\right)f'(r)
.\end{align*}
Because \(f(r)\) only depends on \(r\) we have \[
	\Delta f(r) = \Delta_r f(r)
.\] for \(n\) dimensions we have the Laplace
\begin{align*}
	\Delta f(r)
	&= \left(\sum_{k=1}^{n} (\partial_k)^2 \right) f(r)
	= \sum_{k=1}^{n} (\partial_k)^2 f(r) \\
	&= \sum_{k=1}^{n} \left[
	\frac{(r_k)^2}{r^2}f''(r) + \left(\frac{1}{r}-\frac{(r_k)^2}{r^3}\right)f'(r)\right] \\
	&= \frac{f''(r)}{r^2} \sum_{k=1}^{n} (r_k)^2
	+ \frac{f'(r)}{r} \sum_{k=1}^{n} 1
	- \frac{f'(r)}{r^3} \sum_{k=1}^{n} (r_k)^2 \\
	&= f''(r) + \frac{n}{r}f'(r) - \frac{1}{r}f'(r) \\
	&= f''(r) + \left(\frac{n-1}{r}\right)f'(r) \\
	&= \left(\frac{\partial^2}{\partial r^2}+\frac{n-1}{r}\frac{\partial}{\partial r}\right)f(r)
.\end{align*}
Therefore we have \[
\Delta_r = \left(\frac{\partial^2}{\partial r^2}+\frac{n-1}{r}\frac{\partial}{\partial r}\right)
.\] 
\subsection*{b,}
\begin{align*}
	\Delta_r^{3\mathrm{D}}R(r)
	&= \left(\frac{\partial^2}{\partial r^2}+\frac{2}{r}\frac{\partial}{\partial r}\right)
	\frac{u(r)}{r^{\alpha}}
	= \frac{\partial}{\partial r}\left(\frac{u'(r)}{r^{\alpha}}
	-\alpha \frac{u(r)}{r^{\alpha+1}}\right)
	+ \frac{2}{r}\left(\frac{u'(r)}{r^{\alpha}}
	-\alpha \frac{u(r)}{r^{\alpha+1}}\right) \\
	&= \frac{u''(r)}{r^{\alpha}}-\alpha \frac{u'(r)}{r^{\alpha+1}}
	- \alpha \frac{u'(r)}{r^{\alpha+1}}+\alpha(\alpha+1) \frac{u(r)}{r^{\alpha+2}}
	+ \frac{2}{r}\left(\frac{u'(r)}{r^{\alpha}}
	-\alpha \frac{u(r)}{r^{\alpha+1}}\right) \\
	&= \frac{1}{r^{\alpha}} u''(r)
	+ \frac{2(1-\alpha)}{r^{\alpha+1}} u'(r)
	- \frac{\alpha(1-\alpha)}{r^{\alpha+2}} u(r)
.\end{align*}
Obviously this simplifies immensely if \(\alpha=1\).
\subsection*{c,}
We set \(\theta = \frac{\pi}{2}\) to get the polar coordinates in \(\mathbb{R}^2\)
\begin{align*}
	\Delta_l^{2\mathrm{D}}
	&= \Delta_l^{3\mathrm{D}}\Big|_{\theta=\frac{\pi}{2}}
	= \frac{1}{r^2} \frac{\partial^2}{\partial \varphi^2}
.\end{align*}
\subsection*{d,}
\begin{align*}
	\Delta_r^{2\mathrm{D}}R(r)
	&= \left(\frac{\partial^2}{\partial r^2}+\frac{1}{r}\frac{\partial}{\partial r}\right)
	\frac{u(r)}{r^{\alpha}} \\
	&= \frac{u''(r)}{r^{\alpha}}
	- 2\alpha \frac{u'(r)}{r^{\alpha+1}}+\alpha(\alpha+1) \frac{u(r)}{r^{\alpha+2}}
	+ \frac{1}{r}\left(\frac{u'(r)}{r^{\alpha}}
	-\alpha \frac{u(r)}{r^{\alpha+1}}\right) \\
	&= \frac{1}{r^{\alpha}}u''(r)
	+ \frac{2(1-\alpha)}{r^{\alpha+1}}u'(r)
	+ \frac{\alpha^2}{r^{\alpha+2}}u(r)
.\end{align*}
For \(\alpha=1\) it simplifies to \[
	\frac{1}{r^{\alpha}}u''(r)+\frac{\alpha^2}{r^{\alpha+2}}u(r)
\] and for \(\alpha=0\) it simplifies to \[
	\frac{1}{r^{\alpha}}u''(r)
	+ \frac{2(1-\alpha)}{r^{\alpha+1}}u'(r)
.\] 
\subsection*{e,}
\begin{align*}
	\bigg(r^2
	&\frac{\partial^2}{\partial r^2}+r \frac{\partial}{\partial r}
	+ (r^2-\nu^2)\bigg) R(r)
	=  \left(r^2 \frac{\partial^2}{\partial r^2}+r \frac{\partial}{\partial r}
	+ (r^2-\nu^2)\right) \frac{u(r)}{r^{\alpha}} \\
	&= \frac{u''(r)}{r^{\alpha-2}}-2\alpha \frac{u'(r)}{r^{\alpha-1}}
	+ \alpha(\alpha+1) \frac{u(r)}{r^{\alpha}}
	+ \frac{u'(r)}{r^{\alpha-1}}
	-\alpha \frac{u(r)}{r^{\alpha}}
	+ (r^2-\nu^2) \frac{u(r)}{r^{\alpha}} \\
	&= \frac{1}{r^{\alpha-2}}u''(r)
	+ \frac{1-2\alpha}{r^{\alpha-1}} u'(r)
	+ \frac{\alpha^2+r^2-\nu^2}{r^{\alpha}} u(r) \\
	&= 0
.\end{align*}
For \(\alpha=\frac{1}{2}\) this simplifies to
\begin{align*}
	r^{\frac{3}{2}}u''(r) + \frac{\frac{1}{4}+r^2-\nu^2}{\sqrt{r}}u(r)
	&= 0 \\
	\Leftrightarrow u''(r) + \frac{\frac{1}{4}+r^2-\nu^2}{r^2}u(r)
	&= 0 \\
	\Leftrightarrow u''(r) + \left(1- \frac{\nu^2-\frac{1}{4}}{r^2}\right)u(r)
	&= 0
.\end{align*}
We try \(u(r) = Ae^{kr} + Be^{-kr}\) as an solution and get
\begin{align*}
	k &= i\sqrt{1-\frac{\nu^2-\frac{1}{4}}{r^2}}
\end{align*}
or for \(r\to\infty\) simply \(k=i\) which gives us  \[
	u(r) = Ae^{ir}+Be^{-ir}
.\] 
\end{document}

