\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage[style=iso]{datetime2}
\usepackage{amsmath, amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{ragged2e}
\usepackage[locale=DE]{siunitx}
\usepackage{braket}
\pdfsuppresswarningpagegroup=1

\title{Theoretische Physik 3: Quantenmechanik}
\subtitle{Submission for exercise 3}
\author{Stefan Gehr}

%\date{\today}
\date{2020-05-11}

\begin{document}
\maketitle
\section*{1}
Given is a wave function \(\psi(x)\) in one dimension, for which \(\braket{p}_{\psi}=\overline{p}\) and \(\Delta p_{\psi}=\sigma_p\).
A new wave function is defined as \[
	\Psi(x) = \psi(x)e^{ip_0x/\hbar}
.\] For \(\Psi(x)\) the expected momentum is given by
\begin{align*}
	\braket{p}_{\Psi} &= \int\mathrm{d}x\, \Psi^*(x) \left[\frac{\hbar}{i}\frac{\partial}{\partial x}\Psi(x)\right] \\
  &= \int\mathrm{d}x\, \psi^*(x)e^{-ip_0x/\hbar} \left[\frac{\hbar}{i}\frac{\partial}{\partial x}\psi(x)e^{ip_0x/\hbar}\right] \\
  &= \int\mathrm{d}x\, \psi^*(x)e^{-ip_0x/\hbar} \frac{\hbar}{i}\left[\frac{\partial \psi(x)}{\partial x}e^{ip_0x/\hbar} + \frac{ip_0}{\hbar}\psi(x)e^{ip_0x/\hbar}\right] \\
  &= \braket{p}_{\psi} + p_0 = \overline{p} + p_0
.\end{align*}
For the variance we have to consider \[
	\left(\Delta p_{\psi}\right)^2 = \braket{p^2}_{\psi} - \braket{p}_{\psi}^2 = \braket{p^2}_{\psi} - \overline{p}^2 = \sigma_p^2
,\] which gives us
\begin{align*}
	\left(\Delta p_{\Psi}\right)^2 &= \braket{p^2}_{\Psi} - \braket{p}_{\Psi}^2 = \braket{p^2}_{\Psi} - (\overline{p} + p_0)^2 \\
	\braket{p^2}_{\Psi} &= \int\mathrm{d}x\, \Psi^*(x) \left[\frac{\hbar^2}{i^2}\frac{\partial^2}{\partial x^2}\Psi(x)\right] \\
	&= \int\mathrm{d}x\, \Psi^*(x) \frac{\hbar^2}{i^2}\left[
	\frac{\partial^2 \psi(x)}{\partial x^2}e^{ip_0x/\hbar} + 2 \frac{ip_0}{\hbar} \frac{\partial \psi(x)}{\partial x}e^{ip_0x/\hbar} + \frac{i^2p_0^2}{\hbar^2}\Psi(x)\right] \\
	&= \braket{p^2}_{\psi} + 2p_0\overline{p} + p_0^2 \\
	\Rightarrow \left(\Delta p_{\Psi}\right)^2 &= \braket{p^2}_{\psi} - \overline{p}^2 = \sigma_p^2
.\end{align*}
So overall the expected value for \(p\) gets shifted by \(p_0\) and the variance stays the same.

\section*{2}
\subsection*{a,}
Consider a real valued wave function \(\psi(x)\in\mathbb{R}\), then
\begin{align*}
	\braket{p} &= \int\mathrm{d}x\, \overbrace{\psi^*(x)}^{=\psi(x)} \left[\frac{\hbar}{i} \frac{\partial}{\partial x}\psi(x)\right] \\
			   &= \frac{\hbar}{i} \int\mathrm{d}x\, \psi(x) \psi'(x) \\
			   &= \frac{\hbar}{2i} \psi^2(x)\Big|_{-\infty}^{\infty} = 0
.\end{align*}
\subsection*{b,}
Given is a wave function at \(t=0\) as \[
	\psi(x,0) = (\sqrt{2\pi}\sigma)^{-\frac{1}{2}} e^{-\frac{x^2}{4\sigma^2}}
.\] We can easily calculate the expected position
\begin{align*}
	\braket{x}_{t=0} =& \int\mathrm{d}x\,
	\psi(x,0) \big[x\overbrace{\psi^*(x,0)}^{=\psi(x,0)}\big]
	= \int\mathrm{d}x\, x \psi(x,0)^2 \\
	=& \int\mathrm{d}x\, x \frac{1}{\sqrt{2\pi}\sigma} e^{-\frac{x^2}{2\sigma^2}} \\
	=& \frac{1}{\sqrt{2\pi}\sigma}
	\left(-\sigma^2\right) \int\mathrm{d}x\,
	\left(-\frac{x}{\sigma^2}\right) e^{-\frac{x^2}{2\sigma^2}} \\
	=& -\frac{\sigma}{\sqrt{2\pi}} e^{-\frac{x^2}{2\sigma^2}}\Big|_{-\infty}^{\infty} = 0
.\end{align*}
\subsection*{c,}
Because \(\psi(x,0) \in \mathbb{R}\) we know that \(\braket{p} = 0\) which means
\(\braket{x}\) isn't propagating in one direction or the other and the wave function
will stay symmetric around \(\braket{x(t)}=0\).
Now if we calculate the probability to find the particle at \(x>10\sigma\) at t=0 we get
\begin{align*}
	P(x>10\sigma) =& \int_{10\sigma}^{\infty}\mathrm{d}x\,
	\overbrace{\psi^*(x,0)}^{=\psi(x,0)}\psi(x,0)
= \int_{10\sigma}^{\infty}\mathrm{d}x\, \psi(x,0)^2 \\
=& \frac{1}{\sqrt{2\pi}\sigma}\int_{10\sigma}^{\infty}\mathrm{d}x\, e^{-\frac{x^2}{2\sigma^2}}
\,\overset{\frac{x}{\sqrt{2}\sigma}\mapsto x}{=}\, \frac{1}{\sqrt{\pi}}\int_{10/\sqrt{2}}^{\infty}\mathrm{d}x\, e^{-x^2} \\
=& \frac{1}{2} \left(\mathrm{erf}(\infty) - \mathrm{erf}(10/\sqrt{2})\right) \\
=& \frac{1}{2} \left(1 - \mathrm{erf}(5\sqrt{2})\right) \\
\approx& \num{7.62e-24} > 0
.\end{align*}
Similarly for \(x<-10\sigma\) we get
\begin{align*}
	P(x<-10\sigma) =& \int_{-\infty}^{-10\sigma}\mathrm{d}x\, |\psi(x,0)|^2
	\overset{x\mapsto -x}{=} -\int_{\infty}^{10\sigma}\mathrm{d}x\,
	|\underbrace{\psi(-x,0)}_{=\psi(x,0)}|^2 \\
	=& \int_{10\sigma}^{\infty}\mathrm{d}x\,
	|\psi(x,0)|^2 = P(x>10\sigma)
.\end{align*}
Because the wave function is symmetric around \(x=0\) we get the same result.
Combining this with the knowledge \(\braket{p} = 0\) we know that it will stay symmetric so for \(t>0\) it will still be as likely to measure the particle
at \(x>10\sigma\) as it is at \(x<-10\sigma\).
\section*{3 Commutators}
\subsection*{a,}
\begin{align*}
	[\hat{A}, \hat{B} + \hat{C}] &=
	\hat{A}(\hat{B}+\hat{C})-(\hat{B}+\hat{C})\hat{A}
	= \hat{A}\hat{B}+\hat{A}\hat{C}-\hat{B}\hat{A}-\hat{C}\hat{A}
	= [\hat{A},\hat{B}]+[\hat{A},\hat{C}]
.\end{align*}
\subsection*{b,}
\begin{align*}
	[\hat{A}, \hat{B} \hat{C}] &=
	\hat{A}\hat{B}\hat{C} - \hat{B}\hat{C}\hat{A}
	= \hat{A}\hat{B}\hat{C} -\hat{B}\hat{A}\hat{C} + \hat{B}\hat{A}\hat{C} - \hat{B}\hat{C}\hat{A} \\
	&= (\hat{A}\hat{B}-\hat{B}\hat{A})\hat{C} + \hat{B}(\hat{A}\hat{C}-\hat{C}\hat{A})
	= [\hat{A},\hat{B}]\hat{C} + \hat{B}[\hat{A},\hat{C}]
.\end{align*}
\section*{4 Ehrenfest's Theorem}
\subsection*{a,}
We take a look at the time derivative of an expectation value
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t} \braket{a}_t =&
	\int\mathrm{d^3}r\, \frac{\mathrm{d}}{\mathrm{d}t} \psi^* \left[\hat{A}\psi\right] \\
	=& \int\mathrm{d^3}r\,
	\frac{\mathrm{d \psi^*}}{\mathrm{d}t} \hat{A}\psi + \psi^* \frac{\mathrm{d}}{\mathrm{d}t}\hat{A}\psi \\
	\overset{\frac{\mathrm{d}\hat{A}}{\mathrm{d}t}=0}{=}& \int\mathrm{d^3}r\,
	\frac{\mathrm{d \psi^*}}{\mathrm{d}t} \hat{A}\psi + \psi^* \hat{A} \frac{\mathrm{d}\psi}{\mathrm{d}t}
.\end{align*}
Now using the Schrödinger equation
\begin{align*}
	i\hbar \frac{\mathrm{d}}{\mathrm{d}t} \psi =& \hat{H}\psi \\
	\Leftrightarrow \frac{\mathrm{d}\psi}{\mathrm{d}t} =& -\frac{i}{\hbar}\hat{H}\psi \\
	\Leftrightarrow \frac{\mathrm{d}\psi^*}{\mathrm{d}t} =& \frac{i}{\hbar} \left(\hat{H}\psi\right)^*
\end{align*}
we get
\begin{align}
	\frac{\mathrm{d}}{\mathrm{d}t} \braket{a}_t =&
	\frac{i}{\hbar}\int\mathrm{d^3}r\, \left[\left(\hat{H}\psi\right)^*\hat{A}\psi
	- \psi^*\left(\hat{A}\hat{H}\psi\right) \right]
	\label{eq:nohermit}
.\end{align}
\(\hat{H}\) is a Hermitian operator which means that \[
	\int\mathrm{d^3}r\, \alpha^*(\vec{r}) \left(\hat{H}\beta(\vec{r})\right)
	= \int\mathrm{d^3}r\, \left(\hat{H}\alpha(\vec{r})\right)^*\beta(\vec{r})
.\] In our case we can use \(\alpha = \psi\) and \(\beta = \hat{A}\psi\) which gets us
\begin{align*}
	\int\mathrm{d^3}r\, \left(\hat{H}\psi\right)^*\hat{A}\psi
	= \int\mathrm{d^3}r\, \psi^* \left(\hat{H}\hat{A}\psi\right)
.\end{align*}
Inserting this into \autoref{eq:nohermit} we get
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t} \braket{a}_t =&
	\frac{i}{\hbar}\int\mathrm{d^3}r\, \left[\psi^*\left(\hat{H}\hat{A}\psi\right)
	- \psi^*\left(\hat{A}\hat{H}\psi\right) \right] \\
	=& \frac{i}{\hbar} \int\mathrm{d^3}r\,
	\psi^* \left([\hat{H},\hat{A}]\psi\right) \\
	=& \frac{i}{\hbar}\braket{[\hat{H},\hat{A}]}
\end{align*}
\begin{align}
	\Leftrightarrow 
	\frac{\mathrm{d}}{\mathrm{d}t} \braket{a}_t =
	\frac{i}{\hbar}\braket{[\hat{H},\hat{A}]}
	\label{eq:expvaltime}
.\end{align}
\subsection*{b,}
Using \autoref{eq:expvaltime} we know
\begin{align}
	\frac{\mathrm{d}}{\mathrm{d}t}\braket{\vec{r}} =&
	\frac{i}{\hbar} \braket{[\hat{H},\vec{r}]} \notag\\
	=& \frac{i}{\hbar}\int\mathrm{d^3}r\, \psi^* \left([\hat{H},\vec{r}]\psi\right) \notag\\
	=& \frac{i}{\hbar}\int\mathrm{d^3}r\, \psi^* \left(\hat{H}\vec{r}\psi - \vec{r}\hat{H}\psi\right)
	\label{eq:exprtimeinter}
.\end{align}
Let's take a look at the first term with Einsteinian index notation
\begin{align*}
	\left(\hat{H}\vec{r}\psi\right)^a =& \hat{H} r^a \psi
	= \left(-\frac{\hbar^2}{2m}\Delta + V\right) r^a \psi
	= \left(-\frac{\hbar^2}{2m}\partial_b\partial^b + V\right) r^a \psi \\
	=& -\frac{\hbar^2}{2m}\partial_b\left(\delta^{ab}\psi + r^a\partial^b\psi\right) + V r^a\psi \\
	=& -\frac{\hbar^2}{2m} \left(\partial^a \psi + \delta^a_b\partial^b\psi + r^a \partial_b\partial^b\psi\right) + V r^a\psi \\
	=& \left(-\frac{\hbar^2}{m}\vec{\nabla}\psi\right)^a + \left(\vec{r} \hat{H}\psi\right)^a
\end{align*}
\begin{align*}
\Leftrightarrow \hat{H} \vec{r} \psi =& -\frac{\hbar^2}{m}\vec{\nabla}\psi + \vec{r}\hat{H}\psi
= -\frac{i\hbar}{m} \frac{\hbar}{i}\vec{\nabla}\psi + \vec{r}\hat{H}\psi \\
=& -\frac{i\hbar}{m} \hat{\vec{p}}\psi + \vec{r}\hat{H}\psi
.\end{align*}
Inserting this into \autoref{eq:exprtimeinter} we get \[
	\frac{\mathrm{d}}{\mathrm{d}t} \braket{\vec{r}} = \frac{1}{m}\braket{\vec{p}}\quad\square.
\]
Again using Equation 2 with
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t}\braket{\vec{p}} =&
	\frac{i}{\hbar} \braket{[\hat{H},\hat{\vec{p}}]} \notag\\
	=& \frac{i}{\hbar}\int\mathrm{d^3}r\, \psi^* \left([\hat{H},\hat{\vec{p}}]\psi\right) \notag\\
	=& \frac{i}{\hbar}\int\mathrm{d^3}r\, \psi^* \left(\hat{H}\hat{\vec{p}}
	- \hat{\vec{p}}\hat{H}\right)\psi
	\label{eq:expptimeinter}
.\end{align*}
Let's look at each of those operator combinations separately.
\begin{align*}
	\hat{H}\hat{\vec{p}}=&
	\left(-\frac{\hbar^2}{2m}\Delta+V\right)\left(\frac{\hbar}{i}\vec{\nabla}\right) \\
	=& -\frac{\hbar^3}{2mi}\Delta\vec{\nabla}+\frac{\hbar}{i}V\vec{\nabla} \\
	\hat{\vec{p}}\hat{H}=&
	\left(\frac{\hbar}{i}\vec{\nabla}\right)\left(-\frac{\hbar^2}{2m}\Delta+V\right) \\
	=& -\frac{\hbar^3}{2mi}\vec{\nabla}\Delta+\frac{\hbar}{i}\vec{\nabla}V
.\end{align*}
Because \(\Delta\vec{\nabla} = \vec{\nabla}\Delta\) we get \[
	[\hat{H},\hat{\vec{p}}] = \frac{\hbar}{i}\left(V\vec{\nabla}-\vec{\nabla}V\right)
\] for the resulting operator. This gives us
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t}\braket{\vec{p}} =&
	\int\mathrm{d^3}r\, \psi^*\left(V\vec{\nabla}\psi-\vec{\nabla}V\psi\right)
.\end{align*}
Let's take a look at the second term \[
	\vec{\nabla}V\psi = \left(\vec{\nabla}V\right)\psi+V\left(\vec{\nabla}\psi\right)
.\] So overall with \(\vec{F} = -\vec{\nabla}V\) we're getting
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t}\braket{\vec{p}} =&
	\int\mathrm{d^3}r\, \psi^*\left(-\vec{\nabla}V\right)\psi \\
	=& \int\mathrm{d^3}r\, |\psi|^2 \vec{F} \\
	=& \braket{\vec{F}} \quad \square
.\end{align*}
\subsection*{c,}
If the mass \(m\) is constant the equations can be combined into
\begin{equation}
	m \frac{\mathrm{d^2}}{\mathrm{d}t^2} \braket{\vec{r}} = \braket{\vec{F}(\vec{r})}
.\end{equation}
With \(\vec{x}=\braket{\vec{r}}\) we can write
\begin{align*}
	\braket{\vec{F}(\vec{r})} = 
	m \frac{\mathrm{d^2}}{\mathrm{d}t^2} \braket{\vec{r}} =
	m \frac{\mathrm{d^2}}{\mathrm{d}t^2} \vec{x}
	= \vec{F}(\vec{x})
	= \vec{F}(\braket{\vec{r}})
.\end{align*}
If the force comes from a potential \(V(\vec{r})\) with \(\vec{F}(\vec{r})=-\vec{\nabla}V(\vec{r})\),
the potential is valid in the Schrödinger equation, and
\begin{equation}
	\braket{\vec{F}(\vec{r})} = \vec{F}(\braket{\vec{r}})
	\label{eq:forceeq}
\end{equation}
then this coincides with Newton's second law
\begin{equation}
	m\frac{\mathrm{d^2}}{\mathrm{d}t^2}\vec{x} = \vec{F}(\vec{x})
.\end{equation}
\autoref{eq:forceeq} is equivalent to
\begin{align*}
\int\mathrm{d^3}r\, \vec{F}(\vec{r}) \, |\psi(\vec{r})|^2
= \vec{F}\left(\int\mathrm{d^3}r\, \vec{r} \, |\psi(\vec{r})|^2 \right)
\end{align*}
which shows that \(\vec{F}\) needs to be linear in \(\vec{r}\).
\end{document}

