This project contains my submissions for my Theoretical Physics 3: Quantum Mechanics homework.

The latest compiled PDFs can be found at
[https://gitlab.com/BoostCookie/tp-3-homework-submissions/-/jobs/artifacts/master/browse?job=compile_pdf](https://gitlab.com/BoostCookie/tp-3-homework-submissions/-/jobs/artifacts/master/browse?job=compile_pdf).

If you want to compile the PDFs yourself:

Dependencies: `git latexmk texlive texlive-latex-extra texlive-science python3-numpy python3-matplotlib python3-scipy`

```sh
git clone git@gitlab.cs.fau.de:oz73ifuv/tp-3-homework-submissions.git
cd tp-3-homework-submissions
# change directory to the .tex-file you want to compile, e.g.:
cd exercise01
make
```
