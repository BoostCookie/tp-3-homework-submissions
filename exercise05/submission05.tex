\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage[style=iso]{datetime2}
\usepackage{amsmath, amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{ragged2e}
\usepackage[locale=DE]{siunitx}
\usepackage{braket}
\usepackage{bm}
\usepackage{bbold}
\pdfsuppresswarningpagegroup=1

\title{Theoretische Physik 3: Quantenmechanik}
\subtitle{Submission for exercise 5}
\author{Stefan Gehr}

%\date{\today}
\date{2020-05-25}

\begin{document}
\maketitle
\section*{Problem 1: Two-level system in Dirac notation}
Our quantum system has exactly two energy eigenvalues and -states
\begin{align}
	\hat{H}\ket{\psi_1} &= E_1 \ket{\psi_1} \\
	\hat{H}\ket{\psi_2} &= E_2 \ket{\psi_2}
\end{align}
with
\begin{equation}
E_1-E_2=\hbar\omega
\label{eq:energydiff}
\end{equation}
\subsection*{a,}
Consider
\begin{align*}
	\bra{\psi_1}\hat{H}\ket{\psi_2} = E_2\braket{\psi_1|\psi_2}
.\end{align*}
Since \(\hat{H}\) is hermitian we can also write
\begin{align*}
	\bra{\psi_1}\hat{H}\ket{\psi_2} = \left(\bra{\psi_2}\hat{H}^{\dagger}\ket{\psi_1}\right)^*
	= \left(\bra{\psi_2}\hat{H}\ket{\psi_1}\right)^* = E_1 \braket{\psi_2|\psi_1}^*
	= E_1 \braket{\psi_1|\psi_2}
.\end{align*}
Therefore
\begin{align*}
	&E_2 \braket{\psi_1|\psi_2} = E_1 \braket{\psi_1|\psi_2} \\
	\Leftrightarrow &(E_1-E_2) \braket{\psi_1|\psi_2} = 0 \\
	\overset{(\ref{eq:energydiff})}{\Leftrightarrow} &\hbar\omega \braket{\psi_1|\psi_2} = 0 \\
	\Leftrightarrow &\braket{\psi_1|\psi_2} = 0
.\end{align*}
This means \(\ket{\psi_1}\) and \(\ket{\psi_2}\) are perpendicular to one another.
Because our Hilbert space is only two-dimensional we can use these two eigenstates as a basis.
\subsection*{b,}
\label{subsec:1b}
The time-dependant solution for the eigenstates is of course
\begin{align*}
	\hat{H}\ket{\psi_j} = E_j \ket{\psi_j} = i\hbar \frac{\mathrm{d}}{\mathrm{d}t} \ket{\psi_j} \\
	\Leftrightarrow \ket{\psi_j(t)} = \ket{\psi_j} e^{-\frac{i}{\hbar} E_j t}
.\end{align*}
With \(\ket{\psi}\) being a superposition of \(\ket{\psi_1}\) and \(\ket{\psi_2}\) we simply get
\begin{align*}
	\ket{\psi(t)} &= \frac{1}{\sqrt{2}} (\ket{\psi_1(t)} + \ket{\psi_2(t)}) \\
			  &= \frac{1}{\sqrt{2}} \left(\ket{\psi_1} e^{-\frac{i}{\hbar}E_1t} + \ket{\psi_2} e^{-\frac{i}{\hbar}E_2t}\right) \\
			  &\overset{(\ref{eq:energydiff})}{=} \frac{1}{\sqrt{2}} \left( \ket{\psi_1} e^{-\frac{i}{\hbar}E_1t} + \ket{\psi_2} e^{-\frac{i}{\hbar}(E_1-\hbar\omega)t} \right) \\
			  &= \frac{1}{\sqrt{2}} \left( \ket{\psi_1} + \ket{\psi_2} e^{i\omega t} \right) e^{-\frac{i}{\hbar}E_1 t}
.\end{align*}
This means over time a phase shift of \(\omega t\) is introduced between the two eigenstates.
\subsection*{c,}
Note to readers: In this section I calculated a lot of unnecessary stuff.
If you want to skip all that then just start reading again at \autoref{eq:necessary}. \\
Every wave function can be written as a superposition of the two eigenfunctions \[
	\ket{\psi} = a\ket{\psi_1}+b\ket{\psi_2}
\] with
\begin{equation}
a^2 + b^2 = 1
\label{eq:restriction}
.\end{equation}
Let's take a look at the expected value
\begin{align*}
	 \braket{a} &= \bra{\psi}\hat{A}\ket{\psi}
	 = (a^*\bra{\psi_1}+b^*\bra{\psi_2}) (a\ket{\psi_2} + b\ket{\psi_1}) \\
				&= (|a|^2+|b|^2)\braket{\psi_1|\psi_2} + a^*b\braket{\psi_1|\psi_1}+ab^*\braket{\psi_2|\psi_2}) \\
				&= a^*b + ab^*
				= a^*b + (a^*b)^*
				= 2\,\mathrm{Re}(a^*b) \in \mathbb{R}
\end{align*}
and the variance
\begin{align*}
	\braket{a^2} &= \bra{\psi}\hat{A}^2\ket{\psi} \\
				 &= \bra{\psi} (a \hat{A}\ket{\psi_2} + b \hat{A}\ket{\psi_1}) \\
				 &= \bra{\psi} (a \ket{\psi_1} + b \hat{A}\ket{\psi_2}) \\
				 &= \braket{\psi|\psi} = 1 \\
	\Rightarrow (\Delta a)^2 &= \braket{a^2} - \braket{a}^2 = 1 - 4\,\mathrm{Re}(a^*b)^2
.\end{align*}
Let's find out whether \((\Delta a)^2 \geq 0\).
Obviously \(\mathrm{Re}(a^*b)\) is larger when \(a^*\) and \(b\) have opposite phases.
This is the case when \(\phi:=\arg(a)=\arg(b)\).
We take a look at \autoref{eq:restriction} with this case
\begin{align*}
	a^2 + b^2 = |a|^2 e^{2i\phi} + |b|^2 e^{2i\phi} = (|a|^2 + |b|^2) e^{2i\phi} = 1 \\
	\overset{1\in\mathbb{R}}{\Rightarrow} \phi = 0
.\end{align*}
We see that \(\mathrm{Re}(a^*b)\) is largest when \(\phi=0\) and therefore \(a,b \in \mathbb{R}\).
Then we can write
\begin{align*}
	\mathrm{Re}(a^*b) &= ab
	\overset{(\ref{eq:restriction})}{=} a \sqrt{1-a^2}
.\end{align*}
It's easy to see that
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}a} a\sqrt{1-a^2}
	&= \sqrt{1-a^2} - \frac{a}{\sqrt{1-a^2}}
		= \frac{1-2a^2}{\sqrt{1-a^2}} \overset{!}{=} 0 \\
	\Leftrightarrow a &= \frac{1}{\sqrt{2}}
	\overset{(\ref{eq:restriction})}{\Rightarrow} a=b=\frac{1}{\sqrt{2}}
\end{align*}
is a maxima.
Even in this extreme case we have \[
	(\Delta a)^2 = 1 - 4 \left(\frac{1}{2}\right)^2 = 0 \geq 0
,\] therefore we  always have \[
\Delta a \geq 0
.\] 
Consider another wave-function \(\ket{\psi'} = c\ket{\psi_1}+d\ket{\psi_2}\).
With
\begin{align}
	\bra{\psi'}\hat{A}\ket{\psi} \label{eq:necessary} \\
	&= (c^*\bra{\psi_1}+d^*\bra{\psi_2})(a\ket{\psi_2}+b\ket{\psi_1}) \nonumber\\
	&= bc^* + ad^* \nonumber\\
	\left(\bra{\psi}\hat{A}\ket{\psi'}\right)^*
	&= \left[(a^*\bra{\psi_1}+b^*\bra{\psi_2})(c\ket{\psi_2}+d\ket{\psi_1})\right]^* \nonumber\\
	&= \left(a^*d + b^*c\right)^* = ad^* + bc^* \nonumber\\
	&= \bra{\psi'}\hat{A}\ket{\psi} \nonumber\\
	\Rightarrow \hat{A} &= \hat{A}^{\dagger} \nonumber
\end{align}
we can see that \(\hat{A}\) is hermitian.
So overall \(\hat{A}\) seems to have all the properties that observables of measurable physical quantities have.
\subsection*{d,}
\(\hat{A}\) can be written as the matrix
\begin{equation}
	\hat{A} =
	\begin{pmatrix}
		0 & 1 \\
		1 & 0
	\end{pmatrix}
.\end{equation}
Let's calculate the eigenvalues and -vectors for this matrix with
\begin{align*}
	\begin{pmatrix}
		0 & 1 \\
		1 & 0
	\end{pmatrix}
	\bm{v} = \lambda \bm{v} \\
	\Leftrightarrow \left(
	\begin{pmatrix}
		0 & 1 \\
		1 & 0
	\end{pmatrix}
- \lambda \mathbb{1} \right) \bm{v} = \bm{0} \\
	\Rightarrow
	\begin{vmatrix}
		-\lambda & 1 \\
		1 & -\lambda
	\end{vmatrix}
	= 0 \\
	\Leftrightarrow \lambda^2 - 1 = 0 \\
	\Leftrightarrow \lambda = \pm 1
\end{align*}
\begin{align*}
	\begin{pmatrix}
		0 & 1 \\
		1 & 0
	\end{pmatrix}
	\cdot
	\begin{pmatrix}
		a_+ \\
		b_+
	\end{pmatrix}
	= 
	\begin{pmatrix}
		a_+ \\
		b_+
	\end{pmatrix} \\
	\Leftrightarrow
	\begin{pmatrix}
		b_+ \\
		a_+
	\end{pmatrix}
	= 
	\begin{pmatrix}
		a_+ \\
		b_+
	\end{pmatrix} \\
	\Rightarrow a_+ = b_+ \overset{||\bm{v}||=1}{=} \frac{\pm 1}{\sqrt{2}} \\
	\bm{v_+} = \frac{\pm 1}{\sqrt{2}}
	\begin{pmatrix}
		1 \\
		1
	\end{pmatrix}
\end{align*}
\begin{align*}
	\begin{pmatrix}
		0 & 1 \\
		1 & 0
	\end{pmatrix}
	\cdot
	\begin{pmatrix}
		a_- \\
		b_-
	\end{pmatrix}
	= - 
	\begin{pmatrix}
		a_- \\
		b_-
	\end{pmatrix} \\
	\Leftrightarrow
	\begin{pmatrix}
		b_- \\
		a_-
	\end{pmatrix}
	= 
	\begin{pmatrix}
		- a_- \\
		- b_-
	\end{pmatrix} \\
	\Rightarrow a_- = - b_- \overset{||\bm{v}||=1}{=} \frac{\pm 1}{\sqrt{2}} \\
	\bm{v_-} = \frac{\pm 1}{\sqrt{2}}
	\begin{pmatrix}
		1 \\
		-1
	\end{pmatrix}
.\end{align*}
So for the eigenvalue \(1\) we have the normalised eigenvectors \(\bm{v_+}=\pm 1/\sqrt{2}\,(\bm{e_1}+\bm{e_2})\)
and for the eigenvalue \(-1\) we have the normalised eigenvectors \(\bm{v_-}=\pm 1/\sqrt{2}\,(\bm{e_1}-\bm{e_2})\).
We can also write them as
\begin{align*}
	\ket{\psi_+} &= \frac{\pm 1}{\sqrt{2}}\,(\ket{\psi_1}+\ket{\psi_2}) \\
	\ket{\psi_-} &= \frac{\pm 1}{\sqrt{2}}\,(\ket{\psi_1}-\ket{\psi_2}) \\
	\ket{\psi_+} &= \frac{\pm 1}{\sqrt{2}}
	\begin{pmatrix}
		1 \\
		1
	\end{pmatrix} \\
	\ket{\psi_-} &= \frac{\pm 1}{\sqrt{2}}
	\begin{pmatrix}
		1 \\
		-1
	\end{pmatrix}
.\end{align*}
\subsection*{e,}
\begin{align*}
	P(a_-)
	&= |\braket{\psi_-|\psi(t)}|^2
	= \left|\frac{1}{2}\left(\bra{\psi_1}-\bra{\psi_2}\right)\left(e^{-\frac{i}{\hbar}E_1t}\ket{\psi_1}-e^{-\frac{i}{\hbar}E_2t}\ket{\psi_2}\right)\right|^2 \\
	&= \frac{1}{4}\left|e^{-\frac{i}{\hbar}E_1t}\braket{\psi_1|\psi_1}
	-e^{-\frac{i}{\hbar}E_2t}\braket{\psi_1|\psi_2}
	-e^{-\frac{i}{\hbar}E_1t}\braket{\psi_2|\psi_1}
	+e^{-\frac{i}{\hbar}E_2t}\braket{\psi_2|\psi_2}\right|^2 \\
	&= \frac{1}{4}\left|e^{-\frac{i}{\hbar}E_1t}+e^{-\frac{i}{\hbar}E_2t}\right|^2
	= \frac{1}{4}\left|e^{\frac{i}{\hbar}E_1t}\right|^2\left|e^{-\frac{i}{\hbar}E_1t}+e^{-\frac{i}{\hbar}E_2t}\right|^2 \\
	&= \frac{1}{4}\left|1+e^{-\frac{i}{\hbar}(E_2-E_1)t}\right|^2
	= \frac{1}{4}\left|1+e^{-\frac{i}{\hbar}\hbar\omega t}\right|^2
	= \frac{1}{4}\left|1+e^{-i\omega t}\right|^2 \\
	&= \frac{1}{4}\left\{ [1+\cos(\omega t)]^2 + \sin^2(\omega t) \right\}
	= \frac{1}{4}\left[1+2\cos(\omega t)+\cos^2{\omega t} + \sin^2(\omega t) \right] \\
	&= \frac{1}{2}\left[1+\cos(\omega t)\right] = \cos^2\left(\frac{\omega t}{2}\right)
.\end{align*}
\section*{Problem 2: Three-dimensional Hilbert space}
\subsection*{a,}
\begin{align*}
	\hat{A} &= \frac{1}{\sqrt{2}}
			\begin{pmatrix}
				1 \\
				1 \\
				0
			\end{pmatrix}
			\cdot \frac{1}{\sqrt{2}}
			\begin{pmatrix}
				1 & 0 & 1
			\end{pmatrix} \\
			&= \frac{1}{{2}}
			\begin{pmatrix}
				1 \\
				1 \\
				0
			\end{pmatrix}
			\cdot
			\begin{pmatrix}
				1 & 0 & 1
			\end{pmatrix} \\
			&= \frac{1}{2}
			\begin{pmatrix}
				1 & 0 & 1 \\
				1 & 0 & 1 \\
				0 & 0 & 0
			\end{pmatrix}
.\end{align*}
\subsection*{b,}
Because of
\begin{align*}
	(\hat{A}^*)^T &= \hat{A}^T
	= \frac{1}{2}
	\begin{pmatrix}
		1 & 1 & 0 \\
		0 & 0 & 0 \\
		1 & 1 & 0
	\end{pmatrix}
	\neq \hat{A}
\end{align*}
\(\hat{A}\) is not hermitian.
\subsection*{c,}
With
\begin{align*}
	|\hat{A}-\lambda \mathbb{1}| \overset{!}{=} 0 \\
	\Leftrightarrow \frac{1}{2}
	\begin{vmatrix}
		1-2\lambda & 0 & 1 \\
		1 & -2\lambda & 1 \\
		0 & 0 & -2\lambda
	\end{vmatrix}
= 0 \\
\Leftrightarrow (1-2\lambda)(-2\lambda)(-2\lambda) = 0 \\
\Leftrightarrow -8\lambda^3 + 4\lambda^2 = 0 \\
\Rightarrow \lambda_{1,2} = 0; \; \lambda_3 = \frac{1}{2}
\end{align*}
we can see that the eigenvalues are \(\lambda_{1,2}=0\) and \(\lambda_3 = \frac{1}{2}\).
\section*{Problem 3: Evolution operator and Heisenberg representation}
\subsection*{a,}
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}\lambda}e^{\lambda \hat{A}}
	&= \lim_{h\to 0} \frac{1}{h}\left(e^{(\lambda + h) \hat{A}} - e^{\lambda \hat{A}}\right) \\
	&= \lim_{h\to 0} \frac{1}{h}\left(\sum_{n=0}^{\infty} \frac{(\lambda+h)^n\hat{A}^n}{n!} - \sum_{n=0}^{\infty} \frac{\lambda^n\hat{A}^n}{n!}\right) \\
	&= \lim_{h\to 0} \frac{1}{h}\left(\sum_{n=1}^{\infty}\frac{[(\lambda+h)^n-\lambda^n]\hat{A}^n}{n!}\right) \\
	&= \lim_{h\to 0} \frac{1}{h}\left(\sum_{n=1}^{\infty}\frac{[n\lambda^{n-1}h+\mathcal{O}(h^2)]\hat{A}^n}{n!}\right) \\
	&= \lim_{h\to 0} \left(\sum_{n=1}^{\infty}\frac{[\lambda^{n-1}+\mathcal{O}(h)]\hat{A}^n}{(n-1)!}\right) \\
	&= \sum_{n=1}^{\infty}\frac{\lambda^{n-1}\hat{A}^n}{(n-1)!}
	= \sum_{n=0}^{\infty}\frac{\lambda^{n}\hat{A}^{n+1}}{n!}
	= \hat{A}\sum_{n=0}^{\infty}\frac{\lambda^{n}\hat{A}^{n}}{n!} \\
	&= \hat{A} e^{\lambda \hat{A}}
.\end{align*}
\subsection*{b,}
Because of the differential equation called the Schrödinger equation
\begin{align}
	i\hbar \frac{\partial}{\partial t} \ket{\psi(t)} = \hat{H}\ket{\psi(t)} \\
	\Leftrightarrow \frac{\partial}{\partial t} \ket{\psi(t)} = -\frac{i}{\hbar}\hat{H}\ket{\psi(t)}
	\label{eq:realignedschr}
\end{align}
we try
\begin{align*}
	\ket{\psi(t)} = A e^{k(t-t_0)}
.\end{align*}
Setting \(t=t_0\) we see that \(A=\ket{\psi(t_0)}\). Setting this into \autoref{eq:realignedschr} we get
\begin{align*}
	k\ket{\psi(t)} = -\frac{i}{\hbar}\hat{H}\ket{\psi(t)}
\end{align*}
which shows us \(k = -\frac{i}{\hbar}\hat{H}\).
So overall we have
\begin{align*}
	\ket{\psi(t)} = e^{-\frac{i}{\hbar}\hat{H}(t-t_0)}\ket{\psi(t_0)}
,\end{align*}
or with
\begin{align*}
	\hat{U}(t-t_0) = e^{-\frac{i}{\hbar}\hat{H}(t-t_0)}
\end{align*}
simply
\begin{align}
	\ket{\psi(t)} = \hat{U}(t-t_0)\ket{\psi(t_0)}
	\label{eq:uequation}
.\end{align}
\subsection*{c,}
\begin{align*}
	\hat{U}^{\dagger} &= \exp\left(-\frac{i}{\hbar}\hat{H}(t-t_0)\right)^{\dagger} \\
	&= \left(\sum_{n=0}^{\infty}\frac{(-i)^n(t-t_0)^n\hat{H}^n}{\hbar^nn!}\right)^{\dagger} \\
	&= \sum_{n=0}^{\infty}\frac{i^n(t-t_0)^n(\overbrace{\hat{H}^{\dagger}}^{=\hat{H}})^n}{\hbar^nn!} \\
	&= \exp\left(\frac{i}{\hbar}\hat{H}(t-t_0)\right) \\
	&= \hat{U}^{-1}
.\end{align*}
\subsection*{d,}
Because of \autoref{eq:uequation} it's easy to see that
\begin{align*}
	\braket{a}_t = \bra{\psi(t)}\hat{A}\ket{\psi(t)} = \bra{\psi(t_0)}\hat{U}^{\dagger}(t-t_0)\hat{A}\hat{U}(t-t_0)\ket{\psi(t_0)}
,\end{align*}
or with \(\hat{B}(t) = \hat{U}^{-1}(t-t_0)\hat{A}\hat{U}(t-t_0)\) just
\begin{align*}
	\braket{a}_t = \bra{\psi(t_0)}\hat{B}(t)\ket{\psi(t_0)}
.\end{align*}
\begin{align*}
	\hat{B}(t_0) = \hat{U}^{-1}(0)\hat{A}\hat{U}(0) = \hat{\mathbb{1}}\hat{A}\hat{\mathbb{1}} = \hat{A}
.\end{align*}
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t}\braket{a}_t
	=& \bra{\psi(t_0)}\left(\frac{\mathrm{d}}{\mathrm{d}t}\hat{U}^{\dagger}(t-t_0)\right)\hat{A}\hat{U}(t-t_0)\ket{\psi(t_0)} \\
	&+ \bra{\psi(t_0)}\hat{U}(t-t_0)\hat{A}\left(\frac{\mathrm{d}}{\mathrm{d}t}\hat{U}(t-t_0)\right)\ket{\psi(t_0)} \\
	=& \bra{\psi(t_0)}\left(\frac{i}{\hbar}\hat{H}\hat{U}^{\dagger}(t-t_0)\right)\hat{A}\hat{U}(t-t_0)\ket{\psi(t_0)} \\
	&- \bra{\psi(t_0)}\hat{U}(t-t_0)\hat{A}\left(\frac{i}{\hbar}\hat{H}\hat{U}(t-t_0)\right)\ket{\psi(t_0)} \\
	\overset{[\hat{H},\hat{U}]=0}{=}&\: \frac{i}{\hbar}\left(\bra{\psi(t_0)}\hat{H}\hat{B}(t)\ket{\psi_0}-\bra{\psi(t_0)}\hat{B}(t)\hat{H}\ket{\psi(t_0)}\right) \\
	=& -\frac{i}{\hbar}\bra{\psi(t_0)}[\hat{B}(t),\hat{H}]\ket{\psi(t_0)} = \frac{\mathrm{d}}{\mathrm{d}t}\braket{a}_t \quad \forall\ket{\psi(t_0)} \\
	\Rightarrow i\hbar \frac{\mathrm{d}\hat{B}(t)}{\mathrm{d}t} =& [\hat{B}(t),\hat{H}]
.\end{align*}
\section*{Problem 4: Commutators}
\subsection*{a,}
We will use the relation
\begin{equation}
	[\hat{A},\hat{B}\hat{C}] = [\hat{A},\hat{B}]\hat{C}+\hat{B}[\hat{A},\hat{C}]
	\label{eq:indurel}
\end{equation}
We prove by induction. We start with \(n=1\):
 \begin{align*}
	 [\hat{x},\hat{p}_x^1] &= [\hat{x},\hat{p}_x] = i\hbar = i\hbar \cdot 1 \cdot \hat{p}_x^{1-1}
.\end{align*}
Then we do \(n\mapsto n+1\):
\begin{align*}
	[\hat{x},\hat{p}_x^{n+1}]
	&= [\hat{x},\hat{p}_x\hat{p}_x^{n}]
	\overset{(\ref{eq:indurel})}{=} [\hat{x},\hat{p}_x]\hat{p}_x^n + \hat{p}_x[\hat{x},\hat{p}_x^n] \\
	&= i\hbar \hat{p}_x^n + \hat{p}_x[\hat{x},\hat{p}_x^n]
	\overset{\mathrm{I.A.}}{=} i\hbar \hat{p}_x^n + \hat{p}_x i\hbar n \hat{p}_x^{n-1} \\
	&= i\hbar (n+1)\hat{p}_x^n \qquad \square
.\end{align*}
For the other relation we also use induction. Again we start with \(n=1\):
\begin{align*}
	[\hat{p}_x,\hat{x}^1] = [\hat{p}_x,\hat{x}] = -i\hbar = -i\hbar \cdot 1 \cdot \hat{x}^{1-1}
.\end{align*}
And again \(n\mapsto n+1\):
\begin{align*}
	[\hat{p}_x,\hat{x}^{n+1}]
	&= [\hat{p}_x,\hat{x}^n \hat{x}]
	\overset{(\ref{eq:indurel})}{=} [\hat{p}_x,\hat{x}^n]\hat{x} + \hat{x}^n[\hat{p}_x,\hat{x}] \\
	&= [\hat{p}_x,\hat{x}^n]\hat{x} - \hat{x}^n i\hbar
	\overset{\mathrm{I.A.}}{=} -i\hbar n\hat{x}^{n-1}\hat{x} - \hat{x}^n i\hbar \\
	&= -i\hbar(n+1)\hat{x}^n \qquad \square
.\end{align*}
\subsection*{b,}
Any function of operators can be written as its power series expansion \[
	\hat{A} = \hat{A}(\hat{x},\hat{p}_x) = \sum_{n,m=0}^{\infty} c_{n,m} \hat{x}^n\hat{p}_x^m
.\] We also know that
\begin{align}
	\hat{x}\hat{p}_x\psi(x) &= -i\hbar x\frac{\partial}{\partial x}\psi(x) \nonumber\\
	\hat{p}_x\hat{x}\psi(x) &= -i\hbar \frac{\partial}{\partial x}x\psi(x) = -i\hbar\left(\psi(x)+x \frac{\partial}{\partial x}\psi(x)\right) = (-i\hbar + \hat{x}\hat{p}_x)\psi(x) \nonumber\\
	\Leftrightarrow \hat{p}_x\hat{x} &= -i\hbar + \hat{x}\hat{p}_x
	\label{eq:pxrel}
.\end{align}
First we prove that
\begin{equation}
	\forall m\in\mathbb{N}: \hat{p}_x^m\hat{x} = -mi\hbar \hat{p}_x^{m-1} + \hat{x}\hat{p}^m
	\label{eq:pxbigrel}
.\end{equation}
We do this by induction
\begin{align*}
	m=1:& \\
		&\hat{p}_x\hat{x}\overset{(\ref{eq:pxrel})}{=} -i\hbar+\hat{x}\hat{p}_x = -1\cdot i\hbar \hat{p}_x^{1-1} + \hat{x}\hat{p}_x^1 \\
	m \mapsto m+1:& \\
				  &\hat{p}_x^{m+1}\hat{x}=\hat{p}_x\hat{p}_x^m\hat{x}
				  \overset{\mathrm{I.A.}}{=} \hat{p}_x (-mi\hbar \hat{p}_x^{m-1} + \hat{x}\hat{p}_x^m)
				  = -mi\hbar \hat{p}_x^m + \hat{p}_x\hat{x}\hat{p}_x^m \\
				  &\overset{(\ref{eq:pxrel})}{=} -mi\hbar\hat{p}_x^m + (-i\hbar+\hat{x}\hat{p}_x)\hat{p}_x^m
				  =-(m+1)i\hbar\hat{p}_x^m+\hat{x}\hat{p}_x^{m+1} \qquad \square
.\end{align*}
This yields us
\begin{align*}
	[\hat{x},\hat{A}]
	&= \left[\hat{x},\sum_{n,m=0}^{\infty} c_{n,m} \hat{x}^n\hat{p}_x^m\right] \\
	&= \hat{x}\sum_{n,m=0}^{\infty} c_{n,m} \hat{x}^n\hat{p}_x^m - \left(\sum_{n,m=0}^{\infty} c_{n,m} \hat{x}^n\hat{p}_x^m\right)\hat{x} \\
	&= \sum_{n,m=0}^{\infty} c_{n,m} \hat{x}^{n+1}\hat{p}_x^m - \sum_{n,m=0}^{\infty} c_{n,m} \hat{x}^n\hat{p}_x^m\hat{x} \\
	&= \sum_{n,m=0}^{\infty} c_{n,m} \hat{x}^{n+1}\hat{p}_x^m -\sum_{n=0}^{\infty}c_{n,0} \hat{x}^{n+1} - \sum_{n=0,m=1}^{\infty} c_{n,m} \hat{x}^n\hat{p}_x^m\hat{x} \\
	&\overset{(\ref{eq:pxbigrel})}{=} \sum_{n=0,m=1}^{\infty}c_{n,m}\hat{x}^{n+1}\hat{p}_x^m - \sum_{n=0,m=1}^{\infty}c_{n,m}\hat{x}^n(-mi\hbar\hat{p}_x^{m-1}+\hat{x}\hat{p}_x^m) \\
	&= \sum_{n=0,m=1}^{\infty}c_{n,m}\hat{x}^{n+1}\hat{p}_x^m - \sum_{n=0,m=1}^{\infty}c_{n,m}\hat{x}^{n+1}\hat{p}_x^m + mi\hbar\sum_{n=0,m=1}^{\infty}c_{n,m}\hat{x}^n\hat{p}_x^{m-1} \\
	&= i\hbar\frac{\partial}{\partial\hat{p}_x}\sum_{n=0,m=0}^{\infty}c_{n,m}\hat{x}^n\hat{p}_x^m \\
	&= i\hbar\frac{\partial\hat{A}}{\partial\hat{p}_x} \qquad \square
.\end{align*}
For the other equation let's first prove that
\begin{equation}
	\forall n \in\mathbb{N}: \hat{p}_x\hat{x}^n = -ni\hbar\hat{x}^{n-1} + \hat{x}^n\hat{p}_x
	\label{eq:pxreverse}
.\end{equation}
Again we use induction
\begin{align*}
	n=1:& \\
		&\hat{p}_x\hat{x} \overset{(\ref{eq:pxrel})}{=} -i\hbar + \hat{x}\hat{p}_x = -1\cdot i\hbar\hat{x}^{1-1} + \hat{x}^1\hat{p}_x \\
	n\mapsto n+1:& \\
				 &\hat{p}_x\hat{x}^{n+1}=\hat{p}_x\hat{x}^n\hat{x}
				 \overset{\mathrm{I.A.}}{=} (-ni\hbar\hat{x}^{n-1} + \hat{x}^n\hat{p}_x)\hat{x} \\
				 &\overset{(\ref{eq:pxrel})}{=} -ni\hbar\hat{x}^n + \hat{x}^n(-i\hbar+\hat{x}\hat{p}_x) \\
				 &= -(n+1)i\hbar\hat{x}^n + \hat{x}^{n+1}\hat{p}_x \qquad \square
.\end{align*}
This yields us
\begin{align*}
	[\hat{p}_x,\hat{A}]
	&= \left[\hat{p}_x, \sum_{n,m=0}^{\infty} c_{n,m} \hat{x}^n\hat{p}_x^m\right] \\
	&= \hat{p}_x\sum_{n,m=0}^{\infty} c_{n,m} \hat{x}^n\hat{p}_x^m - \left(\sum_{n,m=0}^{\infty} c_{n,m} \hat{x}^n\hat{p}_x^m\right)\hat{p}_x \\
	&= \sum_{n,m=0}^{\infty}c_{n,m}\hat{p}_x\hat{x}^n\hat{p}_x^m - \sum_{n,m=0}^{\infty} c_{n,m} \hat{x}^n\hat{p}_x^{m+1} \\
	&= \sum_{n=1,m=0}^{\infty}c_{n,m}\hat{p}_x\hat{x}^n\hat{p}_x^m +\sum_{m=0}^{\infty}c_{0,m}\hat{p}_x^{m+1} - \sum_{n,m=0}^{\infty} c_{n,m} \hat{x}^n\hat{p}_x^{m+1} \\
	&\overset{(\ref{eq:pxreverse})}{=}
	\sum_{n=1,m=0}^{\infty}c_{n,m}(-ni\hbar\hat{x}^{n-1} + \hat{x}^n\hat{p}_x)\hat{p}_x^m
	- \sum_{n=1,m=0}^{\infty} c_{n,m} \hat{x}^n\hat{p}_x^{m+1} \\
	&= -ni\hbar\sum_{n=1,m=0}^{\infty}c_{n,m}\hat{x}^{n-1}\hat{p}_x^m + \sum_{n=1,m=0}^{\infty}c_{n,m}\hat{x}^n\hat{p}_x^{m+1}
	- \sum_{n=1,m=0}^{\infty} c_{n,m} \hat{x}^n\hat{p}_x^{m+1} \\
	&= -i\hbar \frac{\partial}{\partial\hat{x}}\sum_{n,m=0}^{\infty}c_{n,m}\hat{x}^n\hat{p}_x^m \\
	&= -i\hbar \frac{\partial\hat{A}}{\partial\hat{x}} \qquad \square
.\end{align*}
\subsection*{c,}
\begin{align*}
	[[\hat{A},\hat{B}]&,\hat{C}]+[[\hat{C},\hat{A}],\hat{B}]+[[\hat{B},\hat{C}],\hat{A}] \\
	=& [\hat{A}\hat{B}-\hat{B}\hat{A},\hat{C}]
	+[\hat{C}\hat{A}-\hat{A}\hat{C},\hat{B}]
	+[\hat{B}\hat{C}-\hat{C}\hat{B},\hat{A}] \\
	=& (\hat{A}\hat{B}-\hat{B}\hat{A})\hat{C}-\hat{C}(\hat{A}\hat{B}-\hat{B}\hat{A}) \\
	&+(\hat{C}\hat{A}-\hat{A}\hat{C})\hat{B}-\hat{B}(\hat{C}\hat{A}-\hat{A}\hat{C}) \\
	&+(\hat{B}\hat{C}-\hat{C}\hat{B})\hat{A}-\hat{A}(\hat{B}\hat{C}-\hat{C}\hat{B}) \\
	=& \hat{A}\hat{B}\hat{C}-\hat{B}\hat{A}\hat{C}-\hat{C}\hat{A}\hat{B}+\hat{C}\hat{B}\hat{A} \\
	 &+\hat{C}\hat{A}\hat{B}-\hat{A}\hat{C}\hat{B}-\hat{B}\hat{C}\hat{A}+\hat{B}\hat{A}\hat{C} \\
	 &+\hat{B}\hat{C}\hat{A}-\hat{C}\hat{B}\hat{A}-\hat{A}\hat{B}\hat{C}+\hat{A}\hat{C}\hat{B} \\
	=&\,0
.\end{align*}
\end{document}

