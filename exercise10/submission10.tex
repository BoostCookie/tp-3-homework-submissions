\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage[style=iso]{datetime2}
\usepackage{amsmath, amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{ragged2e}
\usepackage[locale=DE,binary-units=true]{siunitx}
\usepackage{braket}
\usepackage{bm}
\usepackage{bbold}
\usepackage{extarrows}
\pdfsuppresswarningpagegroup=1

\title{Theoretische Physik 3: Quantenmechanik}
\subtitle{Submission for exercise 10}
\author{Stefan Gehr}

%\date{\today}
\date{2020-07-03}

\begin{document}
\maketitle
\section*{Problem 1: Algebra of orbital angular momentum}
\subsection*{a,}
We have \(\hat{\vec{r}}=\vec{r}\) and \(\hat{\vec{p}}=-i\hbar\vec{\nabla}\).
With index notation and the properties
\begin{align}
	\epsilon^{abc}\epsilon_{ade}
	&= \delta^b_{\phantom{a}d}\delta^c_{\phantom{c}e}
	-\delta^b_{\phantom{b}e}\delta^c_{\phantom{c}d}
	\label{eq:leviOne} \\
	\hat{p}_a\hat{r}^b\psi(\vec{r})
	&= -i\hbar\partial_ar^b\psi(\vec{r})
	= -i\hbar(\delta_a^{\phantom{a}b}\psi(\vec{r})+r^b\partial_a\psi(\vec{r})) \nonumber\\
	&= (-i\hbar\delta_a^{\phantom{a}b}+\hat{r}^b\hat{p}_a)\psi(\vec{r}) \nonumber\\
	\Leftrightarrow \hat{p}_a\hat{r}^b
	&= -i\hbar\delta_a^{\phantom{a}b}+\hat{r}^b\hat{p}_a
	\label{eq:pr} \\
	\epsilon_{abc}\delta^{ab}
	&= \epsilon_{aac} = 0
	\label{eq:leviTwo}
\end{align}
of the Levi-Civita symbol in three dimensions,
it's easy to see that
\begin{align*}
	\left(\hat{\vec{L}} \times \hat{\vec{L}}\right)^{a}
	&= \epsilon^{abc}\hat{L}_b\hat{L}_c
	= \epsilon^{abc}\epsilon_{bde}\hat{r}^d\hat{p}^e\epsilon_{cfg}\hat{r}^f\hat{p}^g \\
	&\overset{(\ref{eq:leviOne})}{=}
	(\delta^c_{\phantom{c}d}\delta^a_{\phantom{a}e}
	-\delta^c_{\phantom{c}e}\delta^a_{\phantom{a}d})\epsilon_{cfg}
	\hat{r}^d\hat{p}^e\hat{r}^f\hat{p}^g \\
	&= \epsilon_{cfg}\hat{r}^c\hat{p}^a\hat{r}^f\hat{p}^g
	- \epsilon_{cfg}\hat{r}^a\hat{p}^c\hat{r}^f\hat{p}^g \\
	%&= \epsilon_{cfg}(\hat{r}^c\hat{p}^a-\hat{r}^a\hat{p}^c)\hat{r}^f\hat{p}^g \\
	&\overset{(\ref{eq:pr})}{=}
	\epsilon_{cfg}\hat{r}^c
	(-i\hbar\delta^{af}+\hat{r}^f\hat{p}^a)
	\hat{p}^g
	- \epsilon_{cfg}\hat{r}^a
	(-i\hbar\delta^{cf}+\hat{r}^f\hat{p}^c)
	\hat{p}^g \\
	&\overset{(\ref{eq:leviTwo})}{=}
	-i\hbar{\epsilon_{c}^{\phantom{c}a}}_{g}\,
	\hat{r}^c\hat{p}^g
	+\underbrace{\epsilon_{cfg}\hat{r}^c\hat{r}^f}_{=(\vec{r}\times \vec{r})_g=0}
	\hat{p}^a\hat{p}^g
	- \hat{r}^a\hat{r}^f
	\underbrace{\epsilon_{cfg}\hat{p}^c\hat{p}^g}_{(\hat{\vec{p}}\times \hat{\vec{p}})_f=0} \\
	&= i\hbar\epsilon^a_{\phantom{a}cg}\hat{r}^c\hat{p}^g
	= i\hbar\hat{L}^a
.\end{align*}
\begin{equation}
	\Rightarrow \hat{\vec{L}}\times \hat{\vec{L}}=i\hbar \hat{\vec{L}} \qquad \square
	\label{eq:a}
.\end{equation}
\subsection*{b,}
The coordinate system was chosen in such a way that
\begin{equation}
	\hat{L}_z\ket{l,m}=\hbar m\ket{l,m}
.\end{equation}
But the \(x\) and \(y\) direction can still be chosen arbitrarily
as long as they are perpendicular to the \(z\) direction and
to each other and as long as the coordinate system is right handed.
This shows right off the bat that for \(n\in\mathbb{N}\)
\begin{align}
	\bra{l,m}(\hat{L}_x)^n\ket{l,m}
	&= \bra{l,m}(\hat{L}_y)^n\ket{l,m}
	\label{eq:arbitraryxy}
.\end{align}
\begin{align*}
	\bra{l,m}\hat{L}_x\ket{l,m}
	&\overset{(\ref{eq:a})}{=}
	i\hbar\bra{l,m}[\hat{L}_y,\hat{L}_z]\ket{l,m} \\
	&= i\hbar\left(\bra{l,m}\hat{L}_y\ket{l,m}\hbar m - \hbar m\bra{l,m}\hat{L}_y\ket{\l,m}\right)
	= 0 \\
	\bra{l,m}\hat{L}_x^2\ket{l,m}
	&= \frac{1}{2}\bra{l,m}2\hat{L}_x^2\ket{l,m}
	\overset{(\ref{eq:arbitraryxy})}{=}
	\frac{1}{2}\bra{l,m}\left(\hat{L}_x^2+\hat{L}_y^2\right)\ket{l,m} \\
	&= \frac{1}{2}\bra{l,m}\left(\hat{\vec{L}}^2-\hat{L}_z^2\right)\ket{l,m}
	= \frac{1}{2}\left[\hbar^2l(l+1)-\hbar^2m^2\right] \\
	&= \frac{\hbar^2}{2}\left[l(l+1)-m^2\right]
\end{align*}
\begin{align*}
%	\frac{i\hbar}{2}\hbar m
%	&= \frac{i\hbar}{2}\bra{l,m}\hat{L}_z\ket{l,m}
%	\overset{(\ref{eq:a})}{=}
%	\frac{1}{2}\bra{l,m}[\hat{L}_x,\hat{L}_y]\ket{l,m} \\
%	&= \frac{1}{2}\left(\bra{l,m}\hat{L}_x\hat{L}_y\ket{l,m}
%	-\bra{l,m}\hat{L}_y\hat{L}_x\ket{l,m}\right) \\
%	&= \frac{1}{2}\left[\bra{l,m}\hat{L}_x\hat{L}_y\ket{l,m}
%	-\left(\bra{l,m}(\hat{L}_y\hat{L}_x)^{\dagger}\ket{l,m}\right)^*\right] \\
%	&= \frac{1}{2}\left[\bra{l,m}\hat{L}_x\hat{L}_y\ket{l,m}
%	-\left(\bra{l,m}\hat{L}_x^{\dagger}\hat{L}_y^{\dagger}\ket{l,m}\right)^*\right] \\
%	&= \frac{1}{2}\left[\bra{l,m}\hat{L}_x\hat{L}_y\ket{l,m}
%	-\left(\bra{l,m}\hat{L}_x\hat{L}_y\ket{l,m}\right)^*\right] \\
%	&= i\,\mathrm{Im}\left(\bra{l,m}\hat{L}_x\hat{L}_y\ket{l,m}\right) \\
	\bra{l,m}\hat{L}_x\hat{L}_y\ket{l,m}
	&= \bra{l,m}\frac{1}{2}\left(\hat{L}_++\hat{L}_-\right)
	\frac{1}{2i}\left(\hat{L}_+-\hat{L}_-\right)\ket{l,m} \\
	&= \frac{-i}{4}\bra{l,m}\left((\hat{L}_+)^2-[\hat{L}_+,\hat{L}_-]-(\hat{L}_-)^2\right)\ket{l,m} \\
	&= \frac{i}{4}\bra{l,m}[\hat{L}_+,\hat{L}_-]\ket{l,m} \\
	&= \frac{i\hbar^2}{4}\Big\{
		\sqrt{[j(j+1)-m(m-1)][j(j+1)-(m-1)m]} \\
	& -\sqrt{[j(j+1)-m(m+1)][j(j+1)-(m+1)m]}\Big\} \\
	&= \frac{i\hbar^2}{4}\left\{|j(j+1)-m(m-1)| -|j(j+1)-m(m+1)|\right\} \\
	&= \frac{i\hbar^2}{4}\left[|j^2-m^2+j+m| -|j^2-m^2+j-m|\right] \\
	&\overset{j\geq 0,\,|m|\leq j}{=}
	\frac{i\hbar^2}{4}\left[j^2-m^2+j+m -(j^2-m^2+j-m)\right] \\
	&= \frac{i}{2}\hbar^2m
.\end{align*}
Independent of \(m\) and \(l\) is the expected angular momentum in \(x\)- and \(y\)-direction
always \(0\). This makes sense since we chose the coordinate system in such a way
that all of the angular momentum should point in the \(z\)-direction. \\
\(m=0\) means that there is no angular momentum in the \(z\)-direction.
For \(l\neq 0\) there is a possibility of angular momentum in the \(x\)- and \(y\)-direction
(\(\Delta L_x = \Delta L_y \neq 0\)). \\
\(m=\pm l\) means that the angular momentum in \(z\)-direction is \(\pm\hbar l\) and
the variances for the \(x\)- and \(y\)-directions are
\((\Delta L_x)^2 = (\Delta L_y)^2 = \hbar^2\frac{l}{2}\).
\section*{Problem 2: Representation of angular momentum}
\subsection*{a,}
Let \(\theta\) be the angle between \(\vec{n}\) and \(\hat{\vec{J}}\).
\begin{align*}
	\left(\vec{n}\cdot\hat{\vec{J}}\right)^2\ket{\pm}
	&= \cos^2(\theta)\hat{\vec{J}}^2\Ket{\frac{1}{2},\pm \frac{1}{2}}
	= \cos^2(\theta)\hbar^2 \frac{1}{2}\left(\frac{1}{2}+1\right)\Ket{\frac{1}{2},\pm \frac{1}{2}}
	= \cos^2(\theta)\frac{3}{4}\hbar^2 \ket{\pm}
.\end{align*}
%We write \(\vec{n}\) in spherical coordinates
%\begin{align*}
%	\vec{n} =
%	\begin{pmatrix}
%		\cos(\phi)\sin(\theta) \\
%		\sin(\phi)\sin(\theta) \\
%		\cos(\theta)
%	\end{pmatrix}
%.\end{align*}
The exercise is worth 3 points which is why I don't think that's it.
So here's an alternative:
\begin{align*}
	\left(\vec{n}\cdot \hat{\vec{J}}\right)^2\ket{\pm}
	&= \left(n_x\hat{J}_x+n_y\hat{J}_y+n_z\hat{J}_z\right)^2\ket{\pm} \\
	&= \Big[n_x^2(\hat{J}_x)^2 + n_y^2(\hat{J}_y)^2 + n_z^2(\hat{J}_z)^2 \\
	&+ n_xn_y(\hat{J}_x\hat{J}_y
	+ \hat{J}_y\hat{J}_x)
	+ n_xn_z(\hat{J}_x\hat{J}_z
	+ \hat{J}_z\hat{J}_x) \\
	&+ n_yn_z(\hat{J}_y\hat{J}_z
	+ \hat{J}_z\hat{J}_y)
	\Big]\ket{\pm}
.\end{align*}
With \autoref{eq:jx}, \autoref{eq:jy} and \autoref{eq:jz} from the next
exercise we can determine
\begin{align}
	\left[(\hat{J}_x)^2\right]
	&= \left[(\hat{J}_y)^2\right]
	= \left[(\hat{J}_z)^2\right]
	= \frac{\hbar^2}{4} \mathbb{1}
	\label{eq:jxyzsquared} \\
	\left[\hat{J}_x\hat{J}_y\right]
	&= i\frac{\hbar^2}{4}
	\begin{pmatrix}
		1 & 0 \\
		0 & -1
	\end{pmatrix}
	= -\left[\hat{J}_y\hat{J}_x\right]
	\label{eq:jxjy} \\
	\left[\hat{J}_x\hat{J}_z\right]
	&= \frac{\hbar^2}{4}
	\begin{pmatrix}
		0 & -1 \\
		1 & 0
	\end{pmatrix}
	= -\left[\hat{J}_z\hat{J}_x\right]
	\label{eq:jxz} \\
	\left[\hat{J}_y\hat{J}_z\right]
	&= i\frac{\hbar^2}{4}
	\begin{pmatrix}
		0 & 1 \\
		1 & 0
	\end{pmatrix}
	= - \left[\hat{J}_z\hat{J}_y\right]
.\end{align}
With this it's easy to see that all the mixing terms fall out and
\begin{align*}
	\left(\vec{n}\cdot \hat{\vec{J}}\right)^2\ket{\pm}
	&= \frac{\hbar^2}{4}\left(n_x^2+n_y^2+n_z^2\right)\ket{\pm}
	= \frac{\hbar^2}{4}\ket{\pm}
.\end{align*}
\subsection*{b,}
\begin{align}
	\bra{\pm}\hat{J}_z\ket{\pm}
	&= \pm \frac{1}{2}\hbar \nonumber\\
	\bra{\pm}\hat{J}_z\ket{\mp}
	&= \mp \frac{1}{2}\hbar \braket{\pm|\mp} = 0 \nonumber\\
	\Rightarrow [\hat{J}_z]
	&= \frac{1}{2}\hbar
	\begin{pmatrix}
		1 & 0 \\
		0 & -1
	\end{pmatrix}
	\label{eq:jz}
\end{align}
\begin{align}
	\bra{\pm}\hat{\vec{J}}^2\ket{\pm}
	&= \frac{3}{4}\hbar^2 \nonumber\\
	\bra{\pm}\hat{\vec{J}}^2\ket{\mp}
	&= \frac{3}{4}\hbar^2 \braket{\pm,\mp} = 0 \nonumber\\
	\Rightarrow \left[\hat{\vec{J}}^2\right]
	&= \frac{3}{4}\hbar^2
	\begin{pmatrix}
		1 & 0 \\
		0 & 1
	\end{pmatrix}
	\label{eq:jsquared}
.\end{align}
For the last two we will use
\begin{align}
	\bra{\pm}\hat{J}_+\ket{\mp}
	&= \Bra{\frac{1}{2},\pm\frac{1}{2}}
	\hat{J}_+
	\Ket{\frac{1}{2},\mp\frac{1}{2}}
	= \hbar\sqrt{\frac{1}{2}\pm \frac{1}{2}}
	\Braket{\frac{1}{2},\pm\frac{1}{2}\bigg|\frac{1}{2},\mp\frac{1}{2}+1} \nonumber\\
	\bra{\pm}\hat{J}_-\ket{\mp}
	&= \Bra{\frac{1}{2},\pm\frac{1}{2}}
	\hat{J}_-
	\Ket{\frac{1}{2},\mp\frac{1}{2}}
	= \hbar\sqrt{\frac{1}{2}\mp \frac{1}{2}}
	\Braket{\frac{1}{2},\pm\frac{1}{2}\bigg|\frac{1}{2},\mp\frac{1}{2}-1} \nonumber\\
	\Longrightarrow\bra{+}\hat{J}_+\ket{-}
	&= \hbar \qquad
	\bra{-}\hat{J}_+\ket{+}
	= 0 \qquad
	\bra{+}\hat{J}_-\ket{-}
	= 0 \qquad
	\bra{-}\hat{J}_-\ket{+}
	= \hbar
	\label{eq:jplusminus}
\end{align}
and as already discussed in \textbf{Problem 1 b}
\begin{equation}
	\bra{j,m}\hat{J}_x\ket{j,m}
	= \bra{j,m}\hat{J}_y\ket{j,m} = 0
	\label{eq:oneBJ}
\end{equation}
which gives us
\begin{align}
	\bra{\pm}\hat{J}_{x}\ket{\pm}
	&\overset{(\ref{eq:oneBJ})}{=} 0
	\overset{(\ref{eq:oneBJ})}{=}
	\bra{\pm}\hat{J}_{y}\ket{\pm} \nonumber\\
	\bra{\pm}\hat{J}_x\ket{\mp}
	&= \frac{1}{2}\bra{\pm}\left(\hat{J}_++\hat{J}_-\right)\ket{\mp} \nonumber\\
	\Rightarrow \bra{+}\hat{J}_x\ket{-}
	&\overset{(\ref{eq:jplusminus})}{=}
	\frac{\hbar}{2} \nonumber\\
	\bra{-}\hat{J}_x\ket{+}
	&\overset{(\ref{eq:jplusminus})}{=}
	\frac{\hbar}{2} \nonumber\\
	\Longrightarrow [\hat{J}_x]
	&= \frac{\hbar}{2}
	\begin{pmatrix}
		0 & 1 \\
		1 & 0
	\end{pmatrix}
	\label{eq:jx}
\end{align}
\begin{align}
	\bra{\pm}\hat{J}_y\ket{\mp}
	&= \frac{1}{2i}\bra{\pm}\left(\hat{J}_+-\hat{J}_-\right)\ket{\mp} \nonumber\\
	\Rightarrow \bra{+}\hat{J}_y\ket{-}
	&\overset{(\ref{eq:jplusminus})}{=}
	\frac{\hbar}{2i} \nonumber\\
	\bra{-}\hat{J}_y\ket{+}
	&\overset{(\ref{eq:jplusminus})}{=}
	-\frac{\hbar}{2i} \nonumber\\
	\Longrightarrow [\hat{J}_y]
	&= i\frac{\hbar}{2}
	\begin{pmatrix}
		0 & -1 \\
		1 & 0
	\end{pmatrix}
	\label{eq:jy}
.\end{align}
\section*{Problem 3: Matrix product state}
\subsection*{a,}
\begin{align*}
	c_{++}
	&=
	\begin{pmatrix}
		1 & 1
	\end{pmatrix}
	\begin{pmatrix}
		\sqrt{\alpha} & 0 \\
		0 & 0
	\end{pmatrix}
	\begin{pmatrix}
		\sqrt{\alpha} & 0 \\
		0 & 0
	\end{pmatrix}
	\begin{pmatrix}
		1 \\
		1
	\end{pmatrix}
	= \alpha \\
	c_{+-}
	&=
	\begin{pmatrix}
		1 & 1
	\end{pmatrix}
	\begin{pmatrix}
		\sqrt{\alpha} & 0 \\
		0 & 0
	\end{pmatrix}
	\begin{pmatrix}
		0 & 0 \\
		0 & \sqrt{\beta}
	\end{pmatrix}
	\begin{pmatrix}
		1 \\
		1
	\end{pmatrix}
	= 0 \\
	c_{-+}
	&=
	\begin{pmatrix}
		1 & 1
	\end{pmatrix}
	\begin{pmatrix}
		0 & 0 \\
		0 & \sqrt{\beta}
	\end{pmatrix}
	\begin{pmatrix}
		\sqrt{\alpha} & 0 \\
		0 & 0
	\end{pmatrix}
	\begin{pmatrix}
		1 \\
		1
	\end{pmatrix}
	= 0 \\
	c_{--}
	&=
	\begin{pmatrix}
		1 & 1
	\end{pmatrix}
	\begin{pmatrix}
		0 & 0 \\
		0 & \sqrt{\beta}
	\end{pmatrix}
	\begin{pmatrix}
		0 & 0 \\
		0 & \sqrt{\beta}
	\end{pmatrix}
	\begin{pmatrix}
		1 \\
		1
	\end{pmatrix}
	= \beta
\end{align*}
\begin{align*}
	\Rightarrow \ket{\psi}
	&= \alpha\ket{+,+} + \beta\ket{-,-} \\
	\braket{\psi|\psi}
	&\overset{!}{=} 1 \\
	\Leftrightarrow \alpha^2 + \beta^2
	&= 1 \qquad \Rightarrow \beta = \sqrt{1-\alpha^2} \\
	\Rightarrow \ket{\psi}
	&= \alpha\ket{+,+} + \sqrt{1-\alpha^2}\ket{-,-}
.\end{align*}
The energy is
\begin{align*}
	E(\alpha)
	&= \bra{\psi}\hat{H}\ket{\psi} \\
	&= \frac{1}{4}
		\left[\alpha\bra{+,+} + \sqrt{1-\alpha^2}\bra{-,-}\right]
		\left[\hat{\sigma}_z \otimes \hat{\mathbb{1}}
		+ \hat{\mathbb{1}} \otimes \hat{\sigma}_z\right]
		\left[\alpha\ket{+,+} + \sqrt{1-\alpha^2}\ket{-,-}\right]
	\\
	&- \frac{\sqrt{3}}{4}
		\left[\alpha\bra{+,+} + \sqrt{1-\alpha^2}\bra{-,-}\right]
		\left[\hat{\sigma}_x \otimes \hat{\sigma}_x
		-\hat{\sigma}_y \otimes \hat{\sigma}_y\right]
		\left[\alpha\ket{+,+} + \sqrt{1-\alpha^2}\ket{-,-}\right] \\
	&= \frac{1}{4}\left[
		2\alpha^2 - 2\left(1-\alpha^2\right)
	\right]-\frac{\sqrt{3}}{4}\left[
		2\alpha\sqrt{1-\alpha^2}+2\alpha\sqrt{1-\alpha^2}
	\right] \\
	&= \alpha^2 -\frac{1}{2} - \sqrt{3}\alpha\sqrt{1-\alpha^2}
.\end{align*}
We want to find its minima in regards to \(\alpha\) 
\begin{align*}
	\frac{\mathrm{d}E}{\mathrm{d}\alpha}(\alpha)
	&\overset{!}{=} 0 \\
	\Leftrightarrow
	2\alpha -\sqrt{3}\sqrt{1-\alpha^2} + \sqrt{3}\frac{\alpha^2}{\sqrt{1-\alpha^2}}
	&= 0 \\
	\Leftrightarrow 2\alpha\sqrt{1-\alpha^2}
	&= \sqrt{3}\left(1-2\alpha^2\right) \\
	\Rightarrow 4\alpha^2\left(1-\alpha^2\right)
	&= 3\left(1-4\alpha^2+4\alpha^4\right) \\
	\Leftrightarrow 16 \alpha^4 - 16 \alpha^2 + 3 &= 0 \\
	\Leftrightarrow \alpha^4 - \alpha^2 + \frac{3}{16} &= 0 \\
	\Rightarrow (\alpha^2)_{1,2}
	&= \frac{1}{2}\pm\sqrt{\frac{1}{4}-\frac{3}{16}} \\
	&= \frac{1}{2}\pm \frac{1}{4} \\
	\Rightarrow \alpha_{1,2} = \pm \frac{\sqrt{3}}{2}
	&\qquad \alpha_{3,4} = \pm\frac{1}{2}
.\end{align*}
We enter those values back into the equation and find that only
\(\alpha_2 = -\frac{\sqrt{3}}{2}\) and \(\alpha_3 = \frac{1}{2}\)
have the property \(\frac{\mathrm{d}E}{\mathrm{d}\alpha}(\alpha_i) = 0\).
To find out if either of those is a minima we take the second derivative
\begin{align*}
	\frac{\mathrm{d}^2E}{\mathrm{d}\alpha^2}(\alpha)
	&= 2 + \sqrt{3}\frac{\alpha}{\sqrt{1-\alpha^2}}
	+ \sqrt{3} \frac{2\alpha\sqrt{1-\alpha^2}+\frac{\alpha^3}{\sqrt{1-\alpha^2}}}{1-\alpha^2}
\end{align*}
and see that
\(\frac{\mathrm{d}^2E}{\mathrm{d}\alpha^2}(\alpha_2) < 0\) and
\(\frac{\mathrm{d}^2E}{\mathrm{d}\alpha^2}(\alpha_3) > 0\).
\(\Rightarrow \alpha_3 = \frac{1}{2}\) is the only minima.
Therefore our approximation for the ground state is \[
	\ket{\psi} = \alpha_3\ket{+,+} + \sqrt{1-(\alpha_3)^2}\ket{-,-}
	= \frac{1}{2}\ket{+,+} + \frac{\sqrt{3}}{2}\ket{-,-}
.\] 
\subsection*{b,}
\(2^N\) coefficients are needed. For \(N=50\) that are \[
	2^{50} = 1125899906842624 \approx \num{1.1e+15}
\] complex numbers that need \[
2^{50} \cdot \SI{16}{\byte}
= \SI{16}{\pebi\byte} = \SI{16384}{\tebi\byte}
\approx \SI{18014}{\tera\byte} \approx \SI{18}{\peta\byte}
\] of storage.
\subsection*{c,}
We have 2 matrices per subsystem (one for \(+\) and one for \(-\) )
and 4 complex numbers per matrix. So overall we need to store \[
2 \cdot 4 \cdot N = 8 N
\] complex numbers. For \(N=50\) that are \[8\cdot 50 = 400\] complex numbers
which requires \[
	400 \cdot \SI{16}{\byte} = \SI{6400}{\byte} = \SI{6.4}{\kilo\byte} = \SI{6.25}{\kibi\byte}
\] of storage.
\subsection*{d,}
No, it's not always possible to express \(10^{15}\) numbers with just \(400\) numbers, duh.
\section*{Problem 4: Generator of rotations}
\subsection*{a,}
With
\begin{equation}
	\frac{i}{\hbar}\delta\vec{\varphi}\cdot \hat{\vec{L}}
	= \frac{i}{\hbar}\delta\vec{\varphi}
	\cdot\left(\vec{r}\times\frac{\hbar}{i}\vec{\nabla}\right)
	= \delta\varphi^a \epsilon_{abc}r^b\partial^c
	= \epsilon_{cab}\delta\varphi^ar^b\partial^c
	= \left(\delta\vec{\varphi}\times\vec{r}\right)\cdot\vec{\nabla}
	\label{eq:volume}
\end{equation}
we use Taylor expansion
\begin{align*}
	\hat{U}_{\delta\vec{\varphi}}\Psi(\vec{r})
	&= \exp\left(\frac{i}{\hbar}\delta\vec{\varphi}\cdot\hat{\vec{L}}\right)
	\Psi(\vec{r}) \\
	&\overset{(\ref{eq:volume})}{=}
	\exp\left[
		\left(\delta\vec{\varphi}\times\vec{r}\right)\cdot\vec{\nabla}
	\right]\Psi(\vec{r}) \\
	&= \left[1+
		\left(\delta\vec{\varphi}\times\vec{r}\right)\cdot\vec{\nabla}
	\right]\Psi(\vec{r})
	+ \mathcal{O}(\delta\vec{\varphi}^2) \\
	&= \Psi(\vec{r})
	+ \left(\delta\vec{\varphi}\times\vec{r}\right)
	\cdot\left(\vec{\nabla}\Psi(\vec{r})\right)
	+ \mathcal{O}(\delta\vec{\varphi}^2) \\
	&= \Psi(\vec{r}+\delta\vec{\varphi}\times \vec{r})
	+ \tilde{\mathcal{O}}(\delta\vec{\varphi}^2)
	\qquad \square
.\end{align*}
\subsection*{b,}
The definition of a "scalar operator" \(\hat{A}\) is that
\begin{equation}
	[\hat{L}^a, \hat{A}] = 0
	\label{eq:scalarop}
.\end{equation}
Let's name the state that \(\hat{A}\) transforms \(\Psi(\vec{r})\)
\begin{equation}
\tilde{\Psi}(\vec{r}) = \hat{A}\Psi(\vec{r})
\label{eq:atrafo}
.\end{equation}
\begin{align*}
	\Rightarrow \hat{U}_{\delta\vec{\varphi}}\tilde{\Psi}(\vec{r})
	&\overset{(\ref{eq:atrafo})}{=}
	\hat{U}_{\delta\vec{\varphi}}\hat{A}\Psi(\vec{r}) \\
	&= \hat{U}_{\delta\vec{\varphi}}\hat{A}
	\underbrace{\hat{U}_{\delta\vec{\varphi}}^{\dagger}\hat{U}_{\delta\vec{\varphi}}}
	_{\hat{\mathbb{1}}}
	\Psi(\vec{r}) \\
	&\overset{(\ref{eq:atrafo})}{=}
	\hat{U}_{\delta\vec{\varphi}}\hat{A}\hat{U}_{\delta\vec{\varphi}}^{\dagger}
	\left(\hat{U}_{\delta\vec{\varphi}}\Psi(\vec{r})\right)
.\end{align*}
\(\Rightarrow \hat{A}\) transforms as \[
	\hat{A} \mapsto \tilde{\hat{A}} = 
	\hat{U}_{\delta\vec{\varphi}}\hat{A}\hat{U}_{\delta\vec{\varphi}}^{\dagger}
.\] 
It's easy to see that
\begin{align*}
	\tilde{\hat{A}}
	&= 
	\hat{U}_{\delta\vec{\varphi}}\hat{A}\hat{U}_{\delta\vec{\varphi}}^{\dagger}
	= \left(1+\frac{i}{\hbar}\delta\vec{\varphi}\cdot \hat{\vec{L}}\right)\hat{A}
	\left(1-\frac{i}{\hbar}\delta\vec{\varphi}\cdot \hat{\vec{L}}\right)
	+ \mathcal{O}(\delta\vec{\varphi}^2) \\
	&= \hat{A} + \frac{i}{\hbar}\delta\vec{\varphi}\cdot \hat{\vec{L}}\hat{A}
	- \hat{A}\frac{i}{\hbar}\delta\vec{\varphi}\cdot \hat{\vec{L}}
	+ \mathcal{O}'(\delta\vec{\varphi}^2) \\
	&= \hat{A} + \frac{i}{\hbar}\delta\vec{\varphi}_a[\hat{L}^a,\hat{A}]
	+ \mathcal{O}'(\delta\vec{\varphi}^2)
	\overset{(\ref{eq:scalarop})}{=} \hat{A}
	+ \mathcal{O}'(\delta\vec{\varphi}^2) \\
	&\approx \hat{A}
\end{align*}
Therefore a scalar operator is invariant under (small?) rotations.
\subsection*{c,}
The definition of a vector operator is
\begin{equation}
	[\hat{L}^a,\hat{v}^b] = i\hbar\epsilon^{abc}\hat{v}_c
	\label{eq:vectorop}
.\end{equation}
The first part of the derivation is similar to that of the scalar operator until
\begin{align*}
	\tilde{\hat{v}}^a
	&= \hdots
	= \hat{v}^a + \frac{i}{\hbar}\delta\varphi_b[\hat{L}^b,v^a]
	+ \mathcal{O}(\delta\vec{\varphi}^2) \\
	&\overset{(\ref{eq:vectorop})}{=}
	\hat{v}^a + \frac{i}{\hbar}\delta\varphi_b\left(i\hbar\epsilon^{bac}\hat{v}_c\right)
	+ \mathcal{O}(\delta\vec{\varphi}^2) \\
	&= \hat{v}^a + \epsilon^{abc}\delta\varphi_b\hat{v}_c
	+ \mathcal{O}(\delta\vec{\varphi}^2) \\
	&\approx \hat{v}^a + \left(\delta\vec{\varphi}\times \hat{\vec{v}}\right)^a
.\end{align*}
\end{document}

