\documentclass{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\usepackage[style=iso]{datetime2}
\usepackage{amsmath, amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{ragged2e}
\usepackage[locale=DE]{siunitx}
\usepackage{braket}
\usepackage{bm}
\usepackage{bbold}
\pdfsuppresswarningpagegroup=1

\title{Theoretische Physik 3: Quantenmechanik}
\subtitle{Submission for exercise 6}
\author{Stefan Gehr}

%\date{\today}
\date{2020-06-04}

\begin{document}
\maketitle
\section*{Problem 1: Angular momentum in quantum mechanics}
We want to take a look at the angular momentum operator
\begin{align*}
	\hat{\vec{L}} &= \hat{\vec{r}} \times \hat{\vec{p}} \\
	\hat{L}^a &= \epsilon^a_{\phantom{a}bc} \hat{r}^b \hat{p}^c
.\end{align*}
We will use the equation
\begin{align}
	%[\hat{A}, \hat{B} + \hat{C}] = [\hat{A},\hat{B}]+[\hat{A},\hat{C}]
	%\label{eq:plusop} \\
	[\hat{A}, \hat{B} \hat{C}] = [\hat{A},\hat{B}]\hat{C} + \hat{B}[\hat{A},\hat{C}]
	\label{eq:mulop}
\end{align}
from exercise 3 problem 3 and also
\begin{align}
	[\hat{r}^a,\hat{F}(\hat{r},\hat{p})]=i\hbar\frac{\partial\hat{F}}{\partial\hat{p}_a}
	\label{eq:rf} \\
	[\hat{p}^a,\hat{F}(\hat{r},\hat{p})]=-i\hbar\frac{\partial\hat{F}}{\partial\hat{r}_a}
	\label{eq:pf}
.\end{align}

\subsection*{a,}
\begin{align*}
	\frac{\mathrm{d}\hat{L}^a}{\mathrm{d}t}
	&= [\hat{L}^a,\hat{H}] + \frac{\partial\hat{L}^a}{\partial t}
	= \epsilon^a_{\phantom{a}bc}[\hat{r}^b\hat{p}^c,\hat{H}]
	= -\epsilon^a_{\phantom{a}bc}[\hat{H},\hat{r}^b\hat{p}^c] \\
	&\overset{(\ref{eq:mulop})}{=} -\epsilon^a_{\phantom{a}bc}\left([\hat{H},\hat{r}^b]\hat{p}^c + \hat{r}^b[\hat{H},\hat{p}^c]\right)
	= \epsilon^a_{\phantom{a}bc}\left([\hat{r}^b,\hat{H}]\hat{p}^c + \hat{r}^b[\hat{p}^c,\hat{H}]\right) \\
	&= \epsilon^a_{\phantom{a}bc}\left([\hat{r}^b,\hat{H}]\hat{p}^c + \hat{r}^b[\hat{p}^c,\hat{H}]\right)
	\overset{(\ref{eq:rf}),(\ref{eq:pf})}{=} i\hbar\epsilon^a_{\phantom{a}bc}\left(\frac{\partial\hat{H}}{\partial\hat{p}_b}\hat{p}^c -\hat{r}^b\frac{\partial\hat{H}}{\partial\hat{r}_c}\right)
.\end{align*}
We calculate those derivatives
\begin{align*}
	\frac{\partial\hat{H}}{\partial\hat{p}_b}
	&= \frac{\partial}{\partial\hat{p}_b}\frac{\hat{p}^d\hat{p}_d}{2m}+\frac{\partial}{\partial\hat{p}_b}\hat{V}(\hat{r}^e\hat{r}_e)
	= \frac{1}{2m}\left(\delta^{bd}\hat{p}_d+\hat{p}^d\delta_d^b\right)
	= \frac{\hat{p}^b}{m} \\
	\frac{\partial\hat{H}}{\partial\hat{r}_c}
	&= \frac{\partial}{\partial\hat{r}_c}\frac{\hat{p}^d\hat{p}_d}{2m}+\frac{\partial}{\partial\hat{r}_c}\hat{V}(\hat{r}^e\hat{r}_e)
	= (\delta^{ce}\hat{r}_e+\hat{r}^e\delta_e^c)\hat{V}'(\hat{r}^e\hat{r}_e)
	= 2\hat{r}^c\hat{V}'(\hat{r}^e\hat{r}_e)
.\end{align*}
Inserting this back into our equation we get
\begin{align*}
	\frac{\mathrm{d}\hat{L}^a}{\mathrm{d}t}
	&= i\hbar\epsilon^a_{\phantom{a}bc}\left(\frac{\hat{p}^b}{m}\hat{p}^c -2\hat{r}^b\hat{r}^c\hat{V}'(\hat{r}^e\hat{r}_e)\right)
	= i\hbar\left[\frac{1}{m}\left(\hat{\vec{p}}\times\hat{\vec{p}}\right)^a -2 \left(\hat{\vec{r}}\times\hat{\vec{r}}\right)^a\hat{V}'(\hat{r}^e\hat{r}_e)\right]
	= 0
.\end{align*}
\subsection*{b,}
No, because \(\hat{\vec{L}}\) being conserved comes from, as Noether's theorem has taught us, the system's symmetry.
Both the classical potential \(V(\vec{r})\) and the potential operator \(\hat{V}(\hat{\vec{r}})\) have this same symmetry.
Therefore we should expect conserved quantities that are derived from those symmetries to be the same in both cases.
\section*{Problem 2: Baker-Campbell-Hausdorff formula}
\subsection*{a,}
We need to show that
\begin{equation}
	[\hat{A},\hat{B}^n] = \sum_{s=0}^{n-1}\hat{B}^s[\hat{A},\hat{B}]\hat{B}^{n-s-1}
	\label{eq:2a}
.\end{equation}
We prove by induction
\begin{align*}
	n=1:& \\
		&[\hat{A},\hat{B}^1]
		= [\hat{A},\hat{B}]
		= \hat{B}^0[\hat{A},\hat{B}]\hat{B}^{1-1}
		= \sum_{s=0}^{1-1} \hat{B}^s[\hat{A},\hat{B}]\hat{B}^{1-s-1}. \\
	n\mapsto n+1:& \\
				&[\hat{A},\hat{B}^{n+1}]
				= [\hat{A},\hat{B}^n\hat{B}]
				\overset{(\ref{eq:mulop})}{=} [\hat{A},\hat{B}^n]\hat{B}+\hat{B}^n[\hat{A},\hat{B}] \\
				&\overset{\mathrm{I.A.}}{=} \left(\sum_{s=0}^{n-1}\hat{B}^s[\hat{A},\hat{B}]\hat{B}^{n-s-1}\right)\hat{B}+\hat{B}^n[\hat{A},\hat{B}] \\
				&= \sum_{s=0}^{n-1}\hat{B}^s[\hat{A},\hat{B}]\hat{B}^{n+1-s-1} + \hat{B}^n[\hat{A},\hat{B}] \\
				&= \sum_{s=0}^{n+1-1}\hat{B}^s[\hat{A},\hat{B}]\hat{B}^{n+1-s-1} \qquad \square
.\end{align*}
\subsection*{b,}
We have operators \(\hat{A}\) and \(\hat{B}\) with the property
\begin{equation}
	[[\hat{A},\hat{B}]\hat{A}] = [[\hat{A},\hat{B}],\hat{B}] = 0
	\label{eq:2bprop}
\end{equation}
\[
	\hat{F}(0) = \hat{\mathbb{1}} = \hat{G}(0)
.\] 
\begin{align*}
	\frac{\mathrm{d}\hat{F}}{\mathrm{d}\lambda}
	&= \hat{A}e^{\lambda\hat{A}}e^{\lambda\hat{B}}\hat{B}
	= \hat{A}\hat{F}(\lambda)+\hat{F}(\lambda)\hat{B}
	= \hat{A}\hat{F}(\lambda)+\hat{B}\hat{F}(\lambda) - [\hat{B},\hat{F}] \\
	&= (\hat{A}+\hat{B})\hat{F}(\lambda) - [\hat{B},\hat{F}(\lambda)]
.\end{align*}
\begin{align*}
	[\hat{B},\hat{F}(\lambda)]
	&= [\hat{B},e^{\lambda\hat{A}}e^{\lambda\hat{B}}]
	= [\hat{B},e^{\lambda\hat{A}}]e^{\lambda\hat{B}}
	= \left[\hat{B}, \sum_{n=0}^{\infty} \frac{(\lambda\hat{A})^n}{n!}\right]e^{\lambda\hat{B}} \\
	&= \left[\hat{B}, \sum_{n=1}^{\infty} \frac{(\lambda\hat{A})^n}{n!}\right]e^{\lambda\hat{B}}
	= \sum_{n=1}^{\infty}\frac{\lambda^n}{n!}[\hat{B},\hat{A}^n] \\
	&\overset{(\ref{eq:2a})}{=} \sum_{1=0} \frac{\lambda^n}{n!} \sum_{s=0}^{n-1} \hat{A}^s[\hat{B},\hat{A}]\hat{A}^{n-s-1}e^{\lambda\hat{B}} \\
	&\overset{(\ref{eq:2bprop})}{=} [\hat{B},\hat{A}]\sum_{n=1}^{\infty}\frac{\lambda^n}{n!}\sum_{s=0}^{n-1}\hat{A}^s\hat{A}^{n-s-1} \\
	&= [\hat{B},\hat{A}]\sum_{n=1}^{\infty}\frac{\lambda^n}{(n-1)!}\hat{A}^{n-1}e^{\lambda\hat{B}} \\
	&= \lambda[\hat{B},\hat{A}]\sum_{n=0}^{\infty}\frac{\lambda^n}{n!}\hat{A}^{n}e^{\lambda\hat{B}}
	= \lambda[\hat{B},\hat{A}]e^{\lambda\hat{A}}e^{\lambda\hat{B}} \\
	&= \lambda[\hat{B},\hat{A}]\hat{F}(\lambda)
.\end{align*}
\begin{align*}
	\Rightarrow\frac{\mathrm{d}\hat{F}}{\mathrm{d}\lambda}
	&= (\hat{A}+\hat{B}-\lambda[\hat{B},\hat{A}])\hat{F}(\lambda) = (\hat{A}+\hat{B}+\lambda[\hat{A},\hat{B}])\hat{F}(\lambda)
.\end{align*}
\begin{align*}
	\frac{\mathrm{d}\hat{G}}{\mathrm{d}\lambda}
	&= e^{\lambda(\hat{A}+\hat{B})}\frac{\mathrm{d}}{\mathrm{d}\lambda}e^{\frac{\lambda^2}{2}[\hat{A},\hat{B}]}
	+ \left(\frac{\mathrm{d}}{\mathrm{d}\lambda}e^{\lambda(\hat{A}+\hat{B})}\right)e^{\frac{\lambda^2}{2}[\hat{A},\hat{B}]} \\
	&= e^{\lambda(\hat{A}+\hat{B})}\lambda[\hat{A},\hat{B}]e^{\frac{\lambda^2}{2}[\hat{A},\hat{B}]}
	+ (\hat{A}+\hat{B})e^{\lambda(\hat{A}+\hat{B})}e^{\frac{\lambda^2}{2}[\hat{A},\hat{B}]} \\
	&= (\hat{A}+\hat{B}+\lambda[\hat{A},\hat{B}])e^{\lambda(\hat{A}+\hat{B})}e^{\frac{\lambda^2}{2}[\hat{A},\hat{B}]} \\
	&= (\hat{A}+\hat{B}+\lambda[\hat{A},\hat{B}])\hat{G}(\lambda)
.\end{align*}
\[
	\Rightarrow \hat{F}(\lambda) = \hat{G}(\lambda) \quad \square
.\] 
\section*{Problem 3: Hermitian operators}
\subsection*{a,}
\begin{align*}
	\hat{A}\hat{B} \overset{!}{=} (\hat{A}\hat{B})^{\dagger} = \hat{B}^{\dagger}\hat{A}^{\dagger} = \hat{B}\hat{A} \\
	\Rightarrow [\hat{A},\hat{B}] \overset{!}{=} 0
.\end{align*}
Because \(\hat{A}\) and \(\hat{B}\) commute we know that they share eigenstates.
Upon measuring \(\hat{A}\hat{B}\) the system will be in one of those eigenstates.
\subsection*{b,}
\begin{align*}
	c[\hat{A},\hat{B}] &\overset{!}{=} (c[\hat{A},\hat{B}])^{\dagger}
	= c^*[\hat{A},\hat{B}]^{\dagger}
	= c^*[(\hat{A}\hat{B}-\hat{B}\hat{A})^{\dagger})]
	= c^*[(\hat{A}\hat{B})^{\dagger}-(\hat{B}\hat{A})^{\dagger})] \\
	&= c^*[\hat{B}^{\dagger}\hat{A}^{\dagger}-\hat{A}^{\dagger}\hat{B}^{\dagger}]
	= c^*[\hat{B}\hat{A}-\hat{A}\hat{B}]
	= c^*[\hat{B},\hat{A}]
	= -c^*[\hat{A},\hat{B}] \\
	\Leftrightarrow 0 &= (c+c^*)[\hat{A},\hat{B}] = 2\,\mathrm{Re}(c)[\hat{A},\hat{B}]
.\end{align*}
So \([\hat{A},\hat{B}]=0\) or \(\mathrm{Re}(c)=0\) need to be the case for \(c[\hat{A},\hat{B}]\) to be Hermitian.
We set \(\hat{B}=\hat{H}\).
Because \(\hat{A}\) is Hermitian we expect \[
	\frac{\mathrm{d}}{\mathrm{d}t}\braket{A} \in \mathbb{R}
\] and because of Ehrenfest we know that \[
\frac{\mathrm{d}}{\mathrm{d}t}\braket{A} = \braket{\frac{\partial A}{\partial t}} + \frac{1}{i\hbar}\braket{[\hat{A},\hat{H}]}
.\] Because \(\hat{A}\) is arbitrary we know that
\begin{align*}
	\frac{1}{i\hbar}\braket{[\hat{A},\hat{H}]} \in \mathbb{R} \\
	\Leftrightarrow \mathrm{Re}\left(\braket{[\hat{A},\hat{H}]}\right)=0
\end{align*}
which is consistent with our result, because if \(\mathrm{Re}(c)=0\) and \(\mathrm{Re}\left(\braket{[\hat{A},\hat{H}]}\right)=0\) then  \[
	c\braket{[\hat{A},\hat{H}]} = \braket{c[\hat{A},\hat{H}]}\in\mathbb{R}
.\] 
\section*{Problem 4: Two 2-level systems}
\subsection*{a,}
\begin{align*}
	\ket{\psi}
	&= \ket{\psi_1}\otimes\ket{\psi_2}
	= \frac{1}{\sqrt{2}}(\ket{1_1}+\ket{2_1})\otimes\ket{1_2} \\
	&= \frac{1}{\sqrt{2}}(\ket{1_1,1_2}+\ket{2_1,1_2})
	= \frac{1}{\sqrt{2}} (1, 0, 1, 0)^{\intercal}
.\end{align*}
\subsection*{b,}
We have the operator \[
	\hat{U}=\ket{2_1}\bra{2_1}\otimes(\ket{2_2}\bra{1_2}+\ket{1_2}\bra{2_2})+\ket{1_1}\bra{1_1}\otimes(\ket{1_2}\bra{1_2}+\ket{2_2}\bra{2_2})
.\]
We apply it
\begin{align*}
	\hat{U}\ket{\psi}
	&= \frac{1}{\sqrt{2}}\left(\ket{2_1}\otimes\ket{2_2} + \ket{1_1}\otimes\ket{1_2}\right)
	= \frac{1}{\sqrt{2}}\left(\ket{2_1,2_2}+\ket{1_1,1_2}\right) \\
	&= \frac{1}{\sqrt{2}}(1,0,0,1)^{\intercal}
.\end{align*}
We can write the operator also as
\begin{align*}
	\hat{U}
	&= \ket{2_1,2_2}\bra{2_1,1_2}+\ket{2_1,1_2}\bra{2_1,2_2}
	+ \ket{1_1,1_2}\bra{1_1,1_2}+\ket{1_1,2_2}\bra{1_1,2_2}
\end{align*}
which lets us easily convert it into its matrix representation in the given basis
\begin{align*}
	\hat{U} =
	\begin{pmatrix}
		1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0 \\
		0 & 0 & 0 & 1 \\
		0 & 0 & 1 & 0
	\end{pmatrix}
.\end{align*}
With this it is easy to see that
\begin{align*}
	\hat{U}^{\dagger}\hat{U}
	&= \hat{U}\hat{U} =
	\begin{pmatrix}
		1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0 \\
		0 & 0 & 0 & 1 \\
		0 & 0 & 1 & 0
	\end{pmatrix}
	\begin{pmatrix}
		1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0 \\
		0 & 0 & 0 & 1 \\
		0 & 0 & 1 & 0
	\end{pmatrix}
	=
	\begin{pmatrix}
		1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1
	\end{pmatrix} = \hat{\mathbb{1}}
.\end{align*}
\subsection*{c,}
\begin{align*}
	\alpha_1\alpha_2\ket{1_1,1_2}&+\beta_1\alpha_2\ket{2_1,1_2}+\alpha_1\beta_2\ket{1_1,2_2}+\beta_1\beta_2\ket{2_1,2_2} \\
	&= (\alpha_1\alpha_2,\alpha_1\beta_2,\beta_1\alpha_2,\beta_1\beta_2)^{\intercal} \\
	&\overset{!}{=} \frac{1}{\sqrt{2}}(1,0,0,1)^{\intercal}
\end{align*}
\begin{align*}
	\Rightarrow \alpha_1\alpha_2 = \beta_1\beta_2 = \frac{1}{\sqrt{2}} \quad\mathrm{and}\quad \alpha_1\beta_2=\beta_1\alpha_2=0
.\end{align*}
The left equation requires all four factors to be non-zero which contradicts with the right equation. \(\square\)
\subsection*{d,}
The subsystems 1 and 2 are entangled.
\end{document}

